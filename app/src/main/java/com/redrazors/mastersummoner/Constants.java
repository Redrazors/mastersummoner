package com.redrazors.mastersummoner;

import com.redrazors.mastersummoner.data.Bestiary;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String TAG = "Master Summoner";



    public static final String STANDARD_LIST="standard";
    public static final String FEAT_EXPANDED="expanded";
    public static final String FEAT_ALTERNATIVE ="nonstandard";
    public static final String FEAT_GOOD="good";
    public static final String FEAT_NEUTRAL="neutral";
    public static final String FEAT_EVIL="evil";
    public static final String FEAT_SPIDER="spider";
    public static final String FEAT_SKELETON="skeleton";

    public static final String RING_AEON="ringAeon";
    public static final String RING_AGATHION="ringAgathion";
    public static final String RING_ANGEL="ringAngel";
    public static final String RING_ARCHON="ringArchon";
    public static final String RING_ASURA="ringAsura";
    public static final String RING_DAEMON="ringDaemon";
    public static final String RING_DIV="ringDiv";
    public static final String RING_INEVITABLE="ringInevitable";
    public static final String RING_KYTON="ringKyton";
    public static final String RING_PROTEAN="ringProtean";
    public static final String RING_PSYCOPOMP="ringPsychopomp";
    public static final String RING_QLIPPOTH="ringQlippoth";

    public static final String FEAT_PLANT="plant";
    public static final String RING_DRAKE="ringDrake";
    public static final String RING_KAMI="ringKami";
    public static final String RING_LESHY="ringLeshy";

    public static final String FEAT_AUGMENTED="augmented";
    public static final String FEAT_SUPERIOR = "superior";
    public static final String FEAT_EVOLVED = "evolved";
    public static final String EQUIPMENT_ROD_GIANT = "rodGiant";
    public static final String FEAT_VERSATILE = "versatile";
    public static final String ARCHETYPE_SHADOW_CALLER = "shadowCaller";
    public static final String ARMY_OF_DARkNESS = "armyDarkness";


    // only summon monster specific options that need to be disabled when mode natures ally is activated
    public static final String[] OPTIONS_SUMMON_MONSTER=new String[]{
            FEAT_EXPANDED, FEAT_GOOD, FEAT_NEUTRAL, FEAT_EVIL, FEAT_SKELETON, ARCHETYPE_SHADOW_CALLER,
            RING_AEON, RING_AGATHION, RING_ANGEL, RING_ARCHON, RING_ASURA, RING_DAEMON, RING_DIV, RING_INEVITABLE,
            RING_KYTON, RING_PROTEAN, RING_PSYCOPOMP, RING_QLIPPOTH,
    };

    public static final String[] OPTIONS_NATURES_ALLY=new String[]{
            FEAT_PLANT, RING_DRAKE , RING_KAMI, RING_LESHY
    };

    public static final String OPTION_SOUND = "sound";
    public static final String OPTION_DICE = "dice";

    public static final String MODE_SUMMON_MONSTER = "modeSummonMonster";


    public static final String PREFERENCES_FEAT_FLATS= "featFlags";
    public static final String PREFERENCES_EXPANDED_SUMMONS= "expandedSummons";
    public static final String PREFERENCES_EXISTING_SUMMONS= "existingSummons";

    public static final String ACTIVE_SUMMON= "activeSummon";


    public static final String ARGUMENT_SUMMON_ID = "argumentSummonID";
    public static final String ARGUMENT_SUMMON_NAME = "argumentSummonName";
    public static final String ARGUMENT_SUMMON_HAS_TEMPLATE = "argumentSummonHasTemplate";
    public static final String ARGUMENT_ATTACK= "argumentAttack";
    public static final String ARGUMENT_SEQUENCE_REF= "argumenSequenceRef";
    public static final String ARGUMENT_LABEL = "argumentLabel";
    public static final String ARGUMENT_BONUS = "argumentBonus";

    public static final String TEMPLATE_NONE = "No Template";
    public static final String TEMPLATE_COUNTERPOISED = "Counterpoised";
    public static final String TEMPLATE_CELESTIAL = "Celestial";
    public static final String TEMPLATE_FIENDISH = "Fiendish";
    public static final String TEMPLATE_ENTROPIC = "Entropic";
    public static final String TEMPLATE_RESOLUTE = "Resolute";
    public static final String TEMPLATE_AERIAL = "Aerial";
    public static final String TEMPLATE_AQUEOUS = "Aqueous";
    public static final String TEMPLATE_CHTHONIC = "Chthonic";
    public static final String TEMPLATE_DARK = "Dark";
    public static final String TEMPLATE_FIERY = "Fiery";
    public static final String TEMPLATE_PRIMORDIAL = "Primordial";
    public static final String TEMPLATE_GIANT = "Giant";
    public static final String TEMPLATE_SHADOW = "Shadow";
    public static final String TEMPLATE_AUGMENTED = "Augmented";

    public static final int SUMMON_TYPE_SUMMON_MONSTER = 0;
    public static final int SUMMON_TYPE_SUMMON_NATURES_ALLY = 1;

    public static String[] SIZES = new String[] {"Fine", "Diminutive", "Tiny", "Small", "Medium", "Large", "Huge", "Gargantuan", "Colossal"};

    public static final Map<String, String> HASHMAP_DICE_INCREASE;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put("1", "1d2");
        aMap.put("1d2", "1d3");
        aMap.put("1d3", "1d4");
        aMap.put("1d4", "1d6");
        aMap.put("1d6", "1d8");
        aMap.put("1d8", "2d6");
        aMap.put("2d6", "3d6");
        aMap.put("3d6", "4d6");
        aMap.put("4d6", "6d6");
        aMap.put("6d6", "8d6");
        aMap.put("8d6", "12d6");

        aMap.put("1d10", "2d8");
        aMap.put("2d8", "3d8");
        aMap.put("3d8", "4d8");
        aMap.put("4d8", "6d8");
        aMap.put("6d8", "8d8");
        aMap.put("8d8", "12d8");

        aMap.put("2d4", "2d6");



        HASHMAP_DICE_INCREASE = Collections.unmodifiableMap(aMap);
    }

    public static final Map<String, String> HASHMAP_SPACE_INCREASE;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put("Tiny", "2 1/5 ft.");
        aMap.put("Small", "5 ft.");
        aMap.put("Medium", "5 ft.");
        aMap.put("Large", "10 ft.");
        aMap.put("Huge", "15 ft.");
        aMap.put("Gargantuan", "20 ft.");
        aMap.put("Colossal", "30 ft.");


        HASHMAP_SPACE_INCREASE = Collections.unmodifiableMap(aMap);
    }

    public static final Map<String, String> HASHMAP_REACH_INCREASE;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put("2 1/5 ft.", "5 ft.");
        aMap.put("5 ft.", "10 ft.");
        aMap.put("10 ft.", "15 ft.");
        aMap.put("15 ft.", "20 ft.");
        aMap.put("20 ft.", "25 ft.");
        aMap.put("25 ft.", "30 ft.");


        HASHMAP_REACH_INCREASE = Collections.unmodifiableMap(aMap);
    }


    public static final Map<String, String> HASHMAP_SOURCES;
    static {


        Map<String, String> aMap = new HashMap<>();
        aMap.put("AP 37","Pathfinder Adventure Path #37: Souls for Smuggler's Shiv Copyright 2010, Paizo Publishing, LLC");
        aMap.put("AP 56","Pathfinder Adventure Path #56: Raiders of the Fever Sea Copyright 2012, Paizo Publishing, LLC");
        aMap.put("AP 85","Pathfinder Adventure Path #85: Fires of Creation Copyright 2014, Paizo Inc");
        aMap.put("Animal Archive","Pathfinder Player Companion: Animal Archive Copyright 2013, Paizo Publishing, LLC");
        aMap.put("B1","Pathfinder RPG Bestiary, Copyright Paizo Publishing LLC 2009");
        aMap.put("B2","Pathfinder RPG Bestiary 2, Copyright Paizo Publishing LLC 2010");
        aMap.put("B3","Pathfinder RPG Bestiary 3, Copyright Paizo Publishing LLC 2011");
        aMap.put("B4","Pathfinder RPG Bestiary 4, Copyright Paizo Publishing LLC 2013");
        aMap.put("B5","Pathfinder RPG Bestiary 5, Copyright Paizo Publishing LLC 2015");
        aMap.put("Crypt Of The Everflame","Crypt Of The Everflame, Copyright Paizo Publishing LLC 2009");
        aMap.put("Familiar Folio","Familiar Folio, Copyright Paizo Publishing LLC 2013");
        aMap.put("Inner Sea Bestiary","Inner Sea Bestiary, Copyright Paizo Publishing LLC 2012");
        aMap.put("Inner Sea Gods","Inner Sea Gods, Copyright Paizo Publishing LLC 2014");
        aMap.put("PFS S3-26","PFS S3-26");
        aMap.put("Reign Of Winter Player's Guide","Reign Of Winter Player's Guide Copyright Paizo Publishing LLC 2013");
        aMap.put("Ultimate Magic","Pathfinder Roleplaying Game Ultimate Magic. Copyright 2011, Paizo Publishing, LLC");
        aMap.put("d20pfsrd","d20pfsrd");



        HASHMAP_SOURCES = Collections.unmodifiableMap(aMap);
    }
}
