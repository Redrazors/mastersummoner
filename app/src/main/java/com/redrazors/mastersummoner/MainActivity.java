package com.redrazors.mastersummoner;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.active.Summoner;
import com.redrazors.mastersummoner.data.DataControl;
import com.redrazors.mastersummoner.data.Evolution;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.TemplateInfo;
import com.redrazors.mastersummoner.dice.DiceRoller;
import com.redrazors.mastersummoner.views.TabControl;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity {

    public DataControl dataControl;

    public TabControl tabControl;
    public Summoner summoner;
    public DiceRoller diceRoller;
    private Gson mGson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Make to run your application only in portrait mode

        setContentView(R.layout.activity_main);



        GsonBuilder gsonBuilder = new GsonBuilder();
        mGson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create();

        dataControl = new DataControl(this);
        dataControl.loadData();

        summoner = new Summoner(this);
        diceRoller = new DiceRoller(this);

        if (savedInstanceState!=null){
            Log.w(Constants.TAG, "SAVED STATE not NULL");
            if (savedInstanceState.containsKey(Constants.ACTIVE_SUMMON)){
                List<String> list = savedInstanceState.getStringArrayList(Constants.ACTIVE_SUMMON);
                for (String json: list){
                    ActiveDetails activeDetails = mGson.fromJson(json, ActiveDetails.class);
                    if (dataControl.bestiary.hashMapMonsters.containsKey(activeDetails.name)){
                        Monster monster = dataControl.bestiary.hashMapMonsters.get(activeDetails.name);

                        TemplateInfo templateInfo = dataControl.hashMapTemplates.get(activeDetails.template);

                        activeDetails.setValuesAfterSavedState(dataControl.featFlags, monster, templateInfo);
                        summoner.hashMapSummons.put(activeDetails.id, activeDetails);

                        // activate active evolutions again
                        for (String evolutionName: activeDetails.listActiveEvolutions){

                            Evolution evolution = dataControl.hashMapEvolutions.get(evolutionName);
                            if (evolution!=null){
                                evolution.evolutionMethod.activateEvolution(true, monster, activeDetails);
                            }

                        }
                    }

                }
            }
        }


        tabControl = new TabControl(this);
        tabControl.initialiseControl();



    }



    public void openDialogFragment(DialogFragment dialogFragment, String tag){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(dialogFragment, tag);
        transaction.commitAllowingStateLoss();
    }



    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // add all the summons, with their hp

        ArrayList<String> list = new ArrayList<>();
        for (ActiveDetails activeDetails: summoner.hashMapSummons.values()){
            list.add(mGson.toJson(activeDetails));
        }
        savedInstanceState.putStringArrayList(Constants.ACTIVE_SUMMON, list);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
