package com.redrazors.mastersummoner.StatBlockParser;

import android.util.Log;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SkillParser {




    public static String ACROBATICS = "Acrobatics";
    public static String APPRAISE = "Appraise";
    public static String BLUFF = "Bluff";
    public static String CLIMB = "Climb";
    public static String CRAFT_A = "Craft (a)";
    public static String CRAFT_B = "Craft (b)";
    public static String CRAFT_C = "Craft (c)";

    public static String DIPLOMACY = "Diplomacy";
    public static String DISABLE_DEVICE = "Disable Device";
    public static String DISGUISE = "Disguise";

    public static String ESCAPE_ARTIST = "Escape Artist";
    public static String FLY = "Fly";
    public static String HANDLE_ANIMAL = "Handle Animal";
    public static String HEAL = "Heal";
    public static String INTIMIDATE = "Intimidate";

    public static String KNOWLEDGE_ARCANA = "Knowledge (arcana)";
    public static String KNOWLEDGE_DUNGEONEERING = "Knowledge (dungeoneering)";
    public static String KNOWLEDGE_ENGINEERING = "Knowledge (engineering)";
    public static String KNOWLEDGE_GEOGRAPHY = "Knowledge (geography)";
    public static String KNOWLEDGE_HISTORY = "Knowledge (history)";
    public static String KNOWLEDGE_LOCAL = "Knowledge (local)";
    public static String KNOWLEDGE_NATURE = "Knowledge (nature)";
    public static String KNOWLEDGE_NOBILITY = "Knowledge (nobility)";
    public static String KNOWLEDGE_PLANES = "Knowledge (planes)";
    public static String KNOWLEDGE_RELIGION = "Knowledge (religion)";

    public static String LINGUISTICS = "Linguistics";
    public static String PERCEPTION = "Perception";
    public static String PERFORM_A = "Perform";
    public static String PROFESSION_A = "Profession";

    public static String RIDE = "Ride";
    public static String SENSE_MOTIVE = "Sense Motive";
    public static String SLEIGHT_OF_HAND = "Sleight of Hand";
    public static String SPELLCRAFT = "Spellcraft";
    public static String STEALTH = "Stealth";
    public static String SURVIVAL = "Survival";
    public static String SWIM = "Swim";
    public static String USE_MAGIC_DEVICE = "Use Magic Device";

    public static final Map<String, Integer> HASHMAP_SKILLS;
    static {
        Map<String, Integer> aMap = new HashMap<>();

        // defense
        aMap.put(ACROBATICS,  1);
        aMap.put(APPRAISE,  3);
        aMap.put(BLUFF,  5);
        aMap.put(CLIMB,  0);
        aMap.put(CRAFT_A,  3);
        aMap.put(CRAFT_B,  3);
        aMap.put(CRAFT_C,  3);
        aMap.put(DIPLOMACY,  5);
        aMap.put(DISABLE_DEVICE,  1);
        aMap.put(DISGUISE,  5);
        aMap.put(ESCAPE_ARTIST,  1);
        aMap.put(FLY,  1);
        aMap.put(HANDLE_ANIMAL,  5);
        aMap.put(HEAL,  4);
        aMap.put(INTIMIDATE,  5);

        aMap.put(KNOWLEDGE_ARCANA,  3);
        aMap.put(KNOWLEDGE_DUNGEONEERING,  3);
        aMap.put(KNOWLEDGE_ENGINEERING,  3);
        aMap.put(KNOWLEDGE_GEOGRAPHY,  3);
        aMap.put(KNOWLEDGE_HISTORY,  3);
        aMap.put(KNOWLEDGE_LOCAL,  3);
        aMap.put(KNOWLEDGE_NATURE,  3);
        aMap.put(KNOWLEDGE_NOBILITY,  3);
        aMap.put(KNOWLEDGE_PLANES,  3);
        aMap.put(KNOWLEDGE_RELIGION,  3);
        aMap.put(LINGUISTICS,  3);
        aMap.put(PERCEPTION,  4);
        aMap.put(PERFORM_A,  5);
        aMap.put(PROFESSION_A,  4);
        aMap.put(RIDE,  1);
        aMap.put(SENSE_MOTIVE,  4);
        aMap.put(SLEIGHT_OF_HAND,  1);
        aMap.put(SPELLCRAFT,  3);
        aMap.put(STEALTH,  1);
        aMap.put(SURVIVAL,  4);
        aMap.put(SWIM,  0);
        aMap.put(USE_MAGIC_DEVICE,  5);


        HASHMAP_SKILLS = Collections.unmodifiableMap(aMap);
    }
    
    public static String parseSkills(ActiveDetails activeDetails, String skillBlock) {


        for (int abilityRef=0; abilityRef<6; abilityRef++){

            int bonus = (int)Math.floor((double)activeDetails.abilityIncrease[abilityRef]/2);
            if (bonus!=0){

                //Log.w(Constants.TAG, "CHANGING SKILLS FOR BONUS "+bonus + " ability ref "+abilityRef);

                for (String skillName: HASHMAP_SKILLS.keySet()){
                    int skillAbilityRef = HASHMAP_SKILLS.get(skillName);
                    if (abilityRef==skillAbilityRef){

                        // replace non capitals
                        if (skillBlock.contains(skillName.toLowerCase())){
                            skillBlock = skillBlock.replace(skillName.toLowerCase(), skillName);
                        }

                        String[] split = skillBlock.split(",");

                        for (String skillText: split){


                            // does this skillText contain the skill we're looking for
                            if (skillText.contains(skillName)){

                                String mutable = skillText.replace(skillName, "");
                                // recognise and save any bracketed text
                                String bracketText = "";
                                int skillBonus = 0;
                                if (mutable.contains("(") && mutable.contains(")")){
                                    try{
                                        bracketText = mutable.split("\\(")[1].split("\\)")[0];
                                        mutable = mutable.replace("("+bracketText+")", "").trim();
                                    } catch (ArrayIndexOutOfBoundsException e){
                                        e.printStackTrace();
                                    }
                                }

                                try{
                                    String bonusText="0";
                                    if (mutable.contains(" +")){
                                        bonusText = mutable.split("\\+")[1].trim();
                                    } else if (mutable.contains(" -")){
                                        bonusText = "-"+mutable.split("\\-")[1].trim();
                                    }

                                    skillBonus = Integer.parseInt(bonusText);
                                } catch (ArrayIndexOutOfBoundsException e){
                                    e.printStackTrace();
                                } catch (NumberFormatException e){
                                    e.printStackTrace();
                                }

                                skillBonus+=bonus;
                                String replacement = skillName;
                                if (bracketText.length()>0){
                                    replacement+= " ("+bracketText+")";
                                }
                                replacement+= " "+Utils.getSymbolAndAmount(skillBonus);

                                skillBlock = skillBlock.replace(skillText, replacement);


                                break;
                            }
                        }


                    }
                }

            }
        }

        return skillBlock;

    }
}
