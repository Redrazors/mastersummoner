package com.redrazors.mastersummoner.StatBlockParser;

import android.util.Log;

import com.redrazors.mastersummoner.Constants;

public class SpellTextParser {


    private static String[] slaLineTriggers = new String[]{" Constant", " At will", " At Will", " 1/day", " 2/day", " 3/day", " 4/day", " 5/day", " 6/day", " 7/day",
            " 8/day", " 9/day", " 10/day", " 11/day", " 12/day", " 13/day", " 14/day", " 15/day", " 16/day", " 17/day", " 18/day", " 1/week" };

    private static String[] spellLineTriggers = new String[]{" 0 (at will)", " 1st", " 2nd", " 3rd", "4th", " 5th", " 6th", " 7th", " 8th", " 9th"};
    private static String[] spellLineSplits = new String[]{" 0 \\(at will\\)", " 1st", " 2nd", " 3rd", "4th", " 5th", " 6th", " 7th", " 8th", " 9th"};

    public static String parseSLAText(String text){

        String topLine = text.split("\\)")[0];
        String rest = text.substring(topLine.length());
        for (String trigger: slaLineTriggers){
            rest = rest.replace(trigger, "</span></br><span class=\"spell-indent\">"+trigger);
        }

        // must happen after 1st parse so than span tag is there
        //Log.w(Constants.TAG, "CHECKING SPELLS");

        for (int i=0; i<slaLineTriggers.length; i++){
            if (rest.contains(slaLineTriggers[i])){
                try{
                    String spellsByLevel = rest.split(slaLineTriggers[i])[1].split("</span>")[0];

                    String replacement = getSpellLinkReplacement(spellsByLevel);

                    rest = rest.replace(spellsByLevel, replacement);

                } catch (ArrayIndexOutOfBoundsException e){
                    e.printStackTrace();
                }


            }

        }
        return topLine+rest;
    }

    public static String parseSpellText(String text){
        String topLine = text.split("\\)")[0];
        String rest = text.substring(topLine.length());




        for (String trigger: spellLineTriggers){
            rest = rest.replace(trigger, "</span></br><span class=\"spell-indent\">"+trigger);
        }

        // must happen after 1st parse so than span tag is there
        for (int i=0; i<spellLineTriggers.length; i++){
            if (rest.contains(spellLineTriggers[i])){
                try{
                    String spellsByLevel = rest.split(spellLineSplits[i])[1].split("</span>")[0];

                    String replacement = getSpellLinkReplacement(spellsByLevel);

                    rest = rest.replace(spellsByLevel, replacement);

                } catch (ArrayIndexOutOfBoundsException e){
                    e.printStackTrace();
                }


            }

        }


        return topLine+rest;
    }


    private static String getSpellLinkReplacement(String spellsByLevel){

        String splitFromThis=spellsByLevel;
        if (splitFromThis.contains("-")){
            splitFromThis = splitFromThis.split("-")[1];
        }


        for (String spellName: splitFromThis.split(", ")){

            spellName = spellName.split("\\(")[0].trim();

            //Log.w(Constants.TAG, spellName);
            if (spellName.contains(")")){
                //Log.w(Constants.TAG, "CONTAINS BRACKET: "+spellName);
            } else if (spellName.length()>0){
                String url = "http://aonprd.com/SpellDisplay.aspx?ItemName="+getOddBallAoNReplacements(spellName);

                spellsByLevel = spellsByLevel.replace(spellName, "<a href='"+url+"'>"+spellName+"</a>" );
            }

        }

        return spellsByLevel;
    }

    private static String getOddBallAoNReplacements(String spellName){

        if (spellName.contains("-")){
            spellName = spellName.replace("-", "");
        }

        if (spellName.equals("summon")){
            return "Summon%20Monster%201";
        }

        spellName = spellName.replace("empowered ", "");


        spellName = spellName.replace("'", "%27");

        if (spellName.contains("greater ")){
            spellName = spellName.replace("greater ","");
            spellName +=", greater";
        }

        if (spellName.contains("lesser ")){
            spellName = spellName.replace("lesser ","");
            spellName +=", lesser";
        }

        if (spellName.contains("mass ")){
            spellName = spellName.replace("mass ", "");
            spellName = spellName+", mass";
        }
        return spellName;
    }
}
