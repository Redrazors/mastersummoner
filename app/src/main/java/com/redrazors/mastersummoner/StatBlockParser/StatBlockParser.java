package com.redrazors.mastersummoner.StatBlockParser;


import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseDefense;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseEcology;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseHeader;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseOffense;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseSpecials;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseStatistics;
import com.redrazors.mastersummoner.active.ActiveDetails;

public class StatBlockParser {

    public static String STYLESHEET="<link rel=\"stylesheet\"href=\"PRD.css\">";
    public static String BREAKER_DEFENSE="<p class = \"stat-block-breaker\">Defense</p>";
    public static String BREAKER_OFFENSE="<p class = \"stat-block-breaker\">Offense</p>";
    public static String BREAKER_STATISTICS="<p class = \"stat-block-breaker\">Statistics</p>";
    public static String BREAKER_ECOLOGY="<p class = \"stat-block-breaker\">Ecology</p>";
    public static String BREAKER_SPECIALS="<p class = \"stat-block-breaker\">Special Abilities</p>";


    public static String getHTMLStatBlock(ActiveDetails activeDetails){

        String html = STYLESHEET;
        html+= ParseHeader.getTitle(activeDetails);
        html+=ParseHeader.getXPCR(activeDetails);
        html+=ParseHeader.getAlignmentType(activeDetails);
        html+=ParseHeader.getInitSenses(activeDetails);
        html+=ParseHeader.getAura(activeDetails);

        html+=BREAKER_DEFENSE;
        html+= ParseDefense.getACs(activeDetails);
        html+= ParseDefense.getHPsHD(activeDetails);
        html+= ParseDefense.getSaves(activeDetails);
        html+= ParseDefense.getExtraDefenses(activeDetails);
        html+= ParseDefense.getExtraWeaknesses(activeDetails);

        html+=BREAKER_OFFENSE;
        html+= ParseOffense.getSpeed(activeDetails);
        html+= ParseOffense.getMelee(activeDetails);
        html+= ParseOffense.getRanged(activeDetails);
        html+= ParseOffense.getSpaceAndReach(activeDetails);
        html+= ParseOffense.getSpecialAttacks(activeDetails);
        html+= ParseOffense.getSLAs(activeDetails);
        html+= ParseOffense.getSpellsPrepared(activeDetails);
        html+= ParseOffense.getSpellsKnown(activeDetails);
        html+= ParseOffense.getPsychicMagic(activeDetails);
        html+= ParseOffense.getOtherSpellBits(activeDetails);

        html+=BREAKER_STATISTICS;
        html+= ParseStatistics.getAbilities(activeDetails);
        html+= ParseStatistics.getBabCMB(activeDetails);
        html+= ParseStatistics.getFeats(activeDetails);
        html+= ParseStatistics.getSkills(activeDetails);
        html+= ParseStatistics.getLanguages(activeDetails);
        html+= ParseStatistics.getSQ(activeDetails);
        html+= ParseStatistics.getGear(activeDetails);


        if (showEcology(activeDetails)){
            html+= BREAKER_ECOLOGY;
            html+= ParseEcology.getEnvironment(activeDetails);
            html+= ParseEcology.getOrganization(activeDetails);
            html+= ParseEcology.getTreasure(activeDetails);
            html+= ParseEcology.getShortDescription(activeDetails);
        }

        if (showSpecials(activeDetails)){
            html+= BREAKER_SPECIALS;
            html+= ParseSpecials.getSpecialAbilities(activeDetails);
        }

        html+= getCopyright(activeDetails);



        html = UMRParser.parseUMRLinks(html);


        return html;

    }

    private static String getCopyright(ActiveDetails activeDetails){
        if (Constants.HASHMAP_SOURCES.containsKey(activeDetails.monster.source)){
            return  "<p class = \"stat-block-1\"><strong>Section 15 Copyright Notice</strong> "+Constants.HASHMAP_SOURCES.get(activeDetails.monster.source)+"</p>";
        }
        return "";
    }

    private static boolean showEcology(ActiveDetails activeDetails){
        if (activeDetails.monster.environment!=null){
            return true;
        }
        if (activeDetails.monster.organization!=null){
            return true;
        }
        if (activeDetails.monster.treasure!=null){
            return true;
        }
        if (activeDetails.monster.descShort!=null){
            if (activeDetails.monster.descShort.length()>0){
                return true;
            }
        }
        return false;
    }

    private static boolean showSpecials(ActiveDetails activeDetails){
        if (activeDetails.monster.specialAbi!=null){
            if (activeDetails.monster.specialAbi.length()>0){
                return true;
            }
        }
        if (activeDetails.templateInfo!=null){
            if (activeDetails.templateInfo.specialAbilities.length()>0){
                return true;
            }
        }
        return false;
    }

}
