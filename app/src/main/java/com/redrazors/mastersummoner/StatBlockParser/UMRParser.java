package com.redrazors.mastersummoner.StatBlockParser;

import com.redrazors.mastersummoner.StatBlockParser.parts.ParseDefense;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseHeader;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseOffense;
import com.redrazors.mastersummoner.StatBlockParser.parts.ParseStatistics;

public class UMRParser {

    public static String parseUMRLinks(String statBlock){

        try{
            String block = statBlock.split(StatBlockParser.BREAKER_DEFENSE)[0];
            String replacmenentBlock = block;
            for (String umr: ParseHeader.HASHMAP_URLS.keySet()){
                String url = ParseHeader.HASHMAP_URLS.get(umr);
                replacmenentBlock = replacmenentBlock.replace(umr, "<a href='"+url+"'>"+umr+"</a>" );
            }
            statBlock = statBlock.replace(block, replacmenentBlock);

        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        try{
            String block = statBlock.split(StatBlockParser.BREAKER_DEFENSE)[1].split(StatBlockParser.BREAKER_OFFENSE)[0];
            String replacmenentBlock = block;
            for (String umr: ParseDefense.HASHMAP_URLS.keySet()){
                String url = ParseDefense.HASHMAP_URLS.get(umr);
                replacmenentBlock = replacmenentBlock.replace(umr, "<a href='"+url+"'>"+umr+"</a>" );
            }
            statBlock = statBlock.replace(block, replacmenentBlock);

        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        try{
            String block = statBlock.split(StatBlockParser.BREAKER_OFFENSE)[1].split(StatBlockParser.BREAKER_STATISTICS)[0];
            String replacmenentBlock = block;
            for (String umr: ParseOffense.HASHMAP_URLS.keySet()){
                String url = ParseOffense.HASHMAP_URLS.get(umr);
                replacmenentBlock = replacmenentBlock.replace(umr, "<a href='"+url+"'>"+umr+"</a>" );
            }
            statBlock = statBlock.replace(block, replacmenentBlock);

        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        try{

            String block = statBlock.split(StatBlockParser.BREAKER_STATISTICS)[1].split("<p class = \"stat-block-breaker\">")[0];
            String replacmenentBlock = block;
            for (String umr: ParseStatistics.HASHMAP_URLS.keySet()){
                String url = ParseStatistics.HASHMAP_URLS.get(umr);
                replacmenentBlock = replacmenentBlock.replace(umr, "<a href='"+url+"'>"+umr+"</a>" );
            }
            statBlock = statBlock.replace(block, replacmenentBlock);

        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }















        return statBlock;
    }
}
