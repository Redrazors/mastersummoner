package com.redrazors.mastersummoner.StatBlockParser.parts;


import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParseDefense {

    public static final Map<String, String> HASHMAP_URLS;
    static {
        Map<String, String> aMap = new HashMap<>();

        // defense
        aMap.put("elemental traits", "http://aonprd.com/MonsterSubtypes.aspx?ItemName=Elemental");
        aMap.put("ooze traits", "http://aonprd.com/UMR.aspx?ItemName=Ooze Traits");
        aMap.put("construct traits", "http://aonprd.com/UMR.aspx?ItemName=Construct Traits");
        aMap.put("plant traits", "http://aonprd.com/UMR.aspx?ItemName=Plant Traits");
        aMap.put("undead traits", "http://aonprd.com/UMR.aspx?ItemName=Undead Traits");
        aMap.put("vulnerability", "http://aonprd.com/UMR.aspx?ItemName=Vulnerabilities");
        aMap.put("vulnerable", "http://aonprd.com/UMR.aspx?ItemName=Vulnerability");
        aMap.put("all-around vision", "http://aonprd.com/UMR.aspx?ItemName=All-Around Vision");
        aMap.put("amorphous", "http://aonprd.com/UMR.aspx?ItemName=Amorphous");
        aMap.put("block attacks", "http://aonprd.com/UMR.aspx?ItemName=Block Attacks");
        aMap.put("change shape", "http://aonprd.com/UMR.aspx?ItemName=Change Shape");
        aMap.put("ferocity", "http://aonprd.com/UMR.aspx?ItemName=Ferocity");
        aMap.put("fortification", "http://aonprd.com/UMR.aspx?ItemName=Fortification");
        aMap.put("hardness", "http://aonprd.com/UMR.aspx?ItemName=Hardness");
        aMap.put("incorporeal", "http://aonprd.com/UMR.aspx?ItemName=Incorporeal");
        aMap.put("light blindness", "http://aonprd.com/UMR.aspx?ItemName=Light Blindness");
        aMap.put("light sensitivity", "http://aonprd.com/UMR.aspx?ItemName=Light Sensitivity");
        aMap.put("natural invisibility", "http://aonprd.com/UMR.aspx?ItemName=Natural Invisibility");
        aMap.put("negative energy affinity", "http://aonprd.com/UMR.aspx?ItemName=Negative Energy Affinity");
        aMap.put("poisonous blood", "http://aonprd.com/UMR.aspx?ItemName=Poisonous Blood");
        aMap.put("regeneration", "http://aonprd.com/UMR.aspx?ItemName=Regeneration");
        aMap.put("rock catching", "http://aonprd.com/UMR.aspx?ItemName=Rock Catching");
        aMap.put("second save", "http://aonprd.com/UMR.aspx?ItemName=Second Save");
        aMap.put("split", "http://aonprd.com/UMR.aspx?ItemName=Split");
        aMap.put("sunlight powerlessness", "http://aonprd.com/UMR.aspx?ItemName=Sunlight Powerlessness");
        aMap.put("unstoppable", "http://aonprd.com/UMR.aspx?ItemName=Unstoppable");


        HASHMAP_URLS = Collections.unmodifiableMap(aMap);
    }

    public static String getACs(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\"><strong>AC</strong> "+activeDetails.getAC()+", touch "+activeDetails.getACTouch()
                +", flat-footed "+activeDetails.getACFlat()+" "+activeDetails.getACMods()+"</p>";
    }

    public static String getHPsHD(ActiveDetails activeDetails){
        return  "<p class = \"stat-block-1\"><strong>hp</strong> "+Integer.toString(activeDetails.getActiveMaxHP())+" ("+activeDetails.getHDFull()+") "+activeDetails.getHPMods()+"</p>";
    }

    public static String getSaves(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\"><strong>Fort</strong> "+ Utils.getSymbolAndAmount(activeDetails.getFort())
                +", <strong>Ref</strong> "+Utils.getSymbolAndAmount(activeDetails.getReflex())
                +", <strong>Will</strong> "+Utils.getSymbolAndAmount(activeDetails.getWill())+activeDetails.getSaveMods()+"</p>";
    }

    public static String getExtraDefenses(ActiveDetails activeDetails){
        String allDefenses = getDefensiveAbilities(activeDetails)+getResists(activeDetails)+getDR(activeDetails)+
                getImmune(activeDetails)+getSR(activeDetails);
        if (allDefenses.length()>0){
            return "<p class = \"stat-block-1\">"+allDefenses+"</p>";
        }
        return "";
    }

    private static String getDefensiveAbilities(ActiveDetails activeDetails){
        String string = activeDetails.getDefensiveAbilities();
        if (string.length()>0){
            return "<strong>Defensive Abilities</strong> "+string+"; ";
        }
        return "";
    }
    private static String getResists(ActiveDetails activeDetails){
        String string = activeDetails.getResist();
        if (string.length()>0){
            return "<strong>Resist</strong> "+string+"; ";
        }
        return "";
    }
    private static String getDR(ActiveDetails activeDetails){
        String string = activeDetails.getDR();
        if (string.length()>0){
            return "<strong>DR</strong> "+string+"; ";
        }
        return "";
    }
    private static String getImmune(ActiveDetails activeDetails){
        String string = activeDetails.getImmune();
        if (string.length()>0){
            return "<strong>Immune</strong> "+string+"; ";
        }
        return "";
    }
    private static String getSR(ActiveDetails activeDetails){
        String string = activeDetails.getSR();
        if (string.length()>0){
            return "<strong>SR</strong> "+string+"; ";
        }
        return "";
    }


    public static String getExtraWeaknesses(ActiveDetails activeDetails){
        String weaknesses = getWeaknesses(activeDetails)+getVulnerability(activeDetails);
        if (weaknesses.length()>0){
            return "<p class = \"stat-block-1\">"+weaknesses+"</p>";
        }
        return "";
    }

    private static String getWeaknesses(ActiveDetails activeDetails){
        String string = activeDetails.getWeaknesses();
        if (string.length()>0){
            return "<strong>Weaknesses</strong> "+string+"; ";
        }
        return "";
    }
    private static String getVulnerability(ActiveDetails activeDetails){
        String string = activeDetails.getVulnerabilities();
        if (string.length()>0){
            return "<strong>Vulnerability</strong> "+string+"; ";
        }
        return "";
    }


}
