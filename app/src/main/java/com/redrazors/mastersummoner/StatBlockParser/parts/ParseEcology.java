package com.redrazors.mastersummoner.StatBlockParser.parts;


import com.redrazors.mastersummoner.active.ActiveDetails;

public class ParseEcology {

    public static String getEnvironment(ActiveDetails activeDetails){
        if (activeDetails.monster.environment==null)return "";
        if (activeDetails.monster.environment.length()==0)return "";
        return "<p class = \"stat-block-1\">"+strong("Environment")+" "+activeDetails.monster.environment+"</p>";
    }

    public static String getOrganization(ActiveDetails activeDetails){
        if (activeDetails.monster.organization==null)return "";
        if (activeDetails.monster.organization.length()==0)return "";
        return "<p class = \"stat-block-1\">"+strong("Organization")+" "+activeDetails.monster.organization+"</p>";
    }

    public static String getTreasure(ActiveDetails activeDetails){
        if (activeDetails.monster.treasure==null)return "";
        if (activeDetails.monster.treasure.length()==0)return "";
        return "<p class = \"stat-block-1\">"+strong("Treasure")+" "+activeDetails.monster.treasure+"</p>";
    }

    public static String getShortDescription(ActiveDetails activeDetails){
        if (activeDetails.monster.descShort==null)return "";
        if (activeDetails.monster.descShort.length()==0)return "";
        return "<p class = \"stat-block-1\"><i>"+activeDetails.monster.descShort+"</i></p>";
    }

    private static String strong(String string){
        return "<strong>"+string+"</strong>";
    }
}
