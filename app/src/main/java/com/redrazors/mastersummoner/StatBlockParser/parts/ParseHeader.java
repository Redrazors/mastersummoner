package com.redrazors.mastersummoner.StatBlockParser.parts;


import com.redrazors.mastersummoner.active.ActiveDetails;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParseHeader {

    public static final Map<String, String> HASHMAP_URLS;
    static {
        Map<String, String> aMap = new HashMap<>();

        // header/senses/aura
        aMap.put("blindsense", "http://aonprd.com/UMR.aspx?ItemName=Blindsense");
        aMap.put("blindsight", "http://aonprd.com/UMR.aspx?ItemName=Blindsight");
        aMap.put("darkvision", "http://aonprd.com/UMR.aspx?ItemName=Darkvision");
        aMap.put("emotion", "http://aonprd.com/UMR.aspx?ItemName=Emotion Aura");
        aMap.put("fear aura", "http://aonprd.com/UMR.aspx?ItemName=Fear");
        aMap.put("frightful presence", "http://aonprd.com/UMR.aspx?ItemName=Frightful Presence");
        aMap.put("greensight", "http://aonprd.com/UMR.aspx?ItemName=Greensight");
        aMap.put("keen scent", "http://aonprd.com/UMR.aspx?ItemName=Keen Scent");
        aMap.put("lifesense", "http://aonprd.com/UMR.aspx?ItemName=Lifesense");
        aMap.put("low-light vision", "http://aonprd.com/UMR.aspx?ItemName=Low-Light Vision");
        aMap.put("mental static", "http://aonprd.com/UMR.aspx?ItemName=Mental Static Aura");
        aMap.put("mistsight", "http://aonprd.com/UMR.aspx?ItemName=Mistsight");
        aMap.put("scent", "http://aonprd.com/UMR.aspx?ItemName=Scent");
        aMap.put("see in darkness", "http://aonprd.com/UMR.aspx?ItemName=See in Darkness");
        aMap.put("stench", "http://aonprd.com/UMR.aspx?ItemName=Stench");
        aMap.put("thoughtsense", "http://aonprd.com/UMR.aspx?ItemName=Thoughtsense");
        aMap.put("tremorsense", "http://aonprd.com/UMR.aspx?ItemName=Tremorsense");
        aMap.put("unnatural aura", "http://aonprd.com/UMR.aspx?ItemName=Unnatural Aura");
        aMap.put("x-ray vision", "http://aonprd.com/UMR.aspx?ItemName=X-Ray Vision");
        aMap.put("true seeing", "http://aonprd.com/SpellDisplay.aspx?ItemName=True%20Seeing");


        HASHMAP_URLS = Collections.unmodifiableMap(aMap);
    }

    public static String getTitle(ActiveDetails activeDetails){
        return "<p class = \"stat-block-title\">"+activeDetails.getDisplayName()+"</p>";
    }

    public static String getXPCR(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\"><strong>XP</strong> "+activeDetails.getXP()+"</p>"; // left cr off for now " <strong>CR</strong> "+activeDetails.getCR()+
    }

    public static String getAlignmentType(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\">"+activeDetails.getAlignment()+" "+activeDetails.getSizeString()+" "+activeDetails.getTypeAndSubtype()+"</p>";
    }

    public static String getInitSenses(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\"><strong>Init</strong> "+activeDetails.getInitString()+"; <strong>Senses</strong> "+activeDetails.getSenses()+"</p>";
    }

    public static String getAura(ActiveDetails activeDetails){
        if (activeDetails.monster.aura!=null){
            return "<p class = \"stat-block-1\"><strong>Aura</strong> "+activeDetails.getAura()+"</p>";
        }
        return "";
    }


}
