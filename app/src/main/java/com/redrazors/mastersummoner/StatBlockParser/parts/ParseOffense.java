package com.redrazors.mastersummoner.StatBlockParser.parts;



import android.util.Log;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.StatBlockParser.SpellTextParser;
import com.redrazors.mastersummoner.active.ActiveDetails;



import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParseOffense {

    public static final Map<String, String> HASHMAP_URLS;
    static {
        Map<String, String> aMap = new HashMap<>();

        // offense
        aMap.put("amazing initiative", "http://aonprd.com/UMR.aspx?ItemName=Amazing Initiative");
        aMap.put("attach", "http://aonprd.com/UMR.aspx?ItemName=Attach");
        aMap.put("bleed", "http://aonprd.com/UMR.aspx?ItemName=Bleed");
        aMap.put("blood drain", "http://aonprd.com/UMR.aspx?ItemName=Blood Drain");
        aMap.put("blood rage", "http://aonprd.com/UMR.aspx?ItemName=Blood Rage");
        aMap.put("breath weapon", "http://aonprd.com/UMR.aspx?ItemName=Breath Weapon");
        aMap.put("burn", "http://aonprd.com/UMR.aspx?ItemName=Burn");
        aMap.put("capsize", "http://aonprd.com/UMR.aspx?ItemName=Capsize");
        aMap.put("constrict", "http://aonprd.com/UMR.aspx?ItemName=Constrict");
        aMap.put("distraction", "http://aonprd.com/UMR.aspx?ItemName=Distraction");
        aMap.put("energy drain", "http://aonprd.com/UMR.aspx?ItemName=Energy Drain");
        aMap.put("engulf", "http://aonprd.com/UMR.aspx?ItemName=Engulf");
        aMap.put("entrap", "http://aonprd.com/UMR.aspx?ItemName=Entrap");
        aMap.put("fast swallow", "http://aonprd.com/UMR.aspx?ItemName=Fast Swallow");
        aMap.put("fear aura", "http://aonprd.com/UMR.aspx?ItemName=Fear");
        aMap.put("gaze", "http://aonprd.com/UMR.aspx?ItemName=Gaze");
        aMap.put("grab", "http://aonprd.com/UMR.aspx?ItemName=Grab");
        aMap.put("heat", "http://aonprd.com/UMR.aspx?ItemName=Heat");
        aMap.put("multiweapon mastery", "http://aonprd.com/UMR.aspx?ItemName=Multiweapon Mastery");
        aMap.put("paralysis", "http://aonprd.com/UMR.aspx?ItemName=Paralysis");
        aMap.put("pounce", "http://aonprd.com/UMR.aspx?ItemName=Pounce");
        aMap.put("powerful charge", "http://aonprd.com/UMR.aspx?ItemName=Powerful Charge");
        aMap.put("pull", "http://aonprd.com/UMR.aspx?ItemName=Pull");
        aMap.put("push", "http://aonprd.com/UMR.aspx?ItemName=Push");
        aMap.put("rake", "http://aonprd.com/UMR.aspx?ItemName=Rake");
        aMap.put("rend", "http://aonprd.com/UMR.aspx?ItemName=Rend");
        aMap.put("rock throwing", "http://aonprd.com/UMR.aspx?ItemName=Rock Throwing");
        aMap.put("smother", "http://aonprd.com/UMR.aspx?ItemName=Smother");
        aMap.put("steal", "http://aonprd.com/UMR.aspx?ItemName=Steal");
        aMap.put("strangle", "http://aonprd.com/UMR.aspx?ItemName=Strangle");
        aMap.put("swallow whole", "http://aonprd.com/UMR.aspx?ItemName=Swallow Whole");
        aMap.put("trample", "http://aonprd.com/UMR.aspx?ItemName=Trample");
        aMap.put("trip", "http://aonprd.com/UMR.aspx?ItemName=Trip");
        aMap.put("web", "http://aonprd.com/UMR.aspx?ItemName=Web");
        aMap.put("whirlwind", "http://aonprd.com/UMR.aspx?ItemName=Whirlwind");


        HASHMAP_URLS = Collections.unmodifiableMap(aMap);
    }

    public static String getSpeed(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\"><strong>Speed</strong> "+activeDetails.getAllSpeeds()+"</p>";
    }

    public static String getMelee(ActiveDetails activeDetails){
        String melee = activeDetails.getAllMelee();

        if (melee.length()>0){
            return "<p class = \"stat-block-1\"><strong>Melee</strong> "+melee+"</p>";
        }
        return "";
    }

    public static String getRanged(ActiveDetails activeDetails){
        String ranged = activeDetails.getAllRanged();

        if (ranged.length()>0){
            return "<p class = \"stat-block-1\"><strong>Ranged</strong> "+ranged+"</p>";
        }
        return "";
    }

    public static String getSpaceAndReach(ActiveDetails activeDetails){
        String spaceReach = activeDetails.getSpace();
        if (spaceReach.length()>0){
            spaceReach="<strong>Space</strong> "+spaceReach+"; ";
        }

        String reach = activeDetails.getReach();
        if (reach.length()>0){
            spaceReach+="<strong>Reach</strong> "+reach+";";
        }
        if (spaceReach.length()>0){
            return "<p class = \"stat-block-1\">"+spaceReach+"</p>";
        }
        return "";


    }

    public static String getSpecialAttacks(ActiveDetails activeDetails){
        String specialAttacks = activeDetails.getSpecialAttacks();

        //Log.e(Constants.TAG, "SPECIAL ATTACKS IN PARSE OFFENSE: "+specialAttacks);
        if (specialAttacks.length()>0){
            return "<p class = \"stat-block-1\"><strong>Special Attacks</strong> "+specialAttacks+"</p>";
        }
        return "";
    }

    public static String getSLAs(ActiveDetails activeDetails){
        String slas = activeDetails.getSLA();

        if (slas.length()==0){
            return "";
        }

        slas = SpellTextParser.parseSLAText(slas);
        return "<p class = \"stat-block-1\"><strong>Spell-Like Abilities</strong><span> "+slas+"</span></p>";

    }

    public static String getSpellsPrepared(ActiveDetails activeDetails){
        String spells = activeDetails.getSpellsPrepared();

        if (spells.length()==0){
            return "";
        }

        spells = SpellTextParser.parseSpellText(spells);
        return "<p class = \"stat-block-1\"><strong>Spells Prepared</strong><span> "+spells+"</span></p>";

    }

    public static String getSpellsKnown(ActiveDetails activeDetails){
        String spells = activeDetails.getSpellsKnown();

        if (spells.length()==0){
            return "";
        }

        spells = SpellTextParser.parseSpellText(spells);
        return "<p class = \"stat-block-1\"><strong>Spells Known</strong><span> "+spells+"</span></p>";

    }

    public static String getPsychicMagic(ActiveDetails activeDetails){
        String spells = activeDetails.monster.psychicMagic;

        if (spells==null)return "";
        if (spells.length()==0)return "";

        spells = SpellTextParser.parseSpellText(spells);
        return "<p class = \"stat-block-1\"><strong>Psychic Magic</strong><span> "+spells+"</span></p>";

    }

    public static String getOtherSpellBits(ActiveDetails activeDetails){
        String otherBits="";
        if (exists(activeDetails.monster.spellDomains)){
            otherBits+="<p class=\"spell-indent\">Spell Domains "+activeDetails.monster.spellDomains+"</p>";
        }
        if (exists(activeDetails.monster.spirit)){
            otherBits+="<p class=\"spell-indent\">Spirit "+activeDetails.monster.spirit+"</p>";
        }
        if (exists(activeDetails.monster.mystery)){
            otherBits+="<p class=\"spell-indent\">Mystery "+activeDetails.monster.mystery+"</p>";
        }
        if (exists(activeDetails.monster.mystery)){
            otherBits+="<p class=\"spell-indent\">Mystery "+activeDetails.monster.mystery+"</p>";
        }
        if (exists(activeDetails.monster.kineticistWildTalents)){
            otherBits+="<p class=\"spell-indent\">"+activeDetails.monster.kineticistWildTalents+"</p>";
        }
        if (exists(activeDetails.monster.occultistImplements)){
            otherBits+="<p class=\"spell-indent\">"+activeDetails.monster.occultistImplements+"</p>";
        }
        if (exists(activeDetails.monster.psychicDiscipline)){
            otherBits+="<p class=\"spell-indent\">Psychic Discipline"+activeDetails.monster.psychicDiscipline+"</p>";
        }
        return otherBits;
    }



    private static boolean exists(String string){
        if (string==null){
            return false;
        }
        if (string.length()==0){
            return false;
        }
        return true;
    }
}
