package com.redrazors.mastersummoner.StatBlockParser.parts;

import android.util.Log;

import com.redrazors.mastersummoner.active.ActiveDetails;


public class ParseSpecials {

    public static String getSpecialAbilities(ActiveDetails activeDetails){
        return "<p class = \"stat-block-1\">"+emboldenTitles(activeDetails.getSpecialAbilities())+"</p>";

    }

    private static String emboldenTitles(String specialAbilities){
        String[] split = specialAbilities.split("\\.");

        for (String sentence: split){
            //Log.w("TAG", sentence);
            if (sentence.contains("(Su)")){
                String start = sentence.split("\\(Su\\)")[0];
                String emboldened = "</br></br><strong>"+start+"(Su)</strong>";
                specialAbilities = specialAbilities.replace(start+"(Su)", emboldened);
            } else if (sentence.contains("(Ex)")){
                String start = sentence.split("\\(Ex\\)")[0];
                String emboldened = "</br></br><strong>"+start+"(Ex)</strong>";
                specialAbilities = specialAbilities.replace(start+"(Ex)", emboldened);
            } else if (sentence.contains("(Sp)")){
                String start = sentence.split("\\(Sp\\)")[0];
                String emboldened = "</br></br><strong>"+start+"(Sp)</strong>";
                specialAbilities = specialAbilities.replace(start+"(Sp)", emboldened);
            }
        }

        String start = specialAbilities.substring(0, 10);
        if (start.equals("</br></br>")){
            specialAbilities = specialAbilities.substring(10);
        }
        return specialAbilities;
    }

    private static String strong(String string){
        return "<strong>"+string+"</strong>";
    }
}
