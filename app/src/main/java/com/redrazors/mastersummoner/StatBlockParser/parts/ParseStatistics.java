package com.redrazors.mastersummoner.StatBlockParser.parts;


import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParseStatistics {

    public static final Map<String, String> HASHMAP_URLS;
    static {
        Map<String, String> aMap = new HashMap<>();

        // statistics
        aMap.put("amphibious", "http://aonprd.com/UMR.aspx?ItemName=Amphibious");
        aMap.put("channel resistance", "http://aonprd.com/UMR.aspx?ItemName=Channel Resistance");
        aMap.put("compression", "http://aonprd.com/UMR.aspx?ItemName=Compression");
        aMap.put("earth glide", "http://aonprd.com/UMR.aspx?ItemName=Earth Glide");
        aMap.put("fast healing", "http://aonprd.com/UMR.aspx?ItemName=Fast Healing");
        aMap.put("freeze", "http://aonprd.com/UMR.aspx?ItemName=Freeze");
        aMap.put("hold breath", "http://aonprd.com/UMR.aspx?ItemName=Hold Breath");
        aMap.put("jet", "http://aonprd.com/UMR.aspx?ItemName=Jet");
        aMap.put("lycanthropic empathy", "http://aonprd.com/UMR.aspx?ItemName=Lycanthropic Empathy");
        aMap.put("no breath", "http://aonprd.com/UMR.aspx?ItemName=No Breath");
        aMap.put("plantbringer", "http://aonprd.com/UMR.aspx?ItemName=Plantbringer");
        aMap.put("powerful blows", "http://aonprd.com/UMR.aspx?ItemName=Powerful Blows");
        aMap.put("recuperation", "http://aonprd.com/UMR.aspx?ItemName=Recuperation");
        aMap.put("sound mimicry", "http://aonprd.com/UMR.aspx?ItemName=Sound Mimicry");
        aMap.put("telepathy", "http://aonprd.com/UMR.aspx?ItemName=Telepathy");
        aMap.put("undersized weapons", "http://aonprd.com/UMR.aspx?ItemName=Undersized Weapons");
        aMap.put("water breathing", "http://aonprd.com/UMR.aspx?ItemName=Water Breathing");
        aMap.put("water dependency", "http://aonprd.com/UMR.aspx?ItemName=Water Dependency");

        HASHMAP_URLS = Collections.unmodifiableMap(aMap);
    }
    private static String[] SHORT_NAMES = new String[]{"Str", "Dex", "Con", "Int", "Wis", "Cha"};

    public static String getAbilities(ActiveDetails activeDetails){
        String string =  "<p class = \"stat-block-1\">";

        for (int i=0; i<6; i++){
            string+=strong(SHORT_NAMES[i])+" "+activeDetails.getAbilityScore(i)+", ";
        }

        if (activeDetails.monster.abilityScoreMods!=null){
            if (activeDetails.monster.abilityScoreMods.length()>0){
                string+=" ("+activeDetails.monster.abilityScoreMods+")";
            }
        }

        string+="</p>";

        return string;
    }

    public static String getBabCMB(ActiveDetails activeDetails){
        String string =  "<p class = \"stat-block-1\">"+strong("Base Atk")+" "+ Utils.getSymbolAndAmount(activeDetails.monster.bab);

        string+="; "+strong("CMB ")+activeDetails.getCMBFull();
        string+="; "+strong("CMD ")+activeDetails.getCMDFull();
        string+="</p>";
        return string;
    }

    public static String getFeats(ActiveDetails activeDetails){
        String feats = activeDetails.getFeats();
        if (feats.length()>0){
            feats = getParsedFeats(feats);
            return "<p class = \"stat-block-1\">"+strong("Feats")+" "+feats+"</p>";
        }
        return "";
    }
    public static String getSkills(ActiveDetails activeDetails){
        String skills= activeDetails.getSkills();
        if (skills.length()>0){
            return "<p class = \"stat-block-1\">"+strong("Skills")+" "+skills+"</p>";
        }
        return "";
    }

    public static String getLanguages(ActiveDetails activeDetails){
        if (activeDetails.monster.languages==null)return "";
        if (activeDetails.monster.languages.length()==0)return "";
        return "<p class = \"stat-block-1\">"+strong("Languages")+" "+activeDetails.monster.languages+"</p>";

    }
    public static String getSQ(ActiveDetails activeDetails){
        if (activeDetails.monster.sq==null)return "";
        if (activeDetails.monster.sq.length()==0)return "";
        return "<p class = \"stat-block-1\">"+strong("SQ")+" "+activeDetails.monster.sq+"</p>";

    }

    public static String getGear(ActiveDetails activeDetails){
        if (activeDetails.monster.gear==null)return "";
        if (activeDetails.monster.gear.length()==0)return "";
        return "<p class = \"stat-block-1\">"+strong("Gear")+" "+activeDetails.monster.gear+"</p>";

    }
    private static String strong(String string){
        return "<strong>"+string+"</strong>";
    }

    private static String getParsedFeats(String feats){
        try{
            String[] featByFeat = feats.split(", ");

            String newFeatBlock = "";
            int counter=0;
            for (String feat: featByFeat){
                newFeatBlock += getFeatLinkReplacement(feat);
                counter++;
                if (counter<featByFeat.length){
                    newFeatBlock+=", ";
                }
            }

            return newFeatBlock;
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        return feats;
    }

    private static String getFeatLinkReplacement(String feat){

        String urlFeat = feat.split("\\(")[0];
        urlFeat = urlFeat.trim();

        String url = "http://aonprd.com/FeatDisplay.aspx?ItemName="+urlFeat;

        return "<a href='"+url+"'>"+feat+"</a>";
    }
}
