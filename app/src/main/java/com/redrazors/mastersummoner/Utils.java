package com.redrazors.mastersummoner;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

public class Utils {

    public static int getPxFromDP(Context context, int dp){
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );

        return px;
    }

    public static String addCommaIfNecessary(String string1, String string2){
        if (string1.length()>0 && string2.length()>0){
            return string1+", "+string2;
        }
        return string1+string2;
    }
    public static String getSymbol(int amount){
        if (amount>=0){
            return "+";
        }
        return "";
    }

    public static String getSymbolAndAmount(int amount){
        if (amount>=0){
            return "+"+Integer.toString(amount);
        }
        return Integer.toString(amount);
    }

    public static int getAbilityBonus(int ability){
        return (int) Math.ceil(((double) ability - 11) / 2);
    }
}
