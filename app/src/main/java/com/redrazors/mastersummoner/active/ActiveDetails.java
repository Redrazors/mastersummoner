package com.redrazors.mastersummoner.active;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.StatBlockParser.SkillParser;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.FeatFlags;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.TemplateInfo;
import com.redrazors.mastersummoner.staticmethods.AttackDamageStatics;
import com.redrazors.mastersummoner.staticmethods.SpecialAttackStatics;
import com.redrazors.mastersummoner.staticmethods.SpeedStatics;
import com.redrazors.mastersummoner.templates.TemplateControl;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ActiveDetails implements Serializable {

    public int sizeBonus=0;
    public int[] abilityIncrease;
    public int damageDiceIncrease=0;
    public double CRBonus=0;
    public int naturalACBonus=0;

    public Monster monster;

    private List<String> listAppliedTemplates = new ArrayList<>();

    @Expose
    public String id;
    @Expose
    public String name;
    @Expose
    public String addendum="";
    @Expose
    public int currentHP;
    @Expose
    public boolean activated;
    @Expose
    public String template;
    @Expose
    public int multipleRef=0;

    @Expose
    public boolean giantSummon;
    @Expose
    public boolean augmented;
    @Expose
    public boolean summonNeutral;
    @Expose
    public List<String> listActiveEvolutions = new ArrayList<>();
    @Expose
    public HashMap<String, Integer> hashMapEvolutionTimesTaken = new HashMap<>();



    public TemplateInfo templateInfo;
    public HashMap<Attack, AttackExtraInformation> hashMapAttackExtraInformation = new HashMap<>();

    private HashMap<String, Attack> hashMapEvolvedAttacks = new HashMap<>();

    private static final String[] NATURAL_PRIMARY_ATTACKS =new String[]{"bite", "tail", "gore", "sting", "tail slap", "kick", "quill"};


    public ActiveDetails(String id, String name, Monster monster, String template, int multipleRef,
                         boolean giantSummon, boolean summonNeutral, boolean augmented, TemplateInfo templateInfo){
        this.id=id;
        this.name = name;
        this.monster = monster;
        this.template = template;
        this.multipleRef = multipleRef;
        this.giantSummon = giantSummon;
        this.summonNeutral = summonNeutral;
        this.augmented = augmented;
        this.templateInfo = templateInfo;


        abilityIncrease = new int[6];

        setAugmented();


        setGiantSummon();

        restoreHP();

        for (Attack attack: monster.listMeleeAttacks){
            hashMapAttackExtraInformation.put(attack, new AttackExtraInformation());
        }
        for (Attack attack: monster.listRangedAttacks){
            hashMapAttackExtraInformation.put(attack, new AttackExtraInformation());
        }
    }

    private void setGiantSummon(){
        if (giantSummon){
            abilityIncrease[0]+=4;
            abilityIncrease[1]-=2;
            abilityIncrease[2]+=4;
            damageDiceIncrease++;
            naturalACBonus+=3;
            sizeBonus++;
            listAppliedTemplates.add(Constants.TEMPLATE_GIANT);
        }
    }

    public void setAugmented(){
        if (augmented){
            abilityIncrease[0]+=4;
            abilityIncrease[2]+=4;
            listAppliedTemplates.add(Constants.TEMPLATE_AUGMENTED);
        }
    }


    // use for savedstaterestore
    public void setValuesAfterSavedState(FeatFlags featFlags, Monster monster, TemplateInfo templateInfo){
        this.monster = monster;
        this.templateInfo = templateInfo;
        abilityIncrease =new int[6];
        listAppliedTemplates = new ArrayList<>();

        setAugmented();


        setGiantSummon();

        hashMapAttackExtraInformation = new HashMap<>();
        for (Attack attack: monster.listMeleeAttacks){
            hashMapAttackExtraInformation.put(attack, new AttackExtraInformation());
        }
        for (Attack attack: monster.listRangedAttacks){
            hashMapAttackExtraInformation.put(attack, new AttackExtraInformation());
        }

        hashMapEvolvedAttacks = new HashMap<>();
    }

    public void restoreHP(){
        currentHP = getActiveMaxHP();
    }

    public int getActiveMaxHP(){
        return monster.hp+getExtraAbilityBonus(2)*monster.getHD();
    }

    public int getExtraAbilityBonus(int ref){
        return (int)Math.floor((double) abilityIncrease[ref]/2);
    }

    public String getName(){
        return monster.name;
    }
    public String getXP(){
        DecimalFormat formatter = new DecimalFormat("#,###");
        return formatter.format(monster.xp);
    }
    public String getCR(){
        // todo
        return monster.cr;
    }
    public String getAlignment(){
        return monster.alignment;
    }

    public String getSizeString(){
        int size = monster.size+sizeBonus;

        int ref = size+2;
        if (ref<0)ref=0;
        if (ref>8)ref=8;

        return Constants.SIZES[ref];
    }
    public int getSize(){
        return monster.size+sizeBonus;
    }

    public String getTypeAndSubtype(){
        if (template.equals(Constants.TEMPLATE_SHADOW)){
            return "outsider"+getSubtype();
        }
        return monster.type+getSubtype();
    }

    private String getSubtype(){
        String subtype = TemplateControl.getTemplateSubtype(template);
        if (monster.subType!=null){
            if (subtype.length()>0)subtype+=", ";
            subtype+=monster.subType;
        }
        if (subtype.length()>0) return " ("+subtype+")";
        return "";
    }

    public int getInit(){
        return monster.init+getExtraAbilityBonus(1);
    }

    public String getInitString(){
        return Utils.getSymbolAndAmount(getInit());
    }
    public String getSenses(){
        return TemplateControl.getSenses(templateInfo, monster.senses);
    }

    public String getAura(){
        return monster.aura;
    }


    public String getACMods(){
        String acMods = monster.acMods;
        if (acMods.length()==0 && naturalACBonus!=0){
            return "";
        }
        if (acMods.contains("natural") && naturalACBonus>0){
            try{
                String[] splits = acMods.split(",");
                for (String split: splits){
                    if (split.contains("natural")){
                        String original = split.split("\\+")[1].split(" ")[0];
                        int newValue = Integer.parseInt(original);
                        newValue+=naturalACBonus;
                        String newSplit = split.replace(original+" natural", Integer.toString(newValue)+" natural");
                        acMods = acMods.replace(split, newSplit);
                    }
                }

            } catch (ArrayIndexOutOfBoundsException e){

            } catch (NumberFormatException e){

            }
        } else if (naturalACBonus>0){
            if (acMods.length()>0){
                acMods+=", ";
            }
            acMods+=Utils.getSymbolAndAmount(naturalACBonus)+" natural";
        }

        int extraDex = getExtraAbilityBonus(1);
        if (acMods.contains("Dex") && extraDex!=0){
            try{
                String[] splits = acMods.split(",");
                for (String split: splits){
                    if (split.contains("Dex")){
                        String original = split.split("\\+")[1].split(" ")[0];
                        int newValue = Integer.parseInt(original);
                        newValue+=extraDex;

                        String newSplit = split.replace(original+" Dex", Integer.toString(newValue)+" Dex");
                        acMods = acMods.replace(split, newSplit);
                    }
                }

            } catch (ArrayIndexOutOfBoundsException e){

            } catch (NumberFormatException e){

            }
        } else if (extraDex!=0){
            if (acMods.length()>0){
                acMods+=", ";
            }
            acMods+=Utils.getSymbolAndAmount(extraDex)+" Dex";
        }

        return "("+acMods+")";
    }

    public int getHP(){
        return monster.hp+getExtraAbilityBonus(2);
    }
    public String getHPMods(){
        if (monster.hp_mods==null)return "";
        return monster.hp_mods;
    }

    public String getHDFull(){
        int hpBonus = 0;
        if (monster.hd.contains("+")){
            try{
                String[] split = monster.hd.split("\\+");
                hpBonus =  Integer.parseInt(split[split.length-1]);
            } catch (NumberFormatException e){
                e.printStackTrace();
            }

        }
        hpBonus +=getExtraAbilityBonus(2)*monster.getHD();
        String adjusted = monster.hd.split("\\+")[0];
        if (hpBonus>0){
            adjusted+="+"+Integer.toString(hpBonus);
        }

        return adjusted;
    }

    public int getFort(){
        return monster.fort+getExtraAbilityBonus(2);
    }
    public int getReflex(){
        return monster.ref+getExtraAbilityBonus(1);
    }
    public int getWill(){

        int will =  monster.will+getExtraAbilityBonus(4);
        if (isSummonNeutralWillIncrease()){
            will+=2;
        }
        return will;
    }
    public String getSaveMods(){
        if (monster.saveMods==null || monster.saveMods.length()==0){
            return "";
        }
        return "; "+monster.saveMods;
    }

    public String getDefensiveAbilities(){
        if (monster.defensiveAbilities==null || monster.defensiveAbilities.length()==0){
            return "";
        }
        return monster.defensiveAbilities;
    }
    public String getResist(){
        return Utils.addCommaIfNecessary(monster.resist, TemplateControl.getTemplateResists(monster, templateInfo));
    }

    public String getDR(){
        return Utils.addCommaIfNecessary(monster.dr, TemplateControl.getTemplateDR(monster, templateInfo));
    }
    public String getImmune(){
        if (monster.immune==null || monster.immune.length()==0){
            return "";
        }
        return monster.immune;
    }
    public String getSR(){
        int sr = TemplateControl.getTemplateSR(monster, templateInfo, giantSummon);
        if (sr==0 && monster.sr==0){
            return "";
        }

        if (monster.sr>sr){
            sr = monster.sr;
        }
        return Integer.toString(sr);
    }

    public String getWeaknesses(){
        if (monster.weaknesses==null || monster.weaknesses.length()==0){
            return "";
        }
        return monster.weaknesses;
    }
    public String getVulnerabilities(){
        if (monster.vulnerability==null || monster.vulnerability.length()==0){
            return "";
        }
        return monster.vulnerability;
    }



    public String getAllSpeeds(){
        // todo template changes;
        String speeds= getSpeedText();

        if (monster.speedMod!=null && monster.speedMod.length()>0){
            speeds+= "("+monster.speedMod+")";
        }
        return speeds;
    }


    public String getAllMelee(){
        if (monster.melee.length()==0)return "";

        try{
            String allMelee = "";
            String[] splitByOr = monster.melee.split(" or ");
            int attackRef = 0;
            int orCount=0;
            for (String orSection: splitByOr){
                String[] splitByComma = orSection.split(",");

                for (int j=0; j<splitByComma.length; j++){
                    allMelee+= AttackDamageStatics.getAttackFullNameCompleteSequence(this, getListMeleeAttacks().get(attackRef));
                    attackRef++;
                    if (j+1<splitByComma.length){
                        allMelee+=", ";
                    }
                }
                orCount++;
                if (orCount<splitByOr.length){
                    allMelee+=" or ";
                }
            }

            for (Attack attack: hashMapEvolvedAttacks.values()){
                allMelee+= ", "+AttackDamageStatics.getAttackFullNameCompleteSequence(this, attack);
            }


            return allMelee;
        } catch (Exception e){
            e.printStackTrace();
        }
        return monster.melee;

    }
    public String getAllRanged(){
        if (monster.ranged.length()==0)return "";

        try{
            String allRanged = "";
            String[] splitByOr = monster.ranged.split(" or ");
            int attackRef = 0;
            int orCount=0;
            for (String orSection: splitByOr){
                String[] splitByComma = orSection.split(",");

                for (int j=0; j<splitByComma.length; j++){
                    allRanged+= AttackDamageStatics.getAttackFullNameCompleteSequence(this, getListRangedAttacks().get(attackRef));
                    attackRef++;
                    if (j+1<splitByComma.length){
                        allRanged+=", ";
                    }
                }
                orCount++;
                if (orCount<splitByOr.length){
                    allRanged+=" or ";
                }
            }


            return allRanged;
        } catch (Exception e){
            e.printStackTrace();
        }
        return monster.ranged;
    }
    public String getSpecialAttacks(){
        return SpecialAttackStatics.getSpecialAttacksString(this, templateInfo, monster);
    }

    public String getSLA(){
        if (monster.spellLikeAbilities==null)return "";
        return monster.spellLikeAbilities;
    }
    public String getSpellsPrepared(){
        if (monster.spellsPrepared==null)return "";
        return monster.spellsPrepared;
    }
    public String getSpellsKnown(){
        if (monster.spellsKnown==null)return "";
        return monster.spellsKnown;
    }

    public String getSpecialAbilities(){
        return TemplateControl.getSpecialAbilities(templateInfo, monster.specialAbi);
    }



    public String getAbilityScore(int ref){
        int[] scores = new int[]{monster.abiStr, monster.abiDex, monster.abiCon, monster.abiInt, monster.abiWis, monster.abiCha};
        if (scores[ref]<0){
            return "—";
        }
        int total = scores[ref]+ abilityIncrease[ref];

        return Integer.toString(total);
    }

    public int getCMB(){
        int cmb = Integer.parseInt(monster.cmb.split("\\(")[0].trim());

        if (monster.feats.toLowerCase().contains("agile maneuvers")){
            cmb+= getExtraAbilityBonus(1);
        } else {
            cmb+= getExtraAbilityBonus(0);
        }
        return cmb;
    }

    public String getCMBFull(){
        int cmb = Integer.parseInt(monster.cmb.split("\\(")[0].trim());

        int bonus=0;
        if (monster.feats.toLowerCase().contains("agile maneuvers")){
            bonus+= getExtraAbilityBonus(1);
        } else {
            bonus+= getExtraAbilityBonus(0);
        }
        String string = Utils.getSymbolAndAmount(cmb+bonus);
        if (monster.cmb.contains("(")){
            String part2 = monster.cmb.split("\\(")[1].split(" ")[0];
            try{
                int specialBonus = Integer.parseInt(part2);
                specialBonus+=bonus;
                String extra = monster.cmb.split("\\(")[1].replace(part2, Utils.getSymbolAndAmount(specialBonus));
                string+=" ("+extra;
            } catch (NumberFormatException e){
                    String extra =  monster.cmd.split("\\(")[1];
                    string+=" ("+extra;
            }

        }
        return string;
    }

    public int getCMD(){
        int cmd = Integer.parseInt(monster.cmd.split("\\(")[0].trim());

        cmd+= getExtraAbilityBonus(1);
        cmd+= getExtraAbilityBonus(0);

        return cmd;
    }

    public String getCMDFull(){
        int cmd = Integer.parseInt(monster.cmd.split("\\(")[0].trim());

        int bonus=0;
        cmd+= getExtraAbilityBonus(1);
        cmd+= getExtraAbilityBonus(0);

        String string = Utils.getSymbolAndAmount(cmd+bonus);
        if (monster.cmd.contains("(")){
            String part2 = monster.cmd.split("\\(")[1].split(" ")[0];
            try{
                int specialBonus = Integer.parseInt(part2);
                specialBonus+=bonus;
                String extra = monster.cmd.split("\\(")[1].replace(part2, Utils.getSymbolAndAmount(specialBonus));
                string+=" ("+extra;
            } catch (NumberFormatException e){
                String extra =  monster.cmd.split("\\(")[1];
                string+=" ("+extra;
            }



        }
        return string;
    }


    public String getFeats(){
        if (monster.feats==null)return "";
        return monster.feats;
    }
    public String getSkills(){
        // todo pump for increase
        if (monster.skills==null)return "";
        String skills = SkillParser.parseSkills(this, monster.skills);
        if (monster.skillMods!=null){
            skills+=" ("+monster.skillMods+")";
        }
        return skills;
    }

    public String getSpace(){
        String space = monster.space;
        if (giantSummon){
            String size = getSizeString();
            if (Constants.HASHMAP_SPACE_INCREASE.containsKey(size)){
                space = Constants.HASHMAP_SPACE_INCREASE.get(size);
            }

        }
        if (space.equals("5 ft.")){
            return "";
        }
        return space;
    }

    public String getReach(){
        String reach = monster.reach;
        if (giantSummon){
            if (monster.size!=1){
                if (Constants.HASHMAP_REACH_INCREASE.containsKey(reach)){
                    reach = Constants.HASHMAP_REACH_INCREASE.get(reach);
                }
            }


        }
        if (reach.equals("5 ft.")){
            return "";
        }
        return reach;
    }


    public List<Attack> getListMeleeAttacks(){
        List<Attack> listAttack = new ArrayList<>(monster.listMeleeAttacks);
        listAttack.addAll(hashMapEvolvedAttacks.values());
        return listAttack;
    }
    public List<Attack> getListRangedAttacks(){
        return monster.listRangedAttacks;
    }

    public void addEvolvedAttack(String name, Attack attack){
        //og.w(Constants.TAG, "ADDING EVOLVED ATTACK "+name);

        Attack biteAttack = getBiteAttack();
        if (name.contains("bite") && biteAttack!=null){
            hashMapAttackExtraInformation.get(biteAttack).evolvedBiteIncrease=true;

            int strBonus = Utils.getAbilityBonus(monster.abiStr);
            int str2h = (int)Math.floor((double)strBonus*1.5);
            int difference = str2h-strBonus;
            Log.w(Constants.TAG, "BITE BONUS IS "+difference);
            if (difference>0){
                // add the penalty
                hashMapAttackExtraInformation.get(biteAttack).evolvedStrengthModifier =difference;
            }

        } else {
            hashMapEvolvedAttacks.put(name, attack);
            hashMapAttackExtraInformation.put(attack, new AttackExtraInformation());
        }




        // check if there was a single two handed natural attack and flag it for reduction

        // is it a single attack
        if (monster.listMeleeAttacks.size()==1){
            Attack checkAttack = monster.listMeleeAttacks.get(0);

            // is it a 1.5 attack
            if (checkAttack.attackType ==Attack.ATK_TWOHANDED_OR_SINGLE_NATURAL){
                for (String naturalAttack: NATURAL_PRIMARY_ATTACKS){
                    // is it a natural attack
                    if (checkAttack.fullName.contains(naturalAttack)){

                        int strBonus = Utils.getAbilityBonus(monster.abiStr);
                        int str2h = (int)Math.floor((double)strBonus*1.5);
                        int difference = strBonus-str2h;
                        if (difference<0){
                            // add the penalty
                            hashMapAttackExtraInformation.get(checkAttack).evolvedStrengthModifier =difference;
                        }

                        break;
                    }
                }
            }


        }

    }
    public void removeEvolvedAttack(String name){
        Attack biteAttack = getBiteAttack();
        if (name.contains("bite") && biteAttack!=null){

            hashMapAttackExtraInformation.get(biteAttack).evolvedBiteIncrease=false;
            hashMapAttackExtraInformation.get(biteAttack).evolvedStrengthModifier =0;

        } else {
            Log.w(Constants.TAG, "REMOVING EVOLVED ATTACK, NAME: "+name);
            if (hashMapEvolvedAttacks.containsKey(name)){
                Log.w(Constants.TAG, "found in hashmap: ");
                hashMapAttackExtraInformation.remove(hashMapEvolvedAttacks.get(name));
                hashMapEvolvedAttacks.remove(name);

                //restore 2 handed natural attack
                if (hashMapEvolvedAttacks.size()==0 && monster.listMeleeAttacks.size()==1){
                    Attack checkAttack = monster.listMeleeAttacks.get(0);
                    hashMapAttackExtraInformation.get(checkAttack).evolvedStrengthModifier =0;
                }
            }
        }



    }

    private Attack getBiteAttack(){
        for (Attack attack: monster.listMeleeAttacks){
            if (attack.fullName.contains("bite")){
                return attack;
            }
        }
        return null;
    }

    public String getAttackFullNameAdjustedDice(Attack attack, int sequenceRef){
        String title = AttackDamageStatics.getAttackFullName(this, attack,sequenceRef, true, false);
        if (hashMapAttackExtraInformation.get(attack).increaseDice){
            String oldDice = Integer.toString(attack.damageDiceNumber)+"d"+Integer.toString(attack.damageDiceSize);

            Log.w(Constants.TAG, "OLD DICE: "+oldDice);
            if (Constants.HASHMAP_DICE_INCREASE.containsKey(oldDice)){
                title = title.replace(oldDice, Constants.HASHMAP_DICE_INCREASE.get(oldDice));
            }
        }
        return title;
    }



    public String getDisplayName(){
        String fullName = name+addendum;
        if (listAppliedTemplates.contains(Constants.TEMPLATE_AUGMENTED)){
            fullName+= " - Augmented";
        }
        if (!template.equals(Constants.TEMPLATE_NONE)){
            fullName =  template+" "+fullName;
        }
        if (listAppliedTemplates.contains(Constants.TEMPLATE_GIANT)){
            fullName = "Giant "+fullName;
        }
        return fullName;
    }

    public String getACString(){
        return "AC "+getAC()+", Touch "+getACTouch()+", Flat-Footed "+getACFlat();
    }

    public String getAC(){
        return Integer.toString(monster.ac+naturalACBonus+getExtraAbilityBonus(1));
    }
    public String getACTouch(){
        return Integer.toString(monster.acTouch+getExtraAbilityBonus(1));
    }
    public String getACFlat(){
        return Integer.toString(monster.acFlat+naturalACBonus);
    }



    public String getSpeedText(){

        SpeedSet speedSet = SpeedStatics.getSpeedSet(monster);
        String speed="";
        if (speedSet.walks()){
            speed+=Integer.toString(speedSet.speed+getPrimordialBonus())+" ft.";
        }

        if (speedSet.burrows() || template.equals(Constants.TEMPLATE_CHTHONIC)){
            speed = addCommaIfNecessary(speed);
            speed+="burrow "+getSpeedBurrow(speedSet)+" ft.";

            if (monster.speed.contains("earth glide")){
                speed+=" earth glide";
            }
        }
        if (speedSet.climbs()  || listActiveEvolutions.contains("Climb (Ex)")){
            speed = addCommaIfNecessary(speed);
            speed+="climb "+Integer.toString(getSpeedClimb(speedSet))+" ft.";
        }
        if (speedSet.flys() || template.equals(Constants.TEMPLATE_AERIAL)){
            speed = addCommaIfNecessary(speed);
            speed+="fly "+getSpeedFly(speedSet)+ " ft. ("+getFlyManeuverability(speedSet)+")";
        }
        if (speedSet.swims() || template.equals(Constants.TEMPLATE_AQUEOUS) || listActiveEvolutions.contains("Swim (Ex)")){
            speed = addCommaIfNecessary(speed);
            speed+="swim "+getSpeedSwim(speedSet) + " ft.";

            if (speedSet.jets()){
                speed+=", jet "+Integer.toString(speedSet.jet) + " ft.";
            }
        }

        return speed;

    }

    private int getPrimordialBonus(){
        if (template.equals(Constants.TEMPLATE_PRIMORDIAL)){
            return 10;
        }
        return 0;
    }

    private int getSpeedBurrow(SpeedSet speedSet){
        if (template.equals(Constants.TEMPLATE_CHTHONIC)){
            int burrow = (int)Math.floor((double)speedSet.getHighestSpeed()/2);
            if (burrow>speedSet.burrow){
                return burrow;
            }
        }
        return speedSet.burrow+getPrimordialBonus();
    }

    private int getSpeedClimb(SpeedSet speedSet){
        int climb =  speedSet.climb;
        if (listActiveEvolutions.contains("Climb (Ex)")){
            int evolvedClimn = speedSet.speed;
            if (hashMapEvolutionTimesTaken.containsKey("Climb (Ex)")){
                evolvedClimn += 20*(hashMapEvolutionTimesTaken.get("Climb (Ex)")-1);
            }
            if (evolvedClimn>climb){
                climb=evolvedClimn;
            }
        }
        return climb+getPrimordialBonus();
    }

    private int getSpeedFly(SpeedSet speedSet){
        if (template.equals(Constants.TEMPLATE_AERIAL)){
            int fly = speedSet.getHighestSpeed();
            if (fly>10*monster.getHD()){
                fly = 10*monster.getHD();
            }
            if (fly>speedSet.fly){
                return fly;
            }

        }
        return speedSet.fly+getPrimordialBonus();
    }
    private String getFlyManeuverability(SpeedSet speedSet){
        if (template.equals(Constants.TEMPLATE_AERIAL)){
            return "perfect";
        }
        return speedSet.maneuverability;
    }
    private int getSpeedSwim(SpeedSet speedSet){
        if (template.equals(Constants.TEMPLATE_AQUEOUS)){
            return speedSet.getHighestSpeed()+10;
        }

        int swim =  speedSet.swim;


        if (listActiveEvolutions.contains("Swim (Ex)")){
            int evolvedSwim = speedSet.speed;
            if (hashMapEvolutionTimesTaken.containsKey("Swim (Ex)")){
                evolvedSwim += 20*(hashMapEvolutionTimesTaken.get("Swim (Ex)")-1);
            }
            if (evolvedSwim>swim){
                swim=evolvedSwim;
            }
        }
        return swim+getPrimordialBonus();
    }





    private String addCommaIfNecessary(String speed){
        if (speed.length()==0){
            return speed;
        }
        return speed+", ";
    }


    private boolean isSummonNeutralWillIncrease(){
        if (template.equals(Constants.TEMPLATE_COUNTERPOISED) || summonNeutral){
            return true;
        }

        return false;
    }

    public int[] getAttackSequence(Attack attack){
        int[] sequence = new int[attack.attackSequence.length];

        sequence[0] = AttackDamageStatics.getAttackHitBonus(this, attack,  attack.attackSequence[0], false);

        for (int i=0; i<sequence.length; i++){
            sequence[i]=sequence[0]-5*i;
        }
        return sequence;
    }
}
