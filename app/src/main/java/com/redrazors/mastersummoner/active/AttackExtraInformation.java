package com.redrazors.mastersummoner.active;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class AttackExtraInformation implements Serializable {

    @Expose
    public boolean increaseDice;

    @Expose
    public int damageBonus=0;

    @Expose
    public int attackBonus=0;

    @Expose
    public boolean powerAttack;

    @Expose
    public boolean smite;

    @Expose
    public boolean vitalStrike;

    @Expose
    public boolean powerfulCharge;

    @Expose
    public int evolvedStrengthModifier =0; // this is the nenalty applied to single natural attacks if an additional evolved attack is added

    @Expose
    public boolean evolvedBiteIncrease=false;
}
