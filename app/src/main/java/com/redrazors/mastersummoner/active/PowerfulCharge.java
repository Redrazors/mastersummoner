package com.redrazors.mastersummoner.active;

public class PowerfulCharge {


    public String reqAttack="gore";
    public int numDice=2;
    public int diceSize=6;
    public int damageBonus=2;
    public double strengthMultiplier=1.5;


    public PowerfulCharge(String powerfulCharge){
        try{
            String[] split = powerfulCharge.split("&");

            reqAttack = split[0];
            String newDamageDice = split[1];

            numDice = Integer.parseInt(newDamageDice.split("d")[0]);
            diceSize = Integer.parseInt(newDamageDice.split("d")[1].split("\\+")[0]);
            damageBonus = Integer.parseInt(newDamageDice.split("\\+")[1]);

            strengthMultiplier = Double.parseDouble(split[2]);

        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
    }
}
