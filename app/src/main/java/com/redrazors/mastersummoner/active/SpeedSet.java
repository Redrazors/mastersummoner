package com.redrazors.mastersummoner.active;

public class SpeedSet {

    public int speed;
    public int swim;
    public int climb;
    public int burrow;
    public int fly;
    public int jet;
    public String maneuverability;

    public boolean walks(){
        if (speed>0)return true;
        return false;
    }

    public boolean swims(){
        if (swim>0)return true;
        return false;
    }

    public boolean jets(){
        if (jet>0)return true;
        return false;
    }

    public boolean climbs(){
        if (climb>0)return true;
        return false;
    }

    public boolean burrows(){
        if (burrow>0)return true;
        return false;
    }

    public boolean flys(){
        if (fly>0)return true;
        return false;
    }

    public int getHighestSpeed(){
        int[] speeds = new int[5];
        speeds[0]=speed;
        speeds[1]=swim;
        speeds[2]=burrow;
        speeds[3]=fly;
        speeds[4]=climb;

        int highest=0;
        for (int speed: speeds){
            if (speed>highest){
                highest=speed;
            }
        }
        return highest;
    }
}
