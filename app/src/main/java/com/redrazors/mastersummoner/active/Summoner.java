package com.redrazors.mastersummoner.active;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.SummonListItem;
import com.redrazors.mastersummoner.data.TemplateInfo;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Summoner {

    //private static String[] CODES = new String[]{" (a)", " (b)", " (c)", " (d)"};

    final static String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private MainActivity mainActivity;
    public HashMap<String, ActiveDetails> hashMapSummons = new HashMap<>();

    public Summoner (MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    public void summonMonster(String name, int count, String template, boolean giantSummon, boolean augmented){


        int highestRef = getCurrentMultipleRef(name);
        for (int i=0; i<count; i++){
            String id = UUID.randomUUID().toString();

            int thisMultipleRef = highestRef+i+1;
            Monster monster = mainActivity.dataControl.bestiary.hashMapMonsters.get(name);



            TemplateInfo templateInfo = mainActivity.dataControl.hashMapTemplates.get(template);
            ActiveDetails activeDetails = new ActiveDetails(id, name, monster, template,
                    thisMultipleRef, giantSummon, isSummonNeutral(name), augmented, templateInfo);
            if (count>1 || highestRef>0 && thisMultipleRef>0 && thisMultipleRef<=26){
                activeDetails.addendum=" ("+getCharForNumber(thisMultipleRef)+")";
            }

            hashMapSummons.put(id, activeDetails);
        }


        mainActivity.tabControl.getMViewPager().setCurrentItem(1, true);
        mainActivity.tabControl.getFragmentActive().updateSummons();

        // add to the list of current summoned monsters
        // update the active fragment
    }

    private boolean isSummonNeutral(String name){
        if (mainActivity.dataControl.featFlags.hashMapFlags.get(Constants.FEAT_NEUTRAL)){
            // is it from the neutral list or has the counterpoised template
            for (List<SummonListItem> listSummonListItems: mainActivity.dataControl.hashMapSummonListItems.get(Constants.FEAT_NEUTRAL).values()){
                for (SummonListItem summonListItem: listSummonListItems){
                    if (summonListItem.name.equals(name)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char)(i + 64)) : null;
    }

    private int getCurrentMultipleRef(String name){
        int multipleRef=0;
        for (ActiveDetails activeDetails: hashMapSummons.values()){
            if (activeDetails.name.equals(name)){
                if (activeDetails.multipleRef>multipleRef){
                    multipleRef  = activeDetails.multipleRef;
                }
            }
        }
        return multipleRef;
    }
}
