package com.redrazors.mastersummoner.data;


import android.util.Log;

import com.redrazors.mastersummoner.Constants;

import java.io.Serializable;

public class Attack implements Serializable {


	public static final int ATK_SPECIAL =0; // melee no strength to hit or damage
	public static final int ATK_OFF_HAND_OR_SECONDARY=1;  // str to hit, 1/2 str to damage
	public static final int ATK_MELEE=2; // str to hit and damage
	public static final int ATK_TWOHANDED_OR_SINGLE_NATURAL=3; // str to hit, 1.5xstr to dam
	public static final int ATK_RANGED=4; // no str to hit or damage
	public static final int ATK_RANGED_COMPOSITE_OR_THROWN=5; // no str to hit, 1x str to damage
	public static final int ATK_RANGED_GIANT=6; // no str to hit, 1.5 str to damage


	public int attackSequence[];
	public String fullName="";

	public int damageDiceNumber=1;
	public int damageDiceSize =6;
	public int damageBonus=0;
	public String magicBonus="";

	public ElementalDice elementalDice;
	public int critRange=20;
	public int critMultiplier = 2;


	public int attackType = ATK_MELEE;

	public boolean noDamage;
	public boolean singleDamage;

	public int multipleNatural=0;



	
	public Attack(String fullName){
		this.fullName = fullName;
	}

	public void setDamageDice(String damageString){

		if (damageString.equals("0")){
			noDamage=true;
			return;
		}

		if (damageString.equals("1")){
		    singleDamage=true;
		    return;
        }

		try{
			String[] split = damageString.split("d");
			damageDiceNumber = Integer.parseInt(split[0]);


			String[] diceSizeAndBonus = split[1].split("\\+|\\-");
			damageDiceSize = Integer.parseInt(diceSizeAndBonus[0]);
			if (diceSizeAndBonus.length>1){
				if (damageString.contains("+")){
					damageBonus=Integer.parseInt(diceSizeAndBonus[1]);
				} else if (damageString.contains("-")){
					damageBonus-=Integer.parseInt(diceSizeAndBonus[1]);
				}

			}
		} catch (NumberFormatException e){
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e){
		    e.printStackTrace();
            Log.w(Constants.TAG, "ERROR WITH "+damageString);
        }
	}

	public void setElementalDice(String elementalString){
		try{
			String[] split = elementalString.split("&");

			String type = split[0];

			String[] diceDetails = split[1].split("d");

			int diceNumber = Integer.parseInt(diceDetails[0]);
			int diceSize = Integer.parseInt(diceDetails[1]);

			elementalDice = new ElementalDice(type, diceNumber, diceSize, false);

		} catch (ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
		} catch (NumberFormatException e){
			e.printStackTrace();
		}


	}
//
	public void setAttackSequence(String sequence){
		String[] split = sequence.split("\\/");
		attackSequence = new int[split.length];
		//augmentedAttackSequence = new int[split.length];
		try{
			for (int i=0; i<split.length; i++){
				attackSequence[i]=Integer.parseInt(split[i]);
				//augmentedAttackSequence[i]=attackSequence[i]+augmentToHit;
			}
		} catch (NumberFormatException e){
			e.printStackTrace();
		}
	}




	// used by the roll all dialog
	public int[] getAttackSequenceOrNaturalMultiples(){

		if (multipleNatural>attackSequence.length){
			int[] multiples = new int[multipleNatural];
			for (int i=0; i<multipleNatural; i++){
				multiples[i]=attackSequence[0];
			}
			return multiples;
		}
        return attackSequence;
    }


    public void setMagicBonus(String bonus){
		magicBonus = "+"+bonus+" ";
	}

	public String getMagicBonus(){
		return magicBonus;
	}






}
