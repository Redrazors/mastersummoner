package com.redrazors.mastersummoner.data;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Root(name="Root")
public class Bestiary {

	@ElementList(inline=true, name="Row")
	public List<Monster> listMonsters = new ArrayList();

	public HashMap<String, Monster> hashMapMonsters = new HashMap<>();

	public void createHashMap(){
		for (Monster monster: listMonsters){
			hashMapMonsters.put(monster.name, monster);
		}
	}
	
}
