package com.redrazors.mastersummoner.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.StatBlockParser.StatBlockParser;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.staticmethods.ShadowCaller;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by david on 28/08/2018.
 */

public class DataControl {

    private MainActivity mainActivity;

    public FeatFlags featFlags;

    public HashMap<String, HashMap<Integer, List<SummonListItem>>> hashMapSummonListItems = new HashMap<>();

    public HashMap<String, HashMap<Integer, List<SummonListItem>>> hashMapNatureListItems = new HashMap<>();

    public HashMap<String, TemplateInfo> hashMapTemplates =new HashMap<>();

    public HashMap<String, Evolution> hashMapEvolutions = new HashMap<>();

    private List<SummonListItem> listMinorSummons = new ArrayList<>();

    public Bestiary bestiary;


    private static final String[] MINOR_MONSTERS = new String[]{
            "Archaeopteryx", "Arctic Fox", "Arctic Hare", "Arctic Tern", "Armadillo", "Blue-Ringed Octopus", "Cat", "Chicken", "Compsognathus",
            "Ermine", "Flying Squirrel", "Fox", "Hawk", "Lizard", "Mole", "Monkey", "Otter", "Owl", "Petromin", "Platypus", "Poisonous Frog", "Porcupine",
            "Ptarmigan", "Pufferfish", "Rabbit", "Raccoon", "Rat", "Raven", "Rhamphorhynchus", "Skunk", "Sloth", "Snapping Turtle", "Snowy Owl", "Tuatara",
            "Turtle", "Hagfish", "Viper", "Weasel"};


    public DataControl(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        featFlags = new FeatFlags();

        // load feat flags from memory
        SharedPreferences sharedPref = mainActivity.getSharedPreferences(Constants.PREFERENCES_FEAT_FLATS, Context.MODE_PRIVATE);
        for (String flagName: featFlags.hashMapFlags.keySet()){
            featFlags.hashMapFlags.put(flagName, sharedPref.getBoolean(flagName, false));
        }
        featFlags.augmented =  sharedPref.getBoolean(Constants.FEAT_AUGMENTED, false);
        featFlags.superior =  sharedPref.getBoolean(Constants.FEAT_SUPERIOR, false);
        featFlags.versatile =  sharedPref.getBoolean(Constants.FEAT_VERSATILE, false);

        featFlags.modeSummonMonster = sharedPref.getBoolean(Constants.MODE_SUMMON_MONSTER, true);

        featFlags.noSound = sharedPref.getBoolean(Constants.OPTION_SOUND, false);
        featFlags.noDice = sharedPref.getBoolean(Constants.OPTION_DICE, false);

        featFlags.giantSummoner = sharedPref.getBoolean(Constants.EQUIPMENT_ROD_GIANT, false);

        featFlags.evolved = sharedPref.getBoolean(Constants.FEAT_EVOLVED, false);



    }



    public List<SummonListItem> getSummonList(int summonLevel){
        List<SummonListItem> listItems = new ArrayList<>();

        if (summonLevel==0){
            return listMinorSummons;
        } else if (featFlags.modeSummonMonster){
            listItems.addAll(hashMapSummonListItems.get(Constants.STANDARD_LIST).get(summonLevel));

            for (String flagName: featFlags.hashMapFlags.keySet()){
                if (featFlags.hashMapFlags.get(flagName)){

                    if (flagName.equals(Constants.FEAT_EXPANDED)){
                        SharedPreferences preferences = mainActivity.getSharedPreferences(Constants.PREFERENCES_EXPANDED_SUMMONS, Context.MODE_PRIVATE);
                        for (SummonListItem summonListItem: hashMapSummonListItems.get(flagName).get(summonLevel)){
                            boolean available = preferences.getBoolean(summonListItem.name, true);
                            if (!listItems.contains(summonListItem) && available){
                                listItems.add(summonListItem);
                            }
                        }

                    } else if (hashMapSummonListItems.containsKey(flagName)) {
                        for (SummonListItem summonListItem: hashMapSummonListItems.get(flagName).get(summonLevel)){
                            if (!listItems.contains(summonListItem)){
                                listItems.add(summonListItem);
                            }
                        }
                    }
                }
            }

            if (featFlags.hashMapFlags.get(Constants.ARCHETYPE_SHADOW_CALLER)){
                listItems = ShadowCaller.getPurifiedList(summonLevel, listItems);
            }

        } else {
            listItems.addAll(hashMapNatureListItems.get(Constants.STANDARD_LIST).get(summonLevel));

            for (String flagName: featFlags.hashMapFlags.keySet()){
                if (featFlags.hashMapFlags.get(flagName)){
                    for (SummonListItem summonListItem: hashMapNatureListItems.get(flagName).get(summonLevel)){
                        if (!listItems.contains(summonListItem)){
                            listItems.add(summonListItem);
                        }
                    }
                }
            }
        }


        Collections.sort(listItems, new Comparator<SummonListItem>() {
            @Override
            public int compare(SummonListItem a1, SummonListItem a2) {
                return a1.name.compareToIgnoreCase(a2.name);
            }
        });

        return listItems;
    }


    public void loadData(){

        hashMapSummonListItems.put(Constants.STANDARD_LIST, new HashMap<Integer, List<SummonListItem>>());

        hashMapSummonListItems.put(Constants.FEAT_EXPANDED, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.FEAT_ALTERNATIVE, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.FEAT_GOOD, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.FEAT_NEUTRAL, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.FEAT_EVIL, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.FEAT_SPIDER, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.FEAT_SKELETON, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.ARCHETYPE_SHADOW_CALLER, new HashMap<Integer, List<SummonListItem>>());

        hashMapSummonListItems.put(Constants.RING_AEON, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_AGATHION, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_ANGEL, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_ARCHON, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_ASURA, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_DAEMON, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_DIV, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_INEVITABLE, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_KYTON, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_PROTEAN, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_PSYCOPOMP, new HashMap<Integer, List<SummonListItem>>());
        hashMapSummonListItems.put(Constants.RING_QLIPPOTH, new HashMap<Integer, List<SummonListItem>>());

        for (HashMap<Integer, List<SummonListItem>> hashMap: hashMapSummonListItems.values()){
            for (int i=1; i<=9; i++){
                hashMap.put(i, new ArrayList<SummonListItem>());
            }
        }

        hashMapNatureListItems.put(Constants.STANDARD_LIST, new HashMap<Integer, List<SummonListItem>>());
        hashMapNatureListItems.put(Constants.FEAT_ALTERNATIVE, new HashMap<Integer, List<SummonListItem>>());
        hashMapNatureListItems.put(Constants.FEAT_PLANT, new HashMap<Integer, List<SummonListItem>>());
        hashMapNatureListItems.put(Constants.FEAT_SPIDER, new HashMap<Integer, List<SummonListItem>>());

        hashMapNatureListItems.put(Constants.RING_DRAKE, new HashMap<Integer, List<SummonListItem>>());
        hashMapNatureListItems.put(Constants.RING_KAMI, new HashMap<Integer, List<SummonListItem>>());
        hashMapNatureListItems.put(Constants.RING_LESHY, new HashMap<Integer, List<SummonListItem>>());

        for (HashMap<Integer, List<SummonListItem>> hashMap: hashMapNatureListItems.values()){
            for (int i=1; i<=9; i++){
                hashMap.put(i, new ArrayList<SummonListItem>());
            }
        }

        setSummonerLists();

        setNatureLists();

        loadBestiary(mainActivity);

        addMonsterAttacks();

        setTemplateLists();

        setEvolutionLists();
        EvolutionMethodSetter.setEvolutionMethods(this);


        getNaturalAttackMultiples();

        for (String minor: MINOR_MONSTERS){
            if (bestiary.hashMapMonsters.containsKey(minor)){
                SummonListItem summonListItem = new SummonListItem(minor);
                summonListItem.template=true;
                listMinorSummons.add(summonListItem);
            }

        }
        Collections.sort(listMinorSummons, new Comparator<SummonListItem>() {
            @Override
            public int compare(SummonListItem a1, SummonListItem a2) {
                return a1.name.compareToIgnoreCase(a2.name);
            }
        });

        //testMonsterAttackNames();

        //checkDuplicateAttacks();

        //checkMonstersExist();

        //checkWillSave();

        //checkFortSave();

        //checkRefSave();

        //checkCMB();

        //checkSingleNaturalAttack();

        // check statblocks
        //checkStatBlocks();

        //spoutSpeeds();

        //checkSources();

//        for (HashMap<Integer, List<SummonListItem>> hashMap: hashMapNatureListItems.values()){
//            if (hashMap.isEmpty()){
//                Log.e(Constants.TAG, "HASHMAP EMPTY ");
//            }
//        }
//
//        for (HashMap<Integer, List<SummonListItem>> hashMap: hashMapSummonListItems.values()){
//            if (hashMap.isEmpty()){
//                Log.e(Constants.TAG, "HASHMAP EMPTY ");
//            }
//        }
    }

    private void checkSources(){
        List<String> listSources = new ArrayList<>();
        for (Monster monster: bestiary.hashMapMonsters.values()){
            if (!listSources.contains(monster.source)){
                listSources.add(monster.source);
            }
        }
        Collections.sort(listSources);
        for (String source: listSources){
            System.out.println(source);
        }
    }

    private void checkSource(){
        for (int summonLevel =1; summonLevel<=9; summonLevel++){
            List<SummonListItem> list = getSummonList(summonLevel);
            for (SummonListItem summonListItem: list){
                Monster monster = bestiary.hashMapMonsters.get(summonListItem.name);
                if (!standardBestiary(monster.source)){
                    Log.w(Constants.TAG,"Monster: "+monster.name+ " : "+monster.source);
                }
            }
        }
    }

    private boolean standardBestiary(String src){
        if (src.equals("B1") || src.equals("B2") || src.equals("B3") ||
                src.equals("B4") || src.equals("B5")){
            return true;
        }
        return false;
    }

    private void spoutSpeeds(){
        for (Monster monster: bestiary.hashMapMonsters.values()){
            Log.w(Constants.TAG, monster.speed);
        }
    }

    private void checkStatBlocks(){
        for (Monster monster: bestiary.hashMapMonsters.values()){
            Log.w(Constants.TAG, "CHECKING STATBLOCK: "+monster.name);
            ActiveDetails activeDetails = new ActiveDetails(null, monster.name, monster, Constants.TEMPLATE_NONE,
                    0, false, false, false, null);
            String statBlock = StatBlockParser.getHTMLStatBlock(activeDetails);
        }
    }


    private void loadBestiary(MainActivity mainActivity){

        try{
            InputStream is = mainActivity.getResources().openRawResource(R.raw.master_summoner_monsters);

            Serializer ser = new Persister();
            bestiary = ser.read(Bestiary.class, is);
            bestiary.createHashMap();

        } catch (Exception e){
            e.printStackTrace();
        }
    }




    private void getNaturalAttackMultiples(){
        for (Monster monster: bestiary.hashMapMonsters.values()){
            for (Attack attack: monster.listMeleeAttacks){
                char c = attack.fullName.charAt(0);
                boolean isDigit = (c >= '0' && c <= '9');
                if (isDigit){
                    try{
                        attack.multipleNatural = Integer.parseInt(String.valueOf(c));
                    } catch (NumberFormatException e){
                        e.printStackTrace();
                    }
//                    if (attack.attackSequence.length>1){
//                        Log.w(Constants.TAG, "ERROR ATTACK SEQUENCE PAST 1");
//                    }
//                    Log.w(Constants.TAG, attack.fullName);
                }
            }
            for (Attack attack: monster.listRangedAttacks){
                char c = attack.fullName.charAt(0);
                boolean isDigit = (c >= '0' && c <= '9');
                if (isDigit){
                    try{
                        attack.multipleNatural = Integer.parseInt(String.valueOf(c));
                    } catch (NumberFormatException e){
                        e.printStackTrace();
                    }
//                    if (attack.attackSequence.length>1){
//                        Log.w(Constants.TAG, "ERROR ATTACK SEQUENCE PAST 1");
//                    }
//                    Log.w(Constants.TAG, attack.fullName);
                }
            }
        }
    }

    private void checkSingleNaturalAttack(){
        for (Monster monster: bestiary.hashMapMonsters.values()){
            if (monster.listMeleeAttacks.size()==1){
                Attack attack = monster.listMeleeAttacks.get(0);
                if (attack.attackType !=3){
                    Log.w(Constants.TAG, "CHECK AUGMENT: "+monster.name);
                }
            }
        }
    }


    private void checkMonstersExist(){
        Log.w(Constants.TAG, "CHECKING MONSTER LISTS");

        for (String key: hashMapSummonListItems.keySet()){
            for (List<SummonListItem> summonListItems: hashMapSummonListItems.get(key).values()){
                for (SummonListItem summonListItem: summonListItems){
                    if (!bestiary.hashMapMonsters.containsKey(summonListItem.name)){
                        Log.w(Constants.TAG, summonListItem.name);
                    }
                }
            }
        }

        for (String key: hashMapNatureListItems.keySet()){
            for (List<SummonListItem> summonListItems: hashMapNatureListItems.get(key).values()){
                for (SummonListItem summonListItem: summonListItems){
                    if (!bestiary.hashMapMonsters.containsKey(summonListItem.name)){
                        Log.w(Constants.TAG, summonListItem.name);
                    }
                }
            }
        }

    }

//    private void testMonsterAttackNames(){
////        for (Monster monster: hashMapMonsters.values()){
////            for (Attack attack: monsterInfo.listMeleeAttacks){
////                String fullName = attack.getFullName(true);
////                Log.w(Constants.TAG, fullName);
////            }
////            for (Attack attack: monsterInfo.listRangedAttacks){
////                String fullName = attack.getFullName(true);
////                Log.w(Constants.TAG, fullName);
////            }
////        }
////    }
//
//    private void checkDuplicateAttacks(){
//        for (Monster monster: hashMapMonsters.values()){
//            outbreak:
//            for (Attack attack: monsterInfo.listMeleeAttacks){
//                for (Attack checkAttack: monsterInfo.listMeleeAttacks){
//                    if (!attack.getFullName(false).equals(checkAttack.getFullName(false))){
//                        if (attack.getFullName(false).contains(checkAttack.getFullName(false))){
//                            Log.w(Constants.TAG, "CHECK: "+monsterInfo.name);
//                            break outbreak;
//                        }
//                    }
//
//                }
//            }
//
//        }
//    }

    private void setNatureLists(){
        XmlPullParserFactory pullParserFactory;

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(mainActivity.getResources().openRawResource(R.raw.data_nature), null);

            parseNatureListXML(parser);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void parseNatureListXML(XmlPullParser parser) throws XmlPullParserException, IOException {


        int eventType = parser.getEventType();

        SummonListItem summonListItem = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            String tag = null;


            switch (eventType) {

                case XmlPullParser.START_DOCUMENT:
                    break;

                case XmlPullParser.START_TAG:

                    tag = parser.getName();

                    if (tag.equals("name")) {
                        summonListItem = new SummonListItem(parser.nextText());
                    } else if (tag.equals("standard")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.STANDARD_LIST).get(level).add(summonListItem);
                    } else if (tag.equals("nonstandard")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.FEAT_ALTERNATIVE).get(level).add(summonListItem);
                    } else if (tag.equals("src")) {
                        summonListItem.source=parser.nextText();
                    } else if (tag.equals("plant")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.FEAT_PLANT).get(level).add(summonListItem);
                    } else if (tag.equals("spider")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.FEAT_SPIDER).get(level).add(summonListItem);
                    } else if (tag.equals("ringDrake")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.RING_DRAKE).get(level).add(summonListItem);
                    } else if (tag.equals("ringKami")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.RING_KAMI).get(level).add(summonListItem);
                    } else if (tag.equals("ringLeshy")) {
                        int level = Integer.parseInt(parser.nextText());
                        hashMapNatureListItems.get(Constants.RING_LESHY).get(level).add(summonListItem);
                    }

                    break;
            }
            eventType = parser.next();
        }

    }


    private void setSummonerLists(){
        XmlPullParserFactory pullParserFactory;

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(mainActivity.getResources().openRawResource(R.raw.data_summoner), null);

            parseSummonListXML(parser);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void parseSummonListXML(XmlPullParser parser) throws XmlPullParserException, IOException {


        int eventType = parser.getEventType();

        SummonListItem summonListItem = null;



        while (eventType != XmlPullParser.END_DOCUMENT) {

            String tag = null;


            switch (eventType) {

                case XmlPullParser.START_DOCUMENT:
                    break;

                case XmlPullParser.START_TAG:

                    tag = parser.getName();

                    if (tag.equals("name")) {
                        summonListItem = new SummonListItem(parser.nextText());
                    } else if (tag.equals("standard")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.STANDARD_LIST, summonListItem, level);
                    } else if (tag.equals("nonstandard")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_ALTERNATIVE, summonListItem, level);
                    } else if (tag.equals("expanded")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_EXPANDED, summonListItem, level);
                    } else if (tag.equals("template")) {
                        summonListItem.template=true;
                    } else if (tag.equals("src")) {
                        summonListItem.source=parser.nextText();
                    } else if (tag.equals("good")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_GOOD, summonListItem, level);
                    } else if (tag.equals("neutral")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_NEUTRAL, summonListItem, level);
                    } else if (tag.equals("evil")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_EVIL, summonListItem, level);
                    } else if (tag.equals("spider")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_SPIDER, summonListItem, level);
                    } else if (tag.equals("skeleton")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.FEAT_SKELETON, summonListItem, level);
                    } else if (tag.equals("shadow")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.ARCHETYPE_SHADOW_CALLER, summonListItem, level);
                    } else if (tag.equals("ringAeon")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_AEON, summonListItem, level);
                    } else if (tag.equals("ringAgathion")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_AGATHION, summonListItem, level);
                    } else if (tag.equals("ringAnel")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_ANGEL, summonListItem, level);
                    } else if (tag.equals("ringArchon")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_ARCHON, summonListItem, level);
                    } else if (tag.equals("ringAsura")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_ASURA, summonListItem, level);
                    } else if (tag.equals("ringDaemon")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_DAEMON, summonListItem, level);
                    } else if (tag.equals("ringDiv")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_DIV, summonListItem, level);
                    } else if (tag.equals("ringInevitable")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_INEVITABLE, summonListItem, level);
                    } else if (tag.equals("ringKyton")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_KYTON, summonListItem, level);
                    } else if (tag.equals("ringProtean")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_PROTEAN, summonListItem, level);
                    } else if (tag.equals("ringPyschopomp")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_PSYCOPOMP, summonListItem, level);
                    } else if (tag.equals("ringQlippoth")) {
                        int level = Integer.parseInt(parser.nextText());
                        addToHashMapAtLevel(Constants.RING_QLIPPOTH, summonListItem, level);
                    }

                    break;
            }
            eventType = parser.next();
        }

    }

    private void addToHashMapAtLevel(String key, SummonListItem summonListItem, int level){
        hashMapSummonListItems.get(key).get(level).add(summonListItem);
    }

    private void addMonsterAttacks() {


        XmlPullParserFactory pullParserFactory;

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(mainActivity.getResources().openRawResource(R.raw.master_summoner_attacks), null);

            parseMonsterXML(parser);

        } catch (XmlPullParserException e) {


            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void parseMonsterXML(XmlPullParser parser) throws XmlPullParserException, IOException {



        int eventType = parser.getEventType();

        Monster monsterInfo = null;
        String name="NOT INITIALISED";
        Attack attack = null;


        while (eventType != XmlPullParser.END_DOCUMENT) {

            String tag = null;


            switch (eventType) {

                case XmlPullParser.START_DOCUMENT:
                    break;

                case XmlPullParser.START_TAG:

                    tag = parser.getName();


                    try{

                        if (tag.equals("name")) {
                            name = parser.nextText();
                            monsterInfo = bestiary.hashMapMonsters.get(name);


                        } else if (tag.equals("melee")) {
                            monsterInfo.melee = parser.nextText();
                        }else if (tag.equals("ranged")) {
                            monsterInfo.ranged = parser.nextText();
                        } else if (tag.equals("ma_0")) {
                            attack = new Attack(parser.nextText());
                            monsterInfo.listMeleeAttacks.add(attack);
                        }else if (tag.equals("mp_0")) {
                            attack.setMagicBonus(parser.nextText());
                        } else if (tag.equals("ms_0")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("md_0")) {
                            attack.setDamageDice(parser.nextText());
                        }else if (tag.equals("mel_0")) {
                            attack.setElementalDice(parser.nextText());
                        } else if (tag.equals("mcr_0")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("mcm_0")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("mt_0")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ma_1")) {
                            attack = new Attack(parser.nextText());
                            monsterInfo.listMeleeAttacks.add(attack);
                        } else if (tag.equals("mp_1")) {
                            attack.setMagicBonus(parser.nextText());
                        }else if (tag.equals("ms_1")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("md_1")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("mel_1")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("mcr_1")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("mcm_1")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("mt_1")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ma_2")) {
                            attack = new Attack(parser.nextText());
                            monsterInfo.listMeleeAttacks.add(attack);
                        } else if (tag.equals("ms_2")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("md_2")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("mel_2")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("mcr_2")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("mcm_2")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("mt_2")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ma_3")) {
                            attack = new Attack(parser.nextText());
                            monsterInfo.listMeleeAttacks.add(attack);
                        } else if (tag.equals("ms_3")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("md_3")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("mel_3")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("mcr_3")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("mcm_3")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("mt_3")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ma_4")) {
                            attack = new Attack(parser.nextText());
                            monsterInfo.listMeleeAttacks.add(attack);
                        } else if (tag.equals("ms_4")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("md_4")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("mel_4")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("mcr_4")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("mcm_4")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("mt_4")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ma_5")) {
                            attack = new Attack(parser.nextText());
                            monsterInfo.listMeleeAttacks.add(attack);
                        } else if (tag.equals("ms_5")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("md_5")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("mel_5")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("mcr_5")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("mcm_5")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("mt_5")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ra_0")) {
                            attack = new Attack(parser.nextText());
                            attack.attackType =2;
                            monsterInfo.listRangedAttacks.add(attack);
                        } else if (tag.equals("rp_0")) {
                            attack.setMagicBonus(parser.nextText());
                        }else if (tag.equals("rs_0")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("rd_0")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("rel_0")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("rcr_0")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("rcm_0")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("rt_0")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }

                        else if (tag.equals("ra_1")) {
                            attack = new Attack(parser.nextText());
                            attack.attackType =2;
                            monsterInfo.listRangedAttacks.add(attack);
                        } else if (tag.equals("rs_1")) {
                            attack.setAttackSequence(parser.nextText());
                        } else if (tag.equals("rd_1")) {
                            attack.setDamageDice(parser.nextText());
                        } else if (tag.equals("rel_1")) {
                            attack.setElementalDice(parser.nextText());
                        }else if (tag.equals("rcr_1")) {
                            attack.critRange = Integer.parseInt(parser.nextText());
                        }  else if (tag.equals("rcm_1")) {
                            attack.critMultiplier = Integer.parseInt(parser.nextText());
                        } else if (tag.equals("rt_1")) {
                            attack.attackType = Integer.parseInt(parser.nextText());
                        }



                    } catch (NumberFormatException e){
                        e.printStackTrace();
                        Log.w(Constants.TAG, "ERROR WITH "+name);
                    }  catch (NullPointerException e){
                        e.printStackTrace();
                    }


                    break;
            }
            eventType = parser.next();
        }

    }


    private void setTemplateLists(){
        XmlPullParserFactory pullParserFactory;

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(mainActivity.getResources().openRawResource(R.raw.data_templates), null);

            parseTemplateXML(parser);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void parseTemplateXML(XmlPullParser parser) throws XmlPullParserException, IOException {


        int eventType = parser.getEventType();

        TemplateInfo templateInfo = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            String tag = null;


            switch (eventType) {

                case XmlPullParser.START_DOCUMENT:
                    break;

                case XmlPullParser.START_TAG:

                    tag = parser.getName();

                    if (tag.equals("template")) {
                        templateInfo = new TemplateInfo();
                        hashMapTemplates.put(parser.nextText(), templateInfo);

                    } else if (tag.equals("senses")) {
                        templateInfo.senses = parser.nextText();
                    } else if (tag.equals("dr5")) {
                        templateInfo.dr5 = parser.nextText();
                    }else if (tag.equals("dr10")) {
                        templateInfo.dr10 = parser.nextText();
                    }else if (tag.equals("resisttype")) {
                        templateInfo.setResistType(parser.nextText());
                    }else if (tag.equals("resistvalue0")) {
                        templateInfo.resistValue[0] = parser.nextText();
                    }else if (tag.equals("resistvalue5")) {
                        templateInfo.resistValue[1] = parser.nextText();
                    }else if (tag.equals("resistvalue10")) {
                        templateInfo.resistValue[2] = parser.nextText();
                    }else if (tag.equals("sr")) {
                        templateInfo.sr =Integer.parseInt(parser.nextText());
                    } else if (tag.equals("specialAttacks")) {
                        templateInfo.specialAttack = parser.nextText();
                    } else if (tag.equals("specialAbilities")) {
                        templateInfo.specialAbilities = parser.nextText();
                    }

                    break;
            }
            eventType = parser.next();
        }

    }


    private void setEvolutionLists(){
        XmlPullParserFactory pullParserFactory;

        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(mainActivity.getResources().openRawResource(R.raw.data_evolutions), null);

            parseEvolutionXML(parser);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void parseEvolutionXML(XmlPullParser parser) throws XmlPullParserException, IOException {


        int eventType = parser.getEventType();

        String name = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            String tag = null;


            switch (eventType) {

                case XmlPullParser.START_DOCUMENT:
                    break;

                case XmlPullParser.START_TAG:

                    tag = parser.getName();

                    if (tag.equals("name")) {
                        name =parser.nextText();


                    } else if (tag.equals("description")) {
                        Evolution evolution = new Evolution(name, parser.nextText());
                        hashMapEvolutions.put(name, evolution);
                    } else if (tag.equals("multi")) {
                        Evolution evolution = hashMapEvolutions.get(name);
                        evolution.multi=true;
                    }

                    break;
            }
            eventType = parser.next();
        }

    }
}
