package com.redrazors.mastersummoner.data;

import java.io.Serializable;

public class ElementalDice implements Serializable {

    public static final String ELEMENT_FIRE = "fire";
    public static final String ELEMENT_COLD = "cold";
    public static final String ELEMENT_ELECTRICITY = "electricity";
    public static final String ELEMENT_ACID = "acid";

    public int diceSize=6;
    public int diceNumber=1;
    public String type=ELEMENT_FIRE;
    public boolean singleDamage;


    public ElementalDice(String type, int diceNumber, int diceSize, boolean singleDamage){
        this.type = type;
        this.diceSize = diceSize;
        this.diceNumber = diceNumber;
        this.singleDamage = singleDamage;
    }
}
