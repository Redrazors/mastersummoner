package com.redrazors.mastersummoner.data;

import com.redrazors.mastersummoner.active.ActiveDetails;


public class Evolution {

    public String name;
    public String description;
    public boolean multi;
    public EvolutionMethod evolutionMethod = new EvolutionMethod() {
        @Override
        public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {
            // to be overwritten with effects as required
        }
    };

    public Evolution(String name, String description){
        this.name = name;
        this.description = description;
    }


    public interface EvolutionMethod{

        void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails);
    }

}
