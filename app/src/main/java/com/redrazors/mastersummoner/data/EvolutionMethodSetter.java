package com.redrazors.mastersummoner.data;


import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;


public class EvolutionMethodSetter {


    public static void setEvolutionMethods(DataControl dataControl){

        bite(dataControl);
        claws(dataControl);
        hooves(dataControl);
        pincers(dataControl);
        slam(dataControl);
        sting(dataControl);
        tailSlap(dataControl);
        tentacle(dataControl);
        wingBuffet(dataControl);
        naturalArmor(dataControl);
    }

    private static void bite(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Bite (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {


                String attackName = "bite";
                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 6;
                    if (size==3){
                        damageDiceSize=8;
                    } else if (size>3){
                        damageDiceNumber=2;
                        damageDiceSize=6;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, false, 1, "bite");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void claws(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Claws (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {

                int num = 2;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Claws (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Claws (Ex)");
                }

                String attackName = Integer.toString(num)+" claws";
                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 4;
                    if (size==3){
                        damageDiceSize=6;
                    } else if (size>3){
                        damageDiceSize=8;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, false, num, "claw");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void hooves(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Hooves (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {

                int num = 2;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Hooves (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Hooves (Ex)");
                }

                String attackName = Integer.toString(num)+" hooves";

                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 4;
                    if (size==3){
                        damageDiceSize=6;
                    } else if (size>3){
                        damageDiceSize=8;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, true, num, "hoove");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void pincers(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Pincers (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {

                int num = 2;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Pincers (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Pincers (Ex)");
                }

                String attackName = Integer.toString(num)+" pincers";

                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 6;
                    if (size==3){
                        damageDiceSize=8;
                    } else if (size>3){
                        damageDiceNumber=2;
                        damageDiceSize=6;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, true, num, "pincer");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void slam(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Slam (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {

                int num = 1;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Slam (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Slam (Ex)");
                }

                String attackName = "slam";
                if (num>1){
                    attackName = Integer.toString(num)+ " slams";
                }

                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 8;
                    if (size==3){
                        damageDiceNumber=2;
                        damageDiceSize=6;
                    } else if (size>3){
                        damageDiceNumber=2;
                        damageDiceSize=8;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, false, num, "slam");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void sting(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Sting (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {


                int num = 1;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Sting (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Sting (Ex)");
                }

                String attackName = "sting";
                if (num>1){
                    attackName = Integer.toString(num)+ " stings";
                }

                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 4;
                    if (size==3){
                        damageDiceSize=6;
                    } else if (size>3){
                        damageDiceSize=8;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, false, num, "sting");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }


    private static void tailSlap(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Tail Slap (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {


                int num = 1;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Tail Slap (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Tail Slap (Ex)");
                }

                String attackName = "tail slap";
                if (num>1){
                    attackName = Integer.toString(num)+ " tail slaps";
                }

                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 6;
                    if (size==3){
                        damageDiceSize=8;
                    } else if (size>3){
                        damageDiceNumber=2;
                        damageDiceSize=6;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, true, num, "tail slap");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void tentacle(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Tentacle (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {

                int num = 1;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Tentacle (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Tentacle (Ex)");
                }

                String attackName = "tentacle";
                if (num>1){
                    attackName = Integer.toString(num)+ " tentacles";
                }

                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 4;
                    if (size==3){
                        damageDiceSize=6;
                    } else if (size>3){
                        damageDiceSize=8;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, true, num, "tentacle");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void wingBuffet(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Wing Buffet (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {


                String attackName = "2 wing buffets";
                if (activate){

                    int size = activeDetails.getSize();
                    int damageDiceNumber = 1;
                    int damageDiceSize = 4;
                    if (size==3){
                        damageDiceSize=6;
                    } else if (size>3){
                        damageDiceSize=8;
                    }
                    addAttack(attackName, damageDiceNumber, damageDiceSize, monster, activeDetails, true, 2, "wing buffet");
                } else {
                    activeDetails.removeEvolvedAttack(attackName);
                }

            }
        };
    }

    private static void naturalArmor(DataControl dataControl){
        dataControl.hashMapEvolutions.get("Improved Natural Armor (Ex)").evolutionMethod = new Evolution.EvolutionMethod() {
            @Override
            public void activateEvolution(boolean activate, Monster monster, ActiveDetails activeDetails) {


                int num = 1;
                if (activeDetails.hashMapEvolutionTimesTaken.containsKey("Improved Natural Armor (Ex)")){
                    num=num*activeDetails.hashMapEvolutionTimesTaken.get("Improved Natural Armor (Ex)");
                }

                int acBonus = 2*num;

                if (activate){

                    activeDetails.naturalACBonus+=acBonus;

                } else {

                    activeDetails.naturalACBonus-=acBonus;
                }

            }
        };
    }

    private static void addAttack(String attackName, int damageDiceNumber, int damageDiceSize, Monster monster, ActiveDetails activeDetails,
                                  boolean secondaryAttack, int multipleNatural, String attackType){

        int attackBonus = getAttackBonus(monster, attackType);
        int damageBonus = Utils.getAbilityBonus(monster.abiStr);
        if (secondaryAttack){
            attackBonus-=5;
            damageBonus = (int)Math.floor((double)damageBonus/2);
        }

        Attack attack = new Attack(attackName);
        attack.attackSequence=new int[]{attackBonus};
        attack.damageDiceNumber=damageDiceNumber;
        attack.damageDiceSize = damageDiceSize;
        attack.damageBonus = damageBonus;
        attack.multipleNatural = multipleNatural;
        attack.fullName = attackName + " +"+Integer.toString(attackBonus)+" ("+Integer.toString(damageDiceNumber)+"d"+Integer.toString(damageDiceSize)+"+"+Integer.toString(damageBonus)+")";

        activeDetails.addEvolvedAttack(attackName, attack);

    }

    private static int getAttackBonus(Monster monster, String attackType){
        int bonus = monster.bab;

        if (monster.isWeaponFinesse() && monster.abiDex>monster.abiStr){
            bonus += Utils.getAbilityBonus(monster.abiDex);
        } else {
            bonus += Utils.getAbilityBonus(monster.abiStr);
        }


        int[] penalties = new int[]{8, 4, 2, 1, 0, -1, -2, -4, -8};
        int arrayPos = 2+monster.size;
        if (arrayPos<penalties.length){
            bonus+=penalties[arrayPos];
            //Log.w(Constants.TAG, "ADDING SIZE PENALTY: "+penalties[arrayPos]);
        }


        bonus+=getWeaponFocus(monster, attackType);

        return bonus;
    }

    private static int getWeaponFocus(Monster monster, String attackType){
        if (monster.feats==null)return 0;
        String search = "Weapon Focus ("+attackType+")";
        if (monster.feats.contains(search)){
            return 1;
        }
        return 0;
    }
}
