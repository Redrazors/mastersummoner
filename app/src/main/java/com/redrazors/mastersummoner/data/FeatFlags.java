package com.redrazors.mastersummoner.data;

import com.redrazors.mastersummoner.Constants;

import java.util.HashMap;

public class FeatFlags {


    public boolean augmented;
    public boolean superior;
    public boolean versatile;
    public boolean giantSummoner;
    public boolean evolved;

    public boolean noSound;
    public boolean noDice;

    public boolean modeSummonMonster=true;

    public HashMap<String, Boolean> hashMapFlags = new HashMap<>();

    public FeatFlags(){
        hashMapFlags.put(Constants.FEAT_EXPANDED, false);
        hashMapFlags.put(Constants.FEAT_ALTERNATIVE, false);
        hashMapFlags.put(Constants.FEAT_GOOD, false);
        hashMapFlags.put(Constants.FEAT_NEUTRAL, false);
        hashMapFlags.put(Constants.FEAT_EVIL, false);
        hashMapFlags.put(Constants.FEAT_SKELETON, false);
        hashMapFlags.put(Constants.FEAT_SPIDER, false);
        hashMapFlags.put(Constants.FEAT_PLANT, false);
        hashMapFlags.put(Constants.ARCHETYPE_SHADOW_CALLER, false);
        hashMapFlags.put(Constants.ARMY_OF_DARkNESS, false);


        hashMapFlags.put(Constants.RING_AEON, false);
        hashMapFlags.put(Constants.RING_AGATHION, false);
        hashMapFlags.put(Constants.RING_ANGEL, false);
        hashMapFlags.put(Constants.RING_ARCHON, false);
        hashMapFlags.put(Constants.RING_ASURA, false);
        hashMapFlags.put(Constants.RING_DAEMON, false);
        hashMapFlags.put(Constants.RING_DIV, false);
        hashMapFlags.put(Constants.RING_INEVITABLE, false);
        hashMapFlags.put(Constants.RING_KYTON, false);
        hashMapFlags.put(Constants.RING_PROTEAN, false);
        hashMapFlags.put(Constants.RING_PSYCOPOMP, false);
        hashMapFlags.put(Constants.RING_QLIPPOTH, false);

        hashMapFlags.put(Constants.RING_DRAKE, false);
        hashMapFlags.put(Constants.RING_KAMI, false);
        hashMapFlags.put(Constants.RING_LESHY, false);
    }

}
