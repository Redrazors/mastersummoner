package com.redrazors.mastersummoner.data;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name="Row")
public class Monster {


    public List<Attack> listMeleeAttacks =new ArrayList<>();

    public List<Attack> listRangedAttacks = new ArrayList<>();

    public boolean isVitalStrike(){
        if (feats==null)return false;
        if (feats.toLowerCase().contains("vital strike")){
            return true;
        }
        return false;
    }

    public boolean isImprovedVitalStrike(){
        if (feats==null)return false;
        if (feats.toLowerCase().contains("improved vital strike")){
            return true;
        }
        return false;
    }
    public boolean isWeaponFinesse(){
        if (feats==null)return false;
        if (feats.toLowerCase().contains("weapon finesse")){
            return true;
        }
        if (abiStr==-1){
            return true;
        }
        return false;
    }
    public boolean isPowerAttack(){
        if (feats==null)return false;
        if (feats.toLowerCase().contains("power attack")){
            return true;
        }
        return false;
    }

    public boolean isRanged(){
        if (listRangedAttacks.size()>0){
            return true;
        }
        return false;
    }

    public boolean isSpellCaster(){
        if (spellLikeAbilities.length()>0 || spellsKnown.length()>0 || spellsPrepared.length()>0) return true;
        return false;
    }

    public boolean isHealer(){
        if (spellLikeAbilities.toLowerCase().contains("cure ")
                || spellsPrepared.toLowerCase().contains("cure ")
                || spellsKnown.toLowerCase().contains("cure ")){
            return true;
        }
        return false;
    }

    public int getHD(){
        try{
            if (hd.contains(" HD")){
                return Integer.parseInt(hd.split(" HD")[0]);
            }
            return Integer.parseInt(hd.split("d")[0]);
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
        return 1;

    }

    @Element(required = false)
    public String name;

    @Element(required = false)
    public String cr; // REQ
    @Element(required = false)
    public String mr;  // mythic rank
    @Element(required = false)
    public int xp; // REQ

    @Element(required = false)
    public String raceClass;




    @Element(required = false)
    public String alignment;// REQ
    @Element(required = false)
    public int size;// REQ
    @Element(required = false)
    public String type;// REQ

    @Element(required = false)
    public String subType;

    @Element(required = false)
    public int init; // REQ
    @Element(required = false)
    public String senses;// REQ
    @Element(required = false)
    public String aura;



// DEFENSES

    @Element(required = false)
    public int ac;// REQ
    @Element(required = false)
    public String acMods="";// REQ
    @Element(required = false)
    public int acFlat;
    @Element(required = false)
    public int acTouch;

    @Element(required = false)
    public int hp;// REQ
    @Element(required = false)
    public String hp_mods;

    @Element(required = false)
    public String hd;// REQ

    @Element(required = false)
    public int fort;// REQ
    @Element(required = false)
    public int ref;// REQ
    @Element(required = false)
    public int will;// REQ
    @Element(required = false)
    public String saveMods;

    @Element(required = false)
    public String defensiveAbilities;
    @Element(required = false)
    public String resist="";
    @Element(required = false)
    public String dr="";
    @Element(required = false)
    public String immune="";
    @Element(required = false)
    public int sr;

    @Element(required = false)
    public String weaknesses;
    @Element(required = false)
    public String vulnerability;





    @Element(required = false)
    public String speed;
    @Element(required = false)
    public String speedMod;

    @Element(required = false)
    public String melee="";
    @Element(required = false)
    public String ranged="";
    @Element(required = false)
    public String space="5 ft.";
    @Element(required = false)
    public String reach="5 ft.";

    @Element(required = false)
    public String abilityScoreMods;
    @Element(required = false)
    public int abiStr;
    @Element(required = false)
    public int abiDex;
    @Element(required = false)
    public int abiCon;
    @Element(required = false)
    public int abiInt;
    @Element(required = false)
    public int abiWis;
    @Element(required = false)
    public int abiCha;

    @Element(required = false)
    public int bab;
    @Element(required = false)
    public String cmb;
    @Element(required = false)
    public String cmd;
    @Element(required = false)
    public String sq;


    @Element(required = false)
    public String languages;
    @Element(required = false)
    public String environment;
    @Element(required = false)
    public String organization;
    @Element(required = false)
    public String treasure;

    @Element(required = false)
    public String source="";


    @Element(required = false)
    public String desc="";
    @Element(required = false)
    public String descShort="";
    @Element(required = false)
    public String specialAbi="";
    @Element(required = false)
    public String before;
    @Element(required = false)
    public String during;
    @Element(required = false)
    public String morale;
    @Element(required = false)
    public String gear;
    @Element(required = false)
    public String otherGear;
    @Element(required = false)
    public String feats="";
    @Element(required = false)
    public String powerfulCharge="";
    @Element(required = false)
    public String specialAttacks="";
    @Element(required = false)
    public String spellLikeAbilities="";
    @Element(required = false)
    public String spellsKnown="";
    @Element(required = false)
    public String spellsPrepared="";
    @Element(required = false)
    public String extractsPrepared="";
    @Element(required = false)
    public String psychicMagic="";
    @Element(required = false)
    public String kineticistWildTalents;
    @Element(required = false)
    public String occultistImplements;

    @Element(required = false)
    public String skills;
    @Element(required = false)
    public String skillMods;

    @Element(required = false)
    public String psychicDiscipline;


// npc details

    @Element(required = false)
    public String altStatistics;

    @Element(required = false)
    public String mystery;
    @Element(required = false)
    public String classArchetypes;
    @Element(required = false)
    public String prohibitedSchools;
    @Element(required = false)
    public String focusedSchool;
    @Element(required = false)
    public String traits;
    @Element(required = false)
    public String thassilonianSpecialization;
    @Element(required = false)
    public String spirit;


    @Element(required = false)
    public String patron;
    @Element(required = false)
    public String spellDomains;

    @Element(required = false)
    public String bloodline;


    @Element(required = false)
    public String mt;
    @Element(required = false)
    public String mythic;



    @Element (required = false)
    public Integer rakeHit;
    @Element (required = false)
    public String rakeDamage;

    @Element (required = false)
    public String trample;
    @Element (required = false)
    public Integer trampleDC;

    @Element (required = false)
    public String constrict;

    @Element (required = false)
    public Integer entrap;






}
