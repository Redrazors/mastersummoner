package com.redrazors.mastersummoner.data;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 27/01/2017.
 */
public class MonsterInfo {
	
	
	public List<Attack> listMeleeAttacks =new ArrayList<>();

	public List<Attack> listRangedAttacks = new ArrayList<>();

	
    public String name="";
    public String src="Bestiary 1";
    public double cr=1.0;
    public int ac=10;
    public int acTouch=10;
    public int acFlat=10;

    public int bab=0;

    public int size=2;

    public boolean powerAttack;
    public boolean weaponFinesse;
    public boolean vitalStrike;
    public boolean improvedVitalStrike;


    public int speed=0;
    public int speedSwim=0;
    public int speedClimb=0;
    public int speedFly=0;
    public String speedManeuverability="";
    public int speedBurrow=0;

    public int init;
    public int perception;

    public int str=10;
    public int dex=10;
    public int cha=10;
    public int HP;
    public int hitDice;
    

    public int fort;
    public int reflex;
    public int will;
    
    

    public String melee="";
    public String ranged="";

  

    public int cmb;

    public String fullText="";
   

    public boolean walks;
    public boolean fly;
    public boolean climb;
    public boolean burrow;
    public boolean swim;

    public MonsterInfo(String name){
    	this.name = name;
    }




}
