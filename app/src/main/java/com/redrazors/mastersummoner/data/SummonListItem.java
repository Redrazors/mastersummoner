package com.redrazors.mastersummoner.data;

public class SummonListItem {

    public String name;
    public boolean template;
    public String source="B1";

    public SummonListItem(String name){
        this.name = name;
    }
}
