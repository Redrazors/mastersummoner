package com.redrazors.mastersummoner.data;

public class TemplateInfo {


    public String senses;
    public String dr5;
    public String dr10;
    public String[] resistType;
    public String[] resistValue=new String[3];
    public int sr=-1;
    public String specialAttack="";
    public String specialAbilities ="";


    public void setResistType(String joined){
        resistType = joined.split("&");
    }

    public String getDamageResist(int hd){
        if (hd<5){
            return "";
        }
        if (hd<11){
            return dr5;
        }
        return dr10;
    }

    public String getResists(int hd){
        if (resistType==null){
            return "";
        }

        String resists="";
        int counter=0;
        for (String type: resistType){
            resists+=type+" ";
            if (hd<5){
                resists+=resistValue[0];
            } else if (hd<11){
                resists+=resistValue[1];
            } else {
                resists+=resistValue[2];
            }
            counter++;
            if (counter<resistType.length){
                resists+=", ";
            }
        }

        return resists;
    }

    public int getSR(double cr, int hd){
        if(sr==-1){
            return 0;
        }

        if (hd>=5){
            cr++;
        }


        int spellResist = ((int)Math.floor(cr))+sr;

        return spellResist;

    }
}
