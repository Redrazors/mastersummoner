package com.redrazors.mastersummoner.dice;

import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.active.PowerfulCharge;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.ElementalDice;
import com.redrazors.mastersummoner.staticmethods.AttackDamageStatics;

import java.util.ArrayList;
import java.util.List;

public class AttackResults {


    // when dice is formed, add it in here with label
    public List<Roll> listDiceAndRolls = new ArrayList<>();

    public List<Roll> listElementalDice = new ArrayList<>();
    public List<Roll> listDamageDice = new ArrayList<>();
    public List<Roll> listCritDice = new ArrayList<>();

    public List<ElementalDice> listSingleDamageElementalDice = new ArrayList<>();

    public Roll attackRoll;
    public Roll critRoll;

    public int hitBonus;
    public Attack attack;

    public int damageTotal;


    public AttackResults(Attack attack, int hitBonus){
        this.attack = attack;
        this.hitBonus = hitBonus;
    }

    public String createAttackRoll(ActiveDetails activeDetails){
        String attackString="1d20";

        // attack roll
        attackRoll = new Roll(20);
        listDiceAndRolls.add(attackRoll);


        if (attack.noDamage || attack.singleDamage){
            return attackString;
        }

        // damage dice
        PowerfulCharge powerfulCharge = AttackDamageStatics.getPowerfulCharge(activeDetails, attack);
        int damageDiceNumber = AttackDamageStatics.getDamageDiceNumber(activeDetails, attack, false, powerfulCharge);
        int damageDiceSize = AttackDamageStatics.getDamageDiceSize(activeDetails, attack, powerfulCharge);

        for (int i=0; i<damageDiceNumber; i++){

            Roll roll = new Roll(damageDiceSize);

            listDiceAndRolls.add(roll);
            listDamageDice.add(roll);

        }

        if (damageDiceSize==2){
            attackString+="+1d4";
        } else if (damageDiceSize==3){
            attackString+="+1d6";
        } else {
            attackString+="+"+Integer.toString(damageDiceNumber)+"d"+Integer.toString(damageDiceSize);
        }



        // elemental dice
        if (attack.elementalDice!=null){
            for (int i=0; i<attack.elementalDice.diceNumber; i++){
                Roll roll = new Roll(attack.elementalDice.diceSize);
                roll.element = attack.elementalDice.type;

                listDiceAndRolls.add(roll);
                listElementalDice.add(roll);
            }
            attackString+="+"+Integer.toString(attack.elementalDice.diceNumber)+"d"+Integer.toString(attack.elementalDice.diceSize);
        }

        // add template elemental dice
        ElementalDice elementalDice = AttackDamageStatics.getTemplatElementalDice(activeDetails.monster, activeDetails.template);
        if (elementalDice!=null){
            if (elementalDice.singleDamage){
                listSingleDamageElementalDice.add(elementalDice);
            } else {
                for (int i=0; i<elementalDice.diceNumber; i++){
                    Roll roll = new Roll(elementalDice.diceSize);
                    roll.element = elementalDice.type;
                    listDiceAndRolls.add(roll);
                    listElementalDice.add(roll);
                }
            }


            attackString+="+"+Integer.toString(elementalDice.diceNumber)+"d"+Integer.toString(elementalDice.diceSize);
        }


        return attackString;

    }

    public String createCritRoll(ActiveDetails activeDetails){
        String critString="1d20";

        critRoll = new Roll(20);
        listDiceAndRolls.add(critRoll);


        PowerfulCharge powerfulCharge = AttackDamageStatics.getPowerfulCharge(activeDetails, attack);
        int damageDiceNumber = AttackDamageStatics.getDamageDiceNumber(activeDetails, attack, true, powerfulCharge);
        int damageDiceSize = AttackDamageStatics.getDamageDiceSize(activeDetails, attack, powerfulCharge);

        int critDiceNumber = damageDiceNumber*(attack.critMultiplier-1);
        // damage dice
        for (int i=0; i<critDiceNumber; i++){
            Roll roll = new Roll(damageDiceSize);
            listDiceAndRolls.add(roll);
            listCritDice.add(roll);
        }

        if (damageDiceSize==2){
            critString+="+1d4";
        } else if (damageDiceSize==3){
            critString+="+1d6";
        } else {
            critString += "+"+Integer.toString(critDiceNumber)+"d"+Integer.toString(damageDiceSize);
        }



        return critString;

    }


    public void setResults(int[] allRolls){
        for (int i=0; i<allRolls.length; i++){

            try{
                listDiceAndRolls.get(i).rollResult = allRolls[i];
            } catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            } catch (NullPointerException e){
                e.printStackTrace();
            } catch (IndexOutOfBoundsException e){
                e.printStackTrace();
            }
        }
    }








}
