package com.redrazors.mastersummoner.dice;

import android.util.Log;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.Utils;

public class Dice {

    // this class has not been implemented throughout, was added for special attacks

    private String diceString;
    public Dice(String diceString){
        this.diceString = diceString;
    }


    public String getDiceString(int extraBonus){
        if (extraBonus==0) return diceString;


        int damageDiceNumber=1;
        int damageDiceSize=8;
        int damageBonus=0;
        try{
            String[] split = diceString.split("d");
            damageDiceNumber = Integer.parseInt(split[0]);


            String[] diceSizeAndBonus = split[1].split("\\+|\\-");
            damageDiceSize = Integer.parseInt(diceSizeAndBonus[0]);
            if (diceSizeAndBonus.length>1){
                if (diceString.contains("+")){
                    damageBonus=Integer.parseInt(diceSizeAndBonus[1]);
                } else if (diceString.contains("-")){
                    damageBonus-=Integer.parseInt(diceSizeAndBonus[1]);
                }

            }
        } catch (NumberFormatException e){
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
            Log.w(Constants.TAG, "ERROR WITH "+diceString);
        }


        String adjustedDamage = Integer.toString(damageDiceNumber)+"d"+Integer.toString(damageDiceSize);
        damageBonus+=extraBonus;
        adjustedDamage+= Utils.getSymbolAndAmount(damageBonus);

        return adjustedDamage;
    }
}
