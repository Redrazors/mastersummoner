package com.redrazors.mastersummoner.dice;

import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.active.PowerfulCharge;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.ElementalDice;
import com.redrazors.mastersummoner.staticmethods.AttackDamageStatics;

import java.util.Random;

public class DiceRoller {

    private static final String DICE_COLOR_BURGANCY = "#5F0000";
    private static final String DICE_COLOR_YELLOW = "#b38f00";

    private static final String LABEL_COLOR_WHITE= "#FFFFFF";
    private static final String LABEL_COLOR_BLACK = "#000000";




    private MainActivity mainActivity;

    private DiceUI diceUI;
    private AttackResults attackResults;

    public DiceRoller(MainActivity mainActivity){
        this.mainActivity = mainActivity;

        diceUI = new DiceUI(mainActivity);

        if (mainActivity.dataControl.featFlags.noDice){
            showHideRollTray(false);
        }

    }

    public void showHideRollTray(boolean show){
        diceUI.showHideDiceTray(show);
    }


    public void rollD20(String label, final int bonus){
        diceUI.initialiseD20Roll(label);

        if (mainActivity.dataControl.featFlags.noDice){

            int[] allRolls = new int[1];
            Random random = new Random();
            allRolls[0] = random.nextInt(20) + 1;
            parseD20Results(allRolls, bonus);

        } else {
            RollCallback rollCallback = new RollCallback() {
                @Override
                public void rollResults(final int[] allRolls) {
                    // switch back to mainthread
                    mainActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            parseD20Results(allRolls, bonus);
                        }
                    });

                }
            };
            diceUI.createRoll(rollCallback, "1d20", DICE_COLOR_BURGANCY, LABEL_COLOR_WHITE );
        }

    }


    public void rollAttackDice(final ActiveDetails activeDetails, final Attack attack, int hitBonus, int sequenceRef){

        hitBonus  = AttackDamageStatics.getAttackHitBonus(activeDetails, attack, hitBonus, true );
        diceUI.initialiseAttack(activeDetails.getAttackFullNameAdjustedDice(attack, sequenceRef));
        //diceUI.setCustomHitDamageBonus(activeDetails.hashMapAttackExtraInformation.get(attack).attackBonus,activeDetails.hashMapAttackExtraInformation.get(attack).damageBonus );
        attackResults = new AttackResults(attack, hitBonus);

        if (mainActivity.dataControl.featFlags.noDice){

            Random random = new Random(); /* <-- this is a constructor */
            attackResults.createAttackRoll(activeDetails);
            int[] allRolls = new int[attackResults.listDiceAndRolls.size()];
            for (int i=0; i<attackResults.listDiceAndRolls.size(); i++){
                Roll roll = attackResults.listDiceAndRolls.get(i);
                allRolls[i] = random.nextInt(roll.diceSize) + 1;
            }
            parseAttackResults(allRolls, activeDetails, attack);


        } else {
            RollCallback rollCallback = new RollCallback() {
                @Override
                public void rollResults(final int[] allRolls) {
                    // switch back to mainthread
                    mainActivity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            parseAttackResults(allRolls, activeDetails, attack);
                        }
                    });

                }
            };

            diceUI.createRoll(rollCallback, attackResults.createAttackRoll(activeDetails), DICE_COLOR_BURGANCY, LABEL_COLOR_WHITE );
        }



    }

    private void parseAttackResults(int[] allRolls, ActiveDetails activeDetails, Attack attack){

        // allrolls will correspond with list in attackResults
        attackResults.setResults(allRolls);




        int hitTotal = attackResults.attackRoll.rollResult+attackResults.hitBonus;
        String hitResult = "Attack Roll 1d20 + "+Integer.toString(attackResults.hitBonus)+" = ("
                +attackResults.attackRoll.rollResult+") + "+attackResults.hitBonus;

        PowerfulCharge powerfulCharge = AttackDamageStatics.getPowerfulCharge(activeDetails, attack);
        int baseDamage = AttackDamageStatics.getDamageBonus(activeDetails, attack, true, powerfulCharge);
        attackResults.damageTotal = baseDamage;
        String damageResult = "";

        if (attackResults.attack.noDamage){
            attackResults.damageTotal=0;
            damageResult="No damage";
        } else if (attackResults.attack.singleDamage){
            attackResults.damageTotal=1;
            damageResult="1";
        } else {
            // build basic dice roll and damage bonus eg 3d6+7
            int damageDiceNumber = AttackDamageStatics.getDamageDiceNumber(activeDetails, attack, false, powerfulCharge);
            int damageDiceSize = AttackDamageStatics.getDamageDiceSize(activeDetails, attack, powerfulCharge);

            damageResult = "Damage "+Integer.toString(damageDiceNumber)+"d"+Integer.toString(damageDiceSize);
            if (attackResults.damageTotal>0){
                damageResult+= Utils.getSymbol(attackResults.damageTotal)+Integer.toString(attackResults.damageTotal);
            }
            if (attackResults.attack.elementalDice!=null){
                damageResult+= " + "+Integer.toString(attack.elementalDice.diceNumber)+"d"+Integer.toString(attack.elementalDice.diceSize)+"["+
                        attack.elementalDice.type+"]";
            }

            damageResult+=" = ";

            int counter=0;
            for (Roll roll: attackResults.listDamageDice){
                if (roll.halfResult){
                    int halfResult = (int)Math.ceil((double)roll.rollResult/2);
                    attackResults.damageTotal+=halfResult;
                    damageResult+=bracketNum(halfResult);
                } else {
                    attackResults.damageTotal+=roll.rollResult;
                    damageResult+=bracketNum(roll.rollResult);
                }

                counter++;
                if (counter<attackResults.listDamageDice.size()){
                    damageResult+=" + ";
                }
            }

            if (baseDamage!=0){
                damageResult+=" "+Utils.getSymbol(baseDamage)+" "+Integer.toString(baseDamage);
            }
            // reset counter and do elemental dice
            counter=0;
            for (Roll roll: attackResults.listElementalDice){
                attackResults.damageTotal+=roll.rollResult;
                //Log.w(Constants.TAG, "ELEMENTAL ROLL: "+roll.)
                damageResult+=" + ("+Integer.toString(roll.rollResult)+" "+roll.element+")";

                counter++;
//                if (counter<attackResults.listElementalDice.size()){
//                    damageResult+=" + ";
//                }
            }
            for (ElementalDice elementalDice: attackResults.listSingleDamageElementalDice){
                attackResults.damageTotal++;
                damageResult+=" + (1 "+elementalDice.type+")";
            }
        }


        //damageResult = "Damage "+attackResults.damageTotal +" ("+damageResult+")";

        int totalDam = attackResults.damageTotal;
        if (totalDam<1 && !attack.noDamage){
            totalDam=1;
            damageResult = "NONLETHAL 1 ("+attackResults.damageTotal +" ("+damageResult+"))";
        }

        diceUI.updateHitAndDamage(hitTotal, hitResult, totalDam, damageResult);
        // assign values
        // check for crits
        checkForCrit(activeDetails, attack);
        //update ui

    }

    private String bracketNum(int num){
        return "("+Integer.toString(num)+")";
    }

    private void checkForCrit(final ActiveDetails activeDetails, final Attack attack){
        //if (attackResults.attackRoll.rollResult>=0){

        if (attackResults.attackRoll.rollResult>=attackResults.attack.critRange && !attackResults.attack.noDamage){

            diceUI.revealCritBoxes();
            if (mainActivity.dataControl.featFlags.noDice){
                Random random = new Random(); /* <-- this is a constructor */
                attackResults.createCritRoll(activeDetails);

                int[] allRolls = new int[attackResults.listDiceAndRolls.size()];
                for (int i=0; i<attackResults.listDiceAndRolls.size(); i++){
                    Roll roll = attackResults.listDiceAndRolls.get(i);
                    if (roll.rollResult==-1){
                        allRolls[i] = random.nextInt(roll.diceSize) + 1;
                    }
                }
                parseCritResults(allRolls, activeDetails, attack);

            } else {
                RollCallback rollCallback = new RollCallback() {
                    @Override
                    public void rollResults(final int[] allRolls) {

                        mainActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                parseCritResults(allRolls, activeDetails, attack);
                            }
                        });
                    }
                };

                diceUI.createRoll(rollCallback, attackResults.createCritRoll(activeDetails), DICE_COLOR_YELLOW, LABEL_COLOR_BLACK );

            }


        }
    }




    private void parseCritResults(int[] allRolls, ActiveDetails activeDetails, Attack attack){

        attackResults.setResults(allRolls);

        PowerfulCharge powerfulCharge = AttackDamageStatics.getPowerfulCharge(activeDetails, attack);

        int critConfirm = attackResults.critRoll.rollResult+attackResults.hitBonus;
        String critConfirmResult = "Crit Confirm Roll 1d20 + "+attackResults.hitBonus+" = "
                +"("+attackResults.critRoll.rollResult+") + "+attackResults.hitBonus;

        // add previis damage
        int critTotal = attackResults.damageTotal;
        // add basic damage mod multipled by crit multiplier -1
        int critDamageBonus = AttackDamageStatics.getDamageBonus(activeDetails, attack, true, powerfulCharge)*(attackResults.attack.critMultiplier-1);
        int critBaseDamage = critDamageBonus;
        critTotal+= critDamageBonus;

        String critTotalResult ="Critital Total ";

        if (attackResults.attack.singleDamage){
            critTotal+=1;
            critTotalResult+="1";
        } else {
            int damageDiceNumber = AttackDamageStatics.getDamageDiceNumber(activeDetails, attack, true, powerfulCharge);

            int numCritDice = damageDiceNumber*(attackResults.attack.critMultiplier-1);
            int critDiceSize = AttackDamageStatics.getDamageDiceSize(activeDetails, attack, powerfulCharge);
            critTotalResult += Integer.toString(numCritDice)+"d"+Integer.toString(critDiceSize);
            if (critBaseDamage>0){
                critTotalResult+=" + "+Integer.toString(critBaseDamage);
            }
            critTotalResult+=" + "+Integer.toString(attackResults.damageTotal)+" = ";
            int counter=0;
            for (Roll roll: attackResults.listCritDice){
                if (roll.halfResult){
                    int halfResult = (int)Math.ceil((double)roll.rollResult/2);
                    critTotal+=halfResult;
                    critTotalResult+=bracketNum(halfResult);
                } else {
                    critTotal+=roll.rollResult;
                    critTotalResult+=bracketNum(roll.rollResult);
                }

                counter++;
                if (counter<attackResults.listCritDice.size()){
                    critTotalResult+=" + ";
                }
            }

            if (critBaseDamage>0){
                critTotalResult+=" + "+Integer.toString(critBaseDamage);
            }

            critTotalResult += " + "+Integer.toString(attackResults.damageTotal);
        }

        if (critTotal<1){
            critTotal=1;
            critTotalResult = "NONLETHAL CRIT "+critTotal +" ("+critTotalResult+")";
        } else {
            //critTotalResult = "Crit Total "+critTotal +" ("+critTotalResult+")";
        }


        diceUI.updateCritAndCritTotal(critConfirm, critConfirmResult, critTotal, critTotalResult);

    }


    private void parseD20Results(int[] allRolls, int bonus){

        int roll =0;
        if (allRolls.length<1){
            // create a random roll if empty
            Random random = new Random(); /* <-- this is a constructor */
            roll = random.nextInt(20) + 1;
        } else {
            roll=allRolls[0];
        }

        int rollTotal = roll+bonus;
        String rollResult = "Roll 1d20 + "+Integer.toString(bonus)+ " = "+bracketNum(roll)+" + "+Integer.toString(bonus);
        diceUI.updateD20Roll(rollTotal, rollResult);


    }


}
