package com.redrazors.mastersummoner.dice;

import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class DiceUI {

    private WebView webView;
    private Button buttonClose;
    private LinearLayout layoutRoller;
    private TextView textViewTitle;
    private TextView textViewHitTotal, textViewCritConfirm, textViewDamageTotal, textViewCritTotal, textViewCritConfirmLabel, textViewCritTotalLabel;
    private TextView textViewHitTotalLabel, textViewDamageTotalLabel;
    private TextView textViewUserHitBonus, textViewUserDamageBonus;
    private RollCallback rollCallback;
    private ListView listViewResults;
    private List<String> listResults = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    private LinearLayout layoutWebViewHolder;

    private MainActivity mainActivity;
    public DiceUI(MainActivity mainActivity){
        this.mainActivity = mainActivity;

        layoutRoller = mainActivity.findViewById(R.id.layout_roller);
        textViewTitle = mainActivity.findViewById(R.id.textview_title);


        textViewHitTotal = mainActivity.findViewById(R.id.textview_hit_total);
        textViewCritConfirm = mainActivity.findViewById(R.id.textview_crit_confirm);
        textViewDamageTotal = mainActivity.findViewById(R.id.textview_damage_total);
        textViewCritTotal = mainActivity.findViewById(R.id.textview_crit_total);

        textViewCritTotalLabel = mainActivity.findViewById(R.id.textview_crit_total_label);
        textViewCritConfirmLabel = mainActivity.findViewById(R.id.textview_crit_confirm_label);
        textViewHitTotalLabel = mainActivity.findViewById(R.id.textview_hit_total_label);
        textViewDamageTotalLabel = mainActivity.findViewById(R.id.textview_damage_total_label);

        textViewUserHitBonus = mainActivity.findViewById(R.id.textview_user_hit_bonus);
        textViewUserDamageBonus = mainActivity.findViewById(R.id.textview_user_damage_bonus);

        listViewResults = mainActivity.findViewById(R.id.listview_results);
        adapter=new ArrayAdapter<>(mainActivity, R.layout.listview_item_small, listResults);
        listViewResults.setAdapter(adapter);


        layoutWebViewHolder = mainActivity.findViewById(R.id.layout_webview_holder);

        webView = mainActivity.findViewById(R.id.webview_roller);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setUserAgentString("Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X) AppleWebKit/600.14 (KHTML, like Gecko) Mobile/12F70");
        //webView.setBackgroundColor(0xFF0000);
        //webView.setBackground(mainActivity.getResources().getDrawable(R.drawable.felt));
        // set transparency here
        webView.setBackgroundColor(0x00FFFFFF);
        //webView.setBackgroundResource(R.drawable.rounded_rectangle_white_solid);

        webView.addJavascriptInterface(new JavaScriptInterface(), "Interface");
        webView.loadUrl("file:///android_asset/index.html");
        webView.loadUrl("javascript:createRoll('1d6')"); // this is required to set it up without load glitches later

        buttonClose = mainActivity.findViewById(R.id.button_close_rolling);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.loadUrl("javascript:clear()");
                layoutRoller.setVisibility(View.INVISIBLE);
            }
        });
        layoutRoller.setVisibility(View.INVISIBLE);


    }

    public void showHideDiceTray(boolean show){
        if (show){
            layoutWebViewHolder.setVisibility(View.VISIBLE);
        } else {
            layoutWebViewHolder.setVisibility(View.GONE);
        }
    }

    void initialiseAttack(String title){
        textViewCritTotal.setText(null);
        textViewHitTotal.setText(null);
        textViewCritConfirm.setText(null);
        textViewDamageTotal.setText(null);

        textViewCritConfirm.setVisibility(View.GONE);
        textViewCritTotal.setVisibility(View.GONE);
        textViewCritConfirmLabel.setVisibility(View.GONE);
        textViewCritTotalLabel.setVisibility(View.GONE);

        textViewUserHitBonus.setVisibility(View.GONE);
        textViewUserDamageBonus.setVisibility(View.GONE);

        layoutRoller.setVisibility(View.VISIBLE);
        listResults.clear();
        adapter.notifyDataSetChanged();

        textViewTitle.setText(title);

        textViewDamageTotal.setVisibility(View.VISIBLE);
        textViewDamageTotalLabel.setVisibility(View.VISIBLE);

        textViewHitTotalLabel.setText("TO HIT");

    }

    void setCustomHitDamageBonus(int hitBonus, int damageBonus){
        if (hitBonus!=0){
            textViewUserHitBonus.setVisibility(View.VISIBLE);
            textViewUserHitBonus.setText("Extra Hit: "+ Utils.getSymbol(hitBonus)+Integer.toString(hitBonus));
        }
        if (damageBonus!=0){
            textViewUserDamageBonus.setVisibility(View.VISIBLE);
            textViewUserDamageBonus.setText("Extra Dam: "+ Utils.getSymbol(damageBonus)+Integer.toString(damageBonus));
        }
    }

    void initialiseD20Roll(String title){
        textViewCritTotal.setText(null);
        textViewHitTotal.setText(null);
        textViewCritConfirm.setText(null);
        textViewDamageTotal.setText(null);

        textViewCritConfirm.setVisibility(View.GONE);
        textViewCritTotal.setVisibility(View.GONE);
        textViewCritConfirmLabel.setVisibility(View.GONE);
        textViewCritTotalLabel.setVisibility(View.GONE);

        textViewUserHitBonus.setVisibility(View.GONE);
        textViewUserDamageBonus.setVisibility(View.GONE);

        layoutRoller.setVisibility(View.VISIBLE);
        listResults.clear();
        adapter.notifyDataSetChanged();

        textViewTitle.setText(title);

        textViewDamageTotal.setVisibility(View.GONE);
        textViewDamageTotalLabel.setVisibility(View.GONE);

        textViewHitTotalLabel.setText("Roll Total");



    }

    void createRoll(RollCallback rollCallback, String roll, String colorDice, String colorLabel){

        this.rollCallback = rollCallback;
        boolean noSound = mainActivity.dataControl.featFlags.noSound;

        String arguments = "javascript:createRoll('"+roll+"', '"+colorDice+"', '"+colorLabel+"',"+noSound+")";
        Log.w(Constants.TAG, arguments);
        webView.loadUrl(arguments);
    }

    public void updateHitAndDamage(final int hitTotal, final String hitResult, final int damageTotal, final String damageResult){

        textViewHitTotal.setText(Integer.toString(hitTotal));
        listResults.add(hitResult);

        textViewDamageTotal.setText(Integer.toString(damageTotal));
        listResults.add(damageResult);

        adapter.notifyDataSetChanged();
    }

    public void revealCritBoxes(){
        textViewCritConfirm.setVisibility(View.VISIBLE);
        textViewCritTotal.setVisibility(View.VISIBLE);
        textViewCritConfirmLabel.setVisibility(View.VISIBLE);
        textViewCritTotalLabel.setVisibility(View.VISIBLE);
    }

    public void updateCritAndCritTotal(final int critConfirm, final String critConfirmResult, final int critTotal, final String critTotalResult){

        textViewCritConfirm.setText(Integer.toString(critConfirm));
        listResults.add(critConfirmResult);

        textViewCritTotal.setText(Integer.toString(critTotal));
        listResults.add(critTotalResult);

        adapter.notifyDataSetChanged();
    }


    public void updateD20Roll(final int rollTotal, final String rollResult){

        textViewHitTotal.setText(Integer.toString(rollTotal));
        listResults.add(rollResult);

        adapter.notifyDataSetChanged();
    }



    private class JavaScriptInterface {

        @JavascriptInterface
        public void callFromJS(String notation, String values) {
            Log.w(Constants.TAG, "CALL FROM JS SUCCESS:"+notation + ": "+values);

            if (layoutRoller.getVisibility()==View.VISIBLE){
                try{
                    //JSONArray notationJSON = new JSONObject(notation).getJSONArray("set");
                    JSONArray valuesJSON = new JSONArray(values);

                    int[] allRolls = new int[valuesJSON.length()];
                    for (int i=0; i<valuesJSON.length(); i++) {
                        allRolls[i] = valuesJSON.getInt(i);
                    }

                    if (rollCallback!=null){
                        rollCallback.rollResults(allRolls);
                    }


                } catch (JSONException e){
                    e.printStackTrace();
                }
            }

        }
    }
}
