package com.redrazors.mastersummoner.dice;

public class Roll {

    public int diceSize;
    public boolean halfResult;  // eg d3 is d6 halved
    public int rollResult=-1;

    public String element;

    public Roll(int diceSize){
        if (diceSize==2){
            this.diceSize=4;
            halfResult=true;
        } else if (diceSize==3){
            this.diceSize=6;
            halfResult=true;
        } else {
            this.diceSize = diceSize;
        }
    }
}
