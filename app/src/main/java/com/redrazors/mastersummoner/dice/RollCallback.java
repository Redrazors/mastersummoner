package com.redrazors.mastersummoner.dice;

public interface RollCallback {

    void rollResults(int[] allRolls);
}
