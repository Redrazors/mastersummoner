package com.redrazors.mastersummoner.staticmethods;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.active.AttackExtraInformation;
import com.redrazors.mastersummoner.active.PowerfulCharge;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.ElementalDice;
import com.redrazors.mastersummoner.data.Monster;

public class AttackDamageStatics {


    // used by stat block, gets full name adjusted for templates without custom bonuses or vitral strid
    public static String getAttackFullNameCompleteSequence(ActiveDetails activeDetails, Attack attack){
        String fullName = replaceAttackSequenceWithFullSequence(activeDetails, attack);

        fullName = replaceDamageDice(activeDetails, attack, fullName, true, false);

        fullName=attack.magicBonus+fullName;

        return fullName;
    }

    public static String getAttackFullName(ActiveDetails activeDetails, Attack attack, int sequenceRef, boolean addCustomBonus, boolean hideVitalStrike){

        if (sequenceRef>=attack.attackSequence.length){
            sequenceRef=0;
        }

        String fullName = replaceAttackSequence(activeDetails, attack, sequenceRef, addCustomBonus);

        fullName = replaceDamageDice(activeDetails, attack, fullName, hideVitalStrike, addCustomBonus);

        fullName=attack.magicBonus+fullName;

        return fullName;

    }

    private static String replaceAttackSequence(ActiveDetails activeDetails, Attack attack, int sequenceRef, boolean addCustomBonus){

        String part1  = attack.fullName.split("\\(")[0];
        String oldPart1 = part1;


        String oldAttackBonus = "";

        for (int i=0; i<attack.attackSequence.length;i++){
            oldAttackBonus+= Utils.getSymbolAndAmount(attack.attackSequence[i]);
            if (i+1<attack.attackSequence.length){
                oldAttackBonus+="/";
            }
        }
        //Log.w(Constants.TAG, "OLD ATTACK BONUS: "+oldAttackBonus);
        int adjustAttackBonus=getAttackHitBonus(activeDetails, attack, attack.attackSequence[sequenceRef], addCustomBonus);
        String sequenceAttackBonus = Utils.getSymbol(adjustAttackBonus)+Integer.toString(adjustAttackBonus);
        //Log.w(Constants.TAG, "sequence ATTACK BONUS: "+sequenceAttackBonus);
        part1 = part1.replace(oldAttackBonus, sequenceAttackBonus);

        return attack.fullName.replace(oldPart1, part1);
    }
    private static String replaceAttackSequenceWithFullSequence(ActiveDetails activeDetails, Attack attack){

        String part1  = attack.fullName.split("\\(")[0];
        String oldPart1 = part1;


        String oldAttackBonus = "";

        for (int i=0; i<attack.attackSequence.length;i++){
            oldAttackBonus+= Utils.getSymbolAndAmount(attack.attackSequence[i]);
            if (i+1<attack.attackSequence.length){
                oldAttackBonus+="/";
            }
        }
        //Log.w(Constants.TAG, "OLD ATTACK BONUS: "+oldAttackBonus);
        String sequenceAttackBonus = "";
        int counter=0;
        for (int seq: attack.attackSequence){
            int adjustAttackBonus=getAttackHitBonus(activeDetails, attack, seq, false);
            sequenceAttackBonus += Utils.getSymbolAndAmount(adjustAttackBonus);
            counter++;
            if (counter<attack.attackSequence.length){
                sequenceAttackBonus+="/";
            }
        }

        //Log.w(Constants.TAG, "sequence ATTACK BONUS: "+sequenceAttackBonus);
        part1 = part1.replace(oldAttackBonus, sequenceAttackBonus);

        return attack.fullName.replace(oldPart1, part1);
    }
    private static String replaceDamageDice(ActiveDetails activeDetails, Attack attack, String fullName, boolean hideVitalStrike, boolean addCustomBonus){


        if (!attack.noDamage && !attack.singleDamage && attack.attackType !=0){

            PowerfulCharge powerfulCharge = getPowerfulCharge(activeDetails, attack);

            try{
                String part2  = attack.fullName.split("\\(")[1];
                String originalPart2 = part2;


                String oldDamageDice = Integer.toString(attack.damageDiceNumber)+"d"+Integer.toString(attack.damageDiceSize);

                String newDamageDice = Integer.toString(getDamageDiceNumber(activeDetails, attack, hideVitalStrike, powerfulCharge))
                        +"d"+Integer.toString(getDamageDiceSize(activeDetails, attack, powerfulCharge));

                // construct the original string of damage dice plus modifer, eg 1d4+1
                if (attack.damageBonus<0){
                    oldDamageDice=oldDamageDice+Integer.toString(attack.damageBonus);
                } else if (attack.damageBonus>0){
                    oldDamageDice=oldDamageDice+"+"+Integer.toString(attack.damageBonus);
                }

                int newDamageBonus = getDamageBonus(activeDetails, attack, addCustomBonus, powerfulCharge);

                if (newDamageBonus<0){
                    newDamageDice=newDamageDice+Integer.toString(newDamageBonus);
                } else if (newDamageBonus>0){
                    newDamageDice=newDamageDice+"+"+Integer.toString(newDamageBonus);
                }

                //Log.w(Constants.TAG, "OLD "+oldDamageDice);
                //Log.w(Constants.TAG, "NEW "+newDamageDice);

                part2 = part2.replace(oldDamageDice, newDamageDice);
                fullName = fullName.replace(originalPart2, part2);
            } catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }

        }

        String[] split = fullName.split("\\)");
        return fullName.replace(split[0], split[0]+getTemplateDamage(activeDetails.monster, activeDetails.template));
    }

    private static String getTemplateDamage(Monster monster, String template){
        ElementalDice elementalDice = getTemplatElementalDice(monster, template);
        if (elementalDice==null){
            return "";
        }
        String dice = Integer.toString(elementalDice.diceNumber)+"d"+Integer.toString(elementalDice.diceSize);
        if (elementalDice.diceSize==1){
            dice = "1";
        }
        return " plus "+dice+" "+elementalDice.type;

    }


    public static ElementalDice getTemplatElementalDice(Monster monster, String template){
        int diceSize = 1;
        int diceNumber = 1;
        if (monster.getHD()>4){
            diceSize=6;
            if (template.equals(Constants.TEMPLATE_FIERY)){
                diceNumber=2;
            }
        }
        if (monster.getHD()>10){
            diceNumber=2;
            if (template.equals(Constants.TEMPLATE_FIERY)){
                diceNumber=3;
            }
        }

        boolean singleDamage=false;
        if (diceNumber==1 && diceSize==1){
            singleDamage=true;
        }



        if (template.equals(Constants.TEMPLATE_AQUEOUS)){
            return new ElementalDice(ElementalDice.ELEMENT_COLD, diceNumber, diceSize, singleDamage);
        } else if (template.equals(Constants.TEMPLATE_AERIAL)){
            return new ElementalDice(ElementalDice.ELEMENT_ELECTRICITY, diceNumber, diceSize, singleDamage);
        } else if (template.equals(Constants.TEMPLATE_CHTHONIC)){
            return new ElementalDice(ElementalDice.ELEMENT_ACID, diceNumber, diceSize, singleDamage);
        } else if (template.equals(Constants.TEMPLATE_FIERY)){
            return new ElementalDice(ElementalDice.ELEMENT_FIRE, diceNumber, diceSize, singleDamage);
        }
        return null;
    }



    public static int getAttackHitBonus(ActiveDetails activeDetails, Attack attack, int bonus, boolean addCustomBonus){

        AttackExtraInformation attackExtraInformation = activeDetails.hashMapAttackExtraInformation.get(attack);
        Monster monster = activeDetails.monster;
        int[] abilityBonus = activeDetails.abilityIncrease;
        //Log.w(Constants.TAG, "GETTING HIT BONUS "+attack.fullName);
        //Log.w(Constants.TAG, "INCOMING BONUS "+bonus);

        if (addCustomBonus){
            bonus+=attackExtraInformation.attackBonus;
        }

        if (attackExtraInformation.powerAttack){
            bonus-=getPowerAttackAttackPenalty(monster);
        }

        if (attackExtraInformation.smite){

            int chaBonus = Utils.getAbilityBonus(monster.abiCha+abilityBonus[5]);
            if (chaBonus<0){
                chaBonus=0;
            }
            bonus+=chaBonus;
        }

        if (attackExtraInformation.powerfulCharge){
            bonus+=2;
        }

        if (addStrToHit(attack)){
            // if weapon finesse, need to check if str + mods from augment and giant summon >dex then add bonus based on difference
            if ( (abilityBonus[0]>0) && monster.isWeaponFinesse()){
                // Ability Scores +4 size bonus to Str and Con, –2 Dex.
                int strAdjust = monster.abiStr+abilityBonus[0];
                int dexAdjust = monster.abiDex+abilityBonus[1];

                // if the new strength is greater than dex then work out the difference between
                if (strAdjust>dexAdjust){
                    int strMod = (int) Math.ceil(((double) strAdjust - 11) / 2);
                    int dexMod = (int) Math.ceil(((double) dexAdjust - 11) / 2);

                    bonus+= (strMod-dexMod);
                }

            } else {

                int extraBonus =(int)Math.floor((double)abilityBonus[0]/2);

                //Log.w(Constants.TAG, "OUTGOING BONUS a "+bonus + " extra bonus "+extraBonus);

                bonus+=extraBonus;

            }
        }

        //Log.w(Constants.TAG, "OUTGOING BONUS "+bonus);

        return bonus;
    }

    private static boolean addStrToHit(Attack attack){
        if (attack.attackType==Attack.ATK_SPECIAL ||
                attack.attackType==Attack.ATK_OFF_HAND_OR_SECONDARY ||attack.attackType==Attack.ATK_MELEE ||
                attack.attackType==Attack.ATK_TWOHANDED_OR_SINGLE_NATURAL ){
            return true;
        }
        return false;
    }

    private static boolean addStrToDamage(Attack attack){
        if (attack.attackType==Attack.ATK_OFF_HAND_OR_SECONDARY
                || attack.attackType==Attack.ATK_MELEE
                || attack.attackType==Attack.ATK_TWOHANDED_OR_SINGLE_NATURAL
                || attack.attackType== Attack.ATK_RANGED_COMPOSITE_OR_THROWN
                || attack.attackType==Attack.ATK_RANGED_GIANT){
            return true;
        }
        return false;
    }


    public static int getDamageBonus(ActiveDetails activeDetails, Attack attack, boolean addCustomBonus, PowerfulCharge powerfulCharge){
        int bonus = attack.damageBonus;


        if (powerfulCharge!=null){
            bonus = powerfulCharge.damageBonus;
        }

        AttackExtraInformation attackExtraInformation = activeDetails.hashMapAttackExtraInformation.get(attack);
        Monster monster = activeDetails.monster;
        int[] abilityBonus = activeDetails.abilityIncrease;

        if (addCustomBonus){
            bonus+=attackExtraInformation.damageBonus;
        }

        if (attackExtraInformation.powerAttack){
            bonus+=getPowerAttackDamageBonus(monster, attack, attackExtraInformation);
        }
        if (attackExtraInformation.smite){
            bonus+=monster.getHD();
        }


        // if strength is below 10 after adding bonus should only be +2

        int strength = monster.abiStr+abilityBonus[0];
        int extraBonus =(int)Math.floor((double)abilityBonus[0]/2);


        //Log.w(Constants.TAG, "STR:"+str);
        //Log.w(Constants.TAG, "IS TWO HANDED :"+isTwoHandedOrSingleNatural(attack));
        // give 1.5 if str>10 and correct attack type
        if (powerfulCharge!=null){
            extraBonus = (int)Math.floor((double)extraBonus*powerfulCharge.strengthMultiplier);
        } else if ( (attack.attackType == Attack.ATK_RANGED_GIANT || isTwoHandedOrSingleNatural(attack, attackExtraInformation) ) && strength >12){
            //Log.w(Constants.TAG, "SETTING STR BONUS TO 1.5");
            extraBonus = (int)Math.floor((double)extraBonus*1.5);
        } else if (attack.attackType == Attack.ATK_OFF_HAND_OR_SECONDARY){
            // half the str bonus if off hand or secondary
            extraBonus = (int)Math.floor((double)extraBonus/2);
        }

        //Log.w(Constants.TAG, "STRE BONUS "+strBonus);
        if (addStrToDamage(attack)){
            //Log.w(Constants.TAG, "ADDING STR BONUS "+strBonus);
            bonus+=extraBonus;
        }






        // remove 1.5 amount if an extra evolved attack has been added
        bonus+=attackExtraInformation.evolvedStrengthModifier;

        //Log.w(Constants.TAG, "BONUS a "+bonus);

        return bonus;
    }



    public static int getDamageDiceNumber(ActiveDetails activeDetails, Attack attack, boolean hideVitalStrike, PowerfulCharge powerfulCharge){


        AttackExtraInformation attackExtraInformation = activeDetails.hashMapAttackExtraInformation.get(attack);
        Monster monster = activeDetails.monster;
        int damageDiceIncrease = activeDetails.damageDiceIncrease;

        int diceNumber = attack.damageDiceNumber;

        if (powerfulCharge!=null){
            diceNumber = powerfulCharge.numDice;
        }

        String diceString = Integer.toString(attack.damageDiceNumber)+"d"+Integer.toString(attack.damageDiceSize);
        if (attackExtraInformation.increaseDice) {
            if (Constants.HASHMAP_DICE_INCREASE.containsKey(diceString)){
                diceString = Constants.HASHMAP_DICE_INCREASE.get(diceString);
                try{
                    diceNumber =  Integer.parseInt(diceString.split("d")[0]);
                } catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }
        for (int i=0; i<damageDiceIncrease; i++){
            if (Constants.HASHMAP_DICE_INCREASE.containsKey(diceString)){
                diceString = Constants.HASHMAP_DICE_INCREASE.get(diceString);
                try{
                    diceNumber =  Integer.parseInt(diceString.split("d")[0]);
                } catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }


        if (attackExtraInformation.vitalStrike && !hideVitalStrike){
            if (monster.isImprovedVitalStrike()){
                diceNumber*=3;
            } else {
                diceNumber*=2;
            }
        }

        return diceNumber;
    }
    public static int getDamageDiceSize(ActiveDetails activeDetails, Attack attack, PowerfulCharge powerfulCharge){


        AttackExtraInformation attackExtraInformation = activeDetails.hashMapAttackExtraInformation.get(attack);
        int damageDiceIncrease = activeDetails.damageDiceIncrease;

        int diceSize = attack.damageDiceSize;
        if (powerfulCharge!=null){
            diceSize = powerfulCharge.diceSize;
        }

        String diceString = Integer.toString(attack.damageDiceNumber)+"d"+Integer.toString(attack.damageDiceSize);;
        if (attackExtraInformation.increaseDice) {
            if (Constants.HASHMAP_DICE_INCREASE.containsKey(diceString)){
                diceString = Constants.HASHMAP_DICE_INCREASE.get(diceString);
                try{
                    diceSize =  Integer.parseInt(diceString.split("d")[1]);
                } catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }

        for (int i=0; i<damageDiceIncrease; i++){
            if (Constants.HASHMAP_DICE_INCREASE.containsKey(diceString)){
                diceString = Constants.HASHMAP_DICE_INCREASE.get(diceString);
                try{
                    diceSize =  Integer.parseInt(diceString.split("d")[1]);
                } catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }
        }

        return diceSize;
    }

    public static int getPowerAttackAttackPenalty(Monster monster){
        return (int) Math.floor((double)monster.bab / 4) + 1;
    }
    public static int getPowerAttackDamageBonus(Monster monster, Attack attack, AttackExtraInformation attackExtraInformation){

        int bonus = 2 * ((int) Math.floor((double)monster.bab / 4) + 1);

        if (isTwoHandedOrSingleNatural(attack, attackExtraInformation)){
            bonus = (int) Math.floor(bonus * 1.5);
        }
        return bonus;
    }

    public static boolean isTwoHandedOrSingleNatural(Attack attack, AttackExtraInformation attackExtraInformation){
        // return true if augment is 3 and there is no 1.5 penalty from an additional evolved attack
        if (attackExtraInformation.evolvedBiteIncrease){
            return true;
        }

        if (attack.attackType ==Attack.ATK_TWOHANDED_OR_SINGLE_NATURAL &&
                attackExtraInformation.evolvedStrengthModifier ==0){
            //Log.w(Constants.TAG, "RETURNING TRUE FOR TWO HANDED");
            return true;
        }
        //Log.w(Constants.TAG, "RETURNING false FOR TWO HANDED");
        return false;
    }



    public static PowerfulCharge getPowerfulCharge(ActiveDetails activeDetails, Attack attack){
        if (activeDetails.hashMapAttackExtraInformation.get(attack).powerfulCharge)
        if (activeDetails.monster.powerfulCharge.length()>0){
            PowerfulCharge powerfulCharge = new PowerfulCharge(activeDetails.monster.powerfulCharge);
            if (attack.fullName.contains(powerfulCharge.reqAttack)){

                return powerfulCharge;


            }
        }
        return null;
    }
}
