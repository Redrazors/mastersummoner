package com.redrazors.mastersummoner.staticmethods;

import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.TemplateInfo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ChallengeRatingStatics {

    private static final Map<String, Double> HASHMAP_FRACTIONS;
    static {
        Map<String, Double> aMap = new HashMap<>();
        aMap.put("1/2", 0.5);
        aMap.put("1/3", 0.3);
        aMap.put("1/4", 0.25);
        aMap.put("1/6", 0.16);
        aMap.put("1/8", 0.125);

        HASHMAP_FRACTIONS = Collections.unmodifiableMap(aMap);
    }

    public static double getCR(TemplateInfo templateInfo, Monster monster, boolean giantSummon){

        double cr=0;
        if (HASHMAP_FRACTIONS.containsKey(monster.cr)){
            cr = HASHMAP_FRACTIONS.get(monster.cr);
        } else {
            try{
                 cr = Double.parseDouble(monster.cr);
            } catch (NumberFormatException e){
                e.printStackTrace();
            }
        }

        if (giantSummon)cr+=1;

        if (templateInfo!=null && monster.getHD()>=5){
            cr+=1;
        }

        return cr;


    }
}
