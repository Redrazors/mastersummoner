package com.redrazors.mastersummoner.staticmethods;

import android.util.Log;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.data.SummonListItem;

import java.util.ArrayList;
import java.util.List;

public class ShadowCaller {

    private static final String[] ILLEGALS =new String[]{"elemental", "lemure", "lantern archon", "dretch", "hell hound", "hound archon", "mephit",
            "babau", "bearded devil", "bralani", "salamander", "xill", "erinye", "lillend azata", "succubus", "bebelith", "bone devil", "vrock"};

    public static final String[] LEGAL_EIGHT = new String[] {"Derghodaemon", "Young Umbral Dragon"};
    public static final String[] LEGAL_NINE = new String[] {"Interlocutor", "Nightwing"};
    public static List<SummonListItem> getPurifiedList(int summonLevel, List<SummonListItem> listItems){

        List<SummonListItem> listRemove = new ArrayList<>();
        if (summonLevel<8){
            for (SummonListItem summonListItem: listItems){
                for (String illegal: ILLEGALS){
                    if (summonListItem.name.toLowerCase().contains(illegal)){
                        listRemove.add(summonListItem);
                    }
                }
            }
        } else if (summonLevel==8){
            for (SummonListItem summonListItem: listItems){
                //Log.w(Constants.TAG, "CHECKING "+summonListItem.name);
                boolean found=false;
                for (String legal: LEGAL_EIGHT){
                    if (summonListItem.name.equals(legal)){
                        found=true;
                    }
                }
                if (!found){
                    listRemove.add(summonListItem);
                }

            }
        }  else if (summonLevel==9){
            for (SummonListItem summonListItem: listItems){
                boolean found=false;
                for (String legal: LEGAL_NINE){
                    if (summonListItem.name.equals(legal)){
                        found=true;
                    }
                }
                if (!found){
                    listRemove.add(summonListItem);
                }
            }
        }

        for (SummonListItem summonListItem: listRemove){
            listItems.remove(summonListItem);
        }
        return listItems;
    }
}
