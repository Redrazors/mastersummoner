package com.redrazors.mastersummoner.staticmethods;

import android.content.Context;
import android.util.Log;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.TemplateInfo;
import com.redrazors.mastersummoner.dice.Dice;
import com.redrazors.mastersummoner.templates.TemplateControl;

public class SpecialAttackStatics {


    public static String getSpecialAttacksString(ActiveDetails activeDetails, TemplateInfo templateInfo, Monster monster){

        String specialAttacks = monster.specialAttacks;

        int strBonus = activeDetails.getExtraAbilityBonus(0);
        int conBonus = activeDetails.getExtraAbilityBonus(2);

        if (strBonus>0){

            specialAttacks = getRakeReplaced(specialAttacks, strBonus, monster);

            specialAttacks = getTrampleReplaced(specialAttacks, strBonus, monster);

            specialAttacks = getConstrictReplaced(specialAttacks, strBonus, monster);

            specialAttacks = getPowerfulChargeReplaced(specialAttacks, strBonus, monster);

            specialAttacks = getEntrapReplaced(specialAttacks, conBonus, monster);

        }

        // finally add on template
        return TemplateControl.getSpecialAttacks(templateInfo, specialAttacks);
    }


    private static String getRakeReplaced(String specialAttacks, int strBonus, Monster monster){

        if (monster.rakeHit==null || monster.rakeDamage==null){
            return specialAttacks;
        }

        try{
            int rakeHit = monster.rakeHit+strBonus;
            String raekHitOriginal = "+"+Integer.toString(monster.rakeHit);
            String rakeHitNew = "+"+Integer.toString(rakeHit);

            specialAttacks = specialAttacks.replace(raekHitOriginal, rakeHitNew);

            String rakeDamageOriginal = monster.rakeDamage;
            Dice dice = new Dice(monster.rakeDamage);
            specialAttacks = specialAttacks.replace(rakeDamageOriginal, dice.getDiceString(getStrDamBonus(strBonus, monster)));

        } catch (Exception e){
            e.printStackTrace();

        }

        return specialAttacks;

    }

    private static String getTrampleReplaced(String specialAttacks, int strBonus, Monster monster){
        if (monster.trample==null || monster.trampleDC==null){
            return specialAttacks;
        }

        try{
            Dice dice = new Dice(monster.trample);
            specialAttacks = specialAttacks.replace(monster.trample, dice.getDiceString(getStrDamBonus(strBonus, monster)));

            String newDC = "DC "+Integer.toString(monster.trampleDC+strBonus);
            String oldDC = "DC "+Integer.toString(monster.trampleDC);

            specialAttacks = specialAttacks.replace(oldDC, newDC);

        } catch (Exception e){
            e.printStackTrace();

        }
        return specialAttacks;
    }


    private static String getEntrapReplaced(String specialAttacks, int conBonus, Monster monster){
        if (monster.entrap==null){
            return specialAttacks;
        }

        // entrap (DC 14, 10 minutes, hardness 5, hp 5)
        try{
            String oldDC = "entrap (DC "+Integer.toString(monster.entrap);
            String newDC = "entrap (DC "+Integer.toString(monster.entrap+conBonus);

            specialAttacks = specialAttacks.replace(oldDC, newDC);

        } catch (Exception e){
            e.printStackTrace();

        }
        return specialAttacks;
    }

    private static String getConstrictReplaced(String specialAttacks, int strBonus, Monster monster){
        if (monster.constrict==null){
            return specialAttacks;
        }

        try{
            Dice dice = new Dice(monster.constrict);
            specialAttacks = specialAttacks.replace(monster.constrict, dice.getDiceString(getStrDamBonus(strBonus, monster)));

        } catch (Exception e){
            e.printStackTrace();

        }
        return specialAttacks;
    }

    private static String getPowerfulChargeReplaced(String specialAttacks, int strBonus, Monster monster){
        if (monster.powerfulCharge.length()>0){
            try{
                String[] split = monster.powerfulCharge.split("&");

                String damageDice = split[1];
                double strengthMultiplier = Double.parseDouble(split[2]);

                strBonus = (int)Math.floor((double)strBonus*strengthMultiplier);

                Dice dice = new Dice(damageDice);

                specialAttacks = specialAttacks.replace(damageDice, dice.getDiceString(strBonus));



            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return specialAttacks;
    }

    private static int getStrDamBonus(int strBonus, Monster monster){
        if (monster.listMeleeAttacks.size()==1){
            return (int)Math.floor((double)strBonus*1.5);
        }
        return strBonus;
    }
}
