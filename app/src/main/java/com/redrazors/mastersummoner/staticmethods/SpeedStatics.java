package com.redrazors.mastersummoner.staticmethods;

import com.redrazors.mastersummoner.active.SpeedSet;
import com.redrazors.mastersummoner.data.Monster;

public class SpeedStatics {

    public static SpeedSet getSpeedSet(Monster monster){
        SpeedSet speedSet = new SpeedSet();
        String[] split = monster.speed.split(",");

        for (String string: split){
            if (string.contains("swim")){
                speedSet.swim =getNumericValue("swim", string);
            } else if (string.contains("climb")){
                speedSet.climb =getNumericValue("climb", string);
            } else if (string.contains("burrow")){
                speedSet.burrow =getNumericValue("burrow", string);
            } else if (string.contains("jet")) {
                speedSet.jet =getNumericValue("jet", string);
            } else if (string.contains("fly")){
                speedSet.fly =getNumericValue("fly", string);
                try{
                    speedSet.maneuverability = string.split("\\(")[1].split("\\)")[0];
                } catch (ArrayIndexOutOfBoundsException e){
                    e.printStackTrace();
                }

            } else {
                speedSet.speed = getNumericValue("", string);
            }
        }

        return speedSet;
    }

    private static int getNumericValue(String key, String string){
        string = string.replace("(ice and snow only)", "");
        string = string.replace(key, "");
        string = string.split("ft.")[0].trim();

        return Integer.parseInt(string);
    }
}
