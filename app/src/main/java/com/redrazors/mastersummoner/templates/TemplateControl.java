package com.redrazors.mastersummoner.templates;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.data.FeatFlags;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.TemplateInfo;
import com.redrazors.mastersummoner.staticmethods.ChallengeRatingStatics;

import java.util.ArrayList;
import java.util.List;

public class TemplateControl {



    public static List<String> getListEligibleTemplates(FeatFlags featFlags, Monster monster, boolean hasTemplate){
        List<String> listTemplates = new ArrayList<>();

        if (featFlags.modeSummonMonster){
            listTemplates = getListSummonMonstersTemplates(featFlags, monster, hasTemplate);
        } else {
            listTemplates = getListSummonNaturesAllyTemplates(featFlags, monster);
        }



        return listTemplates;
    }

    private static List<String> getListSummonMonstersTemplates(FeatFlags featFlags, Monster monster, boolean hasTemplate){
        List<String> listTemplates = new ArrayList<>();

        listTemplates.add(Constants.TEMPLATE_NONE);

        if (!hasTemplate){
            return listTemplates;
        }

        listTemplates.add(Constants.TEMPLATE_CELESTIAL);
        listTemplates.add(Constants.TEMPLATE_FIENDISH);
        listTemplates.add(Constants.TEMPLATE_RESOLUTE);
        listTemplates.add(Constants.TEMPLATE_ENTROPIC);

        if (featFlags.hashMapFlags.get(Constants.FEAT_NEUTRAL)){
            listTemplates.add(Constants.TEMPLATE_COUNTERPOISED);
        }

        if (featFlags.hashMapFlags.get(Constants.ARCHETYPE_SHADOW_CALLER) || featFlags.hashMapFlags.get(Constants.ARMY_OF_DARkNESS)){
            listTemplates.add(Constants.TEMPLATE_SHADOW);
        }

        if (featFlags.versatile){
            // non-outsider with none of the subtypes that follow: air, cold, earth, fire, or water
            listTemplates.add(Constants.TEMPLATE_PRIMORDIAL);
            try{
                String typeSubtype = "";
                if (monster.type!=null){
                    typeSubtype+=monster.type;
                }
                if (monster.subType!=null){
                    typeSubtype+=monster.subType;
                }
                typeSubtype = typeSubtype.toLowerCase();

                if (!typeSubtype.contains("outsider")
                        && !typeSubtype.contains("fire")
                        && !typeSubtype.contains("cold")
                        && !typeSubtype.contains("earth")
                        && !typeSubtype.contains("air")
                        && !typeSubtype.contains("water")){

                    listTemplates.add(Constants.TEMPLATE_AERIAL);
                    listTemplates.add(Constants.TEMPLATE_AQUEOUS);
                    listTemplates.add(Constants.TEMPLATE_FIERY);
                    listTemplates.add(Constants.TEMPLATE_DARK);
                    listTemplates.add(Constants.TEMPLATE_CHTHONIC);

                }


            } catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }



        }
        return listTemplates;
    }

    private static List<String> getListSummonNaturesAllyTemplates(FeatFlags featFlags, Monster monster){
        List<String> listTemplates = new ArrayList<>();

        listTemplates.add(Constants.TEMPLATE_NONE);

        if (featFlags.versatile){

            listTemplates.add(Constants.TEMPLATE_PRIMORDIAL);

            try{
                String type = "";
                if (monster.type!=null){
                    type+=monster.type.toLowerCase();
                }


                if (type.contains("humanoid")
                        || type.contains("animal")
                        || type.contains("vermin")){

                    listTemplates.add(Constants.TEMPLATE_AERIAL);
                    listTemplates.add(Constants.TEMPLATE_AQUEOUS);
                    listTemplates.add(Constants.TEMPLATE_FIERY);
                    listTemplates.add(Constants.TEMPLATE_DARK);
                    listTemplates.add(Constants.TEMPLATE_CHTHONIC);

                }


            } catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }
        }

        return listTemplates;
    }


    public static String getDefaultTemplate(FeatFlags featFlags){
        if (featFlags.hashMapFlags.get(Constants.FEAT_NEUTRAL)){
            return Constants.TEMPLATE_COUNTERPOISED;
        }
        if (featFlags.hashMapFlags.get(Constants.FEAT_EVIL)){
            return Constants.TEMPLATE_FIENDISH;
        }
        return Constants.TEMPLATE_CELESTIAL;
    }

    public static String getTemplateSubtype(String template){
        if (template.equals(Constants.TEMPLATE_AERIAL)){
            return "air";
        }
        if (template.equals(Constants.TEMPLATE_AQUEOUS)){
            return "water";
        }
        if (template.equals(Constants.TEMPLATE_CHTHONIC)){
            return "earth";
        }
        if (template.equals(Constants.TEMPLATE_FIERY)){
            return "fire";
        }
        if (template.equals(Constants.TEMPLATE_SHADOW)){
            return "augmented";
        }
        return "";
    }

    public static String getTemplateResists(Monster monster, TemplateInfo templateInfo){
        if (templateInfo==null){
            return "";
        }
        return templateInfo.getResists(monster.getHD());
    }

    public static String getTemplateDR(Monster monster, TemplateInfo templateInfo){
        if (templateInfo==null){
            return "";
        }
        return templateInfo.getDamageResist(monster.getHD());
    }

    public static int getTemplateSR(Monster monster, TemplateInfo templateInfo, boolean giantSummon){
        if (templateInfo==null){
            return 0;
        }
        double cr = ChallengeRatingStatics.getCR(templateInfo, monster, giantSummon);

        return templateInfo.getSR(cr, monster.getHD());
    }

    public static String getSenses(TemplateInfo templateInfo, String senses){
        if (templateInfo==null){
            return senses;
        }
        String templateSenses = templateInfo.senses;
        if (templateSenses==null) return senses;

        if (senses.contains(templateSenses)) return senses;

        String part1 = senses.split(";")[0];
        part1+=", "+templateSenses;

        try{
            return part1+";"+senses.split(";")[1];
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        return senses;

    }

    public static String getSpecialAttacks(TemplateInfo templateInfo, String specialAttacks){
        if (templateInfo==null){
            return specialAttacks;
        }
        String templateSpecial = templateInfo.specialAttack;
        if (templateSpecial==null) return specialAttacks;

        if (specialAttacks.length()>0){
            specialAttacks+="; ";
        }
        return specialAttacks+templateSpecial;
    }
    public static String getSpecialAbilities(TemplateInfo templateInfo, String specialAbilities){
        if (templateInfo==null){
            return specialAbilities;
        }
        String templateSpecial = templateInfo.specialAbilities;
        if (templateSpecial==null) return specialAbilities;


        return specialAbilities+templateSpecial;
    }



//    public String getTemplateInjectedStatBlock(DataControl dataControl, String template){
//
//        TemplateInfo templateInfo = dataControl.hashMapTemplates.get(template);
//        if (templateInfo==null){
//            Log.w(Constants.TAG, "NULL TEMPLATE "+template);
//            return "";
//        }
//
//
//        String injection = "<hr/><div><h5><b>"+template.toUpperCase()+" TEMPLATE</b></h5></div><hr/><div>";
//        if (templateInfo.senses!=null){
//            injection +="<h5><b>Senses </b>"+templateInfo.senses+"</h5>";
//        }
//        if (templateInfo.speed!=null){
//            injection +="<h5><b>Speed </b>"+templateInfo.speed+"</h5>";
//        }
//
//        String dr=templateInfo.getDamageResist(monsterInfo.hitDice);
//        String resists = templateInfo.getResists(monsterInfo.hitDice);
//
//
//        String sr = templateInfo.getSR(getCR(), monsterInfo.hitDice);
//
//        if (dr.length()>0 || resists.length()>0 || sr.length()>0){
//            injection +="<h5>";
//
//            if (dr.length()>0){
//                injection+="<b>DR </b>"+dr+"; ";
//            }
//            if (resists.length()>0){
//                injection+="<b>Resist </b>"+resists+"; ";
//            }
//            if (sr.length()>0){
//                injection+="<b>SR </b>"+sr;
//            }
//
//            injection += "</h5>";
//        }
//
//        if (templateInfo.specialAttack!=null){
//            injection+="<h5><b>Special </b>"+templateInfo.specialAttack+"</h5>";
//        }
//
//        if (template.equals(Constants.TEMPLATE_PRIMORDIAL)){
//            String slas = "Dancing Lights";
//            if (monsterInfo.hitDice>=5)slas+=", Faerie Fire";
//            if (monsterInfo.hitDice>=10)slas+=", Lesser Confusion";
//            injection+="<h5><b>Spell-Like Abilities</b> gains spell-like abilities:"  + slas +", " +
//                    "each available 1/day. The DCs of any saves against these abilities are equal to 10 + the primordial creature’s Charisma bonus + spell level.</h5>";
//        }
//
//        injection += "</div>";
//
//
//        String defenseHeader = "<hr/><div><h5><b>DEFENSE</b></h5></div><hr/>";
//
//        injection += defenseHeader;
//
//        templateInjectedStatBlock = templateInjectedStatBlock.replace(defenseHeader, injection);
//
//        // title
//        //String titleTop="<div class=\"heading\"><p class=\"alignleft\">";
//        //String newTitle = titleTop+template+" ";
//
//        // cr
////        String xp = "<b>XP </b>";
////        String newXp = "<b>CR </b>"+Double.toString(monsterInfo.cr)+"; <b>XP </b>";
//
//        //adjustedText = adjustedText.replace(titleTop, newTitle);
//        String copyright = "<div><h5>Source: "+monsterInfo.src+"</h5></div>";
//        templateInjectedStatBlock= templateInjectedStatBlock+copyright;
//        return templateInjectedStatBlock;
//    }
}
