package com.redrazors.mastersummoner.views;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.views.fragments.FragmentActive;
import com.redrazors.mastersummoner.views.fragments.FragmentOptions;
import com.redrazors.mastersummoner.views.fragments.FragmentSummon;


public class MainActivityTabsPagerAdapter extends FragmentStatePagerAdapter {

    private MainActivity mainActivity;


    public MainActivityTabsPagerAdapter(FragmentManager fm, MainActivity mainActivity) {
        super(fm);
        this.mainActivity = mainActivity;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new FragmentSummon();
            case 1:
                return new FragmentActive();
            case 2:
                return new FragmentOptions();
        }


        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }


}