package com.redrazors.mastersummoner.views;

import android.graphics.Color;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.widget.TabHost;
import android.widget.TextView;

import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.views.fragments.FragmentActive;
import com.redrazors.mastersummoner.views.fragments.FragmentSummon;

public class TabControl implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

    private MainActivityTabsPagerAdapter mAdapter;
    private ViewPager mViewPager;
    private TabHost mTabHost;
    private MainActivity mainActivity;

    public TabControl (MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    public void initialiseControl(){
        mViewPager = (ViewPager) mainActivity.findViewById(R.id.viewpager);

        // Tab Initialization
        initialiseTabHost();
        mAdapter = new MainActivityTabsPagerAdapter(mainActivity.getSupportFragmentManager(), mainActivity);

        // Fragments and ViewPager Initialization
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setCurrentItem(0);
        mViewPager.setOnPageChangeListener(this);



        initTabsAppearance();
    }

    private void initTabsAppearance() {
        // Change background
        for (int i = 0; i < mTabHost.getTabWidget().getTabCount(); i++)
            mTabHost.getTabWidget().getChildTabViewAt(i).setBackgroundResource(R.drawable.tab_bg);
    }


    public ViewPager getMViewPager() {
        return mViewPager;
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    // Manages the Page changes, synchronizing it with Tabs
    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        int pos = this.mViewPager.getCurrentItem();
        this.mTabHost.setCurrentTab(pos);
    }

    @Override
    public void onPageSelected(int arg0) {
        //Log.w(Constants.TAG, "PAGE SELECTED "+arg0);
    }


    // Tabs Creation
    private void initialiseTabHost() {
        mTabHost = (TabHost) mainActivity.findViewById(android.R.id.tabhost);
        mTabHost.setup();

        // graphics set as so:
        // MainActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("StatsTab").setIndicator("Summary", getResources().getDrawable(R.drawable.ic_launcher)));

        AddTab(mainActivity, this.mTabHost, this.mTabHost.newTabSpec("SummonTab").setIndicator("Summon", null));
        AddTab(mainActivity, this.mTabHost, this.mTabHost.newTabSpec("ActiveTab").setIndicator("Active", null));
        AddTab(mainActivity, this.mTabHost, this.mTabHost.newTabSpec("OptionsTab").setIndicator("Options", null));

        mTabHost.setOnTabChangedListener(this);

//        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
//
//        }

        for(int i=0;i<mTabHost.getTabWidget().getChildCount();i++)
        {
            TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.WHITE);
            mTabHost.getTabWidget().getChildAt(i).getLayoutParams().height /= 1.50;
            tv.setPadding(0, 0, 0, 0);
            tv.setShadowLayer(2, 2, 2, Color.BLACK);
        }
    }



    // Manages the Tab changes, synchronizing it with Pages
    public void onTabChanged(String tag) {
        int pos = this.mTabHost.getCurrentTab();

        this.mViewPager.setCurrentItem(pos);

    }


    // Method to add a TabHost
    private static void AddTab(MainActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    public FragmentActive getFragmentActive(){
        return (FragmentActive) getFragment(1);
    }
    public FragmentSummon getFragmentSummon(){
        return (FragmentSummon) getFragment(0);
    }

    private Fragment getFragment(int ref) {
        if (mViewPager == null) {
            return null;
        }
        return (Fragment) mAdapter.instantiateItem(mViewPager,ref);
        //return (UpdateFragment) mainActivity.getSupportFragmentManager().findFragmentByTag(getFragmentTag(ref));
    }
}
