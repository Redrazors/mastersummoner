package com.redrazors.mastersummoner.views.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.active.SpeedSet;
import com.redrazors.mastersummoner.data.DataControl;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.data.SummonListItem;
import com.redrazors.mastersummoner.staticmethods.SpeedStatics;

import java.util.HashMap;
import java.util.List;


public class ListViewAdapterSummonList extends BaseAdapter {


    private HashMap<String, SummonListItem> hashMapItems = new HashMap<>();

    private List<SummonListItem> listItems;
    public SummonListItem currentSelection;
    public MainActivity mainActivity;
    private int summonLevel;


    public ListViewAdapterSummonList(MainActivity mainActivity, List<SummonListItem> listItems, int summonLevel) {
        super();
        this.mainActivity = mainActivity;
        this.listItems = listItems;
        this.summonLevel = summonLevel;

    }





    public void onItemSelected(View view, int position){
        ViewHolder holder = (ViewHolder) view.getTag();


        if (listItems.size()>position){
            currentSelection = listItems.get(position);
        }


    }


    private class ViewHolder {
        TextView textViewName;
        ImageView iconFly;
        ImageView iconSwim;
        ImageView iconBurrow;
        ImageView iconClimb;
        ImageView iconPower;
        ImageView iconVital;
        ImageView iconRanged;
        ImageView iconSpells;
        ImageView iconHeal;



    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        LayoutInflater inflater = mainActivity.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_item_summons, null);
            holder = new ViewHolder();
            holder.textViewName =  convertView.findViewById(R.id.textview_name);
            holder.iconFly =  convertView.findViewById(R.id.icon_fly);
            holder.iconSwim =  convertView.findViewById(R.id.icon_swim);
            holder.iconBurrow =  convertView.findViewById(R.id.icon_burrow);
            holder.iconClimb =  convertView.findViewById(R.id.icon_climb);
            holder.iconPower =  convertView.findViewById(R.id.icon_power);
            holder.iconVital =  convertView.findViewById(R.id.icon_vital);
            holder.iconRanged =  convertView.findViewById(R.id.icon_ranged);
            holder.iconSpells =  convertView.findViewById(R.id.icon_spells);
            holder.iconHeal =  convertView.findViewById(R.id.icon_heal);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if (listItems.size()>position){
            SummonListItem summonListItem = listItems.get(position);


            String name = summonListItem.name;

            if (summonListItem.template){
                name+="*";
            }

            if (isAltnerativeSummon(summonListItem)){
                name+=" ("+summonListItem.source+")";
            }
            holder.textViewName.setText(name);



            Monster monster = getDataControl().bestiary.hashMapMonsters.get(summonListItem.name);

            if (monster!=null){

                SpeedSet speedSet = SpeedStatics.getSpeedSet(monster);

                if (speedSet.flys()){
                    holder.iconFly.setVisibility(View.VISIBLE);
                } else {
                    holder.iconFly.setVisibility(View.GONE);
                }

                if (speedSet.swims()){
                    holder.iconSwim.setVisibility(View.VISIBLE);
                } else {
                    holder.iconSwim.setVisibility(View.GONE);
                }

                if (speedSet.burrows()){
                    holder.iconBurrow.setVisibility(View.VISIBLE);
                } else {
                    holder.iconBurrow.setVisibility(View.GONE);
                }

                if (speedSet.climbs()){
                    holder.iconClimb.setVisibility(View.VISIBLE);
                } else {
                    holder.iconClimb.setVisibility(View.GONE);
                }

                if (monster.isPowerAttack()){
                    holder.iconPower.setVisibility(View.VISIBLE);
                } else {
                    holder.iconPower.setVisibility(View.GONE);
                }

                if (monster.isVitalStrike()){
                    holder.iconVital.setVisibility(View.VISIBLE);
                } else {
                    holder.iconVital.setVisibility(View.GONE);
                }
                if (monster.isRanged()){
                    holder.iconRanged.setVisibility(View.VISIBLE);
                } else {
                    holder.iconRanged.setVisibility(View.GONE);
                }
                if (monster.isSpellCaster()){
                    holder.iconSpells.setVisibility(View.VISIBLE);
                } else {
                    holder.iconSpells.setVisibility(View.GONE);
                }
                if (monster.isHealer()){
                    holder.iconHeal.setVisibility(View.VISIBLE);
                } else {
                    holder.iconHeal.setVisibility(View.GONE);
                }

            } else {
                Log.w(Constants.TAG, "NULL MONSTER "+summonListItem.name);
            }


        }


        return convertView;
    }



    private boolean isAltnerativeSummon(SummonListItem summonListItem){
        if (mainActivity.dataControl.hashMapSummonListItems.get(Constants.FEAT_ALTERNATIVE).containsKey(summonLevel)){
            if (mainActivity.dataControl.hashMapSummonListItems.get(Constants.FEAT_ALTERNATIVE).get(summonLevel).contains(summonListItem)
                    && mainActivity.dataControl.featFlags.hashMapFlags.get(Constants.FEAT_ALTERNATIVE)){
                return true;
            }
        }

        return false;
    }






    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public int getPositionOfItem(String item){
        if (listItems.contains(item)){
            return listItems.indexOf(item);
        }
        return 0;
    }

    public DataControl getDataControl(){
        return mainActivity.dataControl;
    }


}

