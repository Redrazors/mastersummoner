package com.redrazors.mastersummoner.views.adapters;

import android.widget.BaseAdapter;

import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.data.DataControl;

import java.util.ArrayList;

/**
 * Created by david on 03/09/2018.
 */

public abstract class ListViewAdapterTemplate extends BaseAdapter {

    public MainActivity mainActivity;


    public ArrayList<String> listItems = new ArrayList<>();
    public String currentSelection;

    public ListViewAdapterTemplate(MainActivity mainActivity){
        super();
        this.mainActivity = mainActivity;

    }

    public void setCurrentSelection(String item) {
        currentSelection = item;
        if (!listItems.contains(currentSelection) && listItems.size()>0){
            currentSelection= listItems.get(0);
        }
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public int getPositionOfItem(String item){
        if (listItems.contains(item)){
            return listItems.indexOf(item);
        }
        return 0;
    }

    public DataControl getDataControl(){
        return mainActivity.dataControl;
    }

}
