package com.redrazors.mastersummoner.views.dialogs;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.active.AttackExtraInformation;
import com.redrazors.mastersummoner.active.PowerfulCharge;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.views.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 21/06/2016.
 */
public class DialogBeginAttack extends DialogTemplate {


    private static final int CENTREPOINT=50;

    private ActiveDetails activeDetails;
    private Monster monster;
    private Attack attack;
    private int sequenceRef;

    private String id;
    private boolean negativeAttackWheel, negativeDamageWheel;
    //private WheelView wheelViewAttackBonus, wheelViewDamageBonus;


    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_begin_attack, container, false);
    }

    @Override
    public void onCreate() {


        id = getArguments().getString(Constants.ARGUMENT_SUMMON_ID);
        attack = (Attack) getArguments().getSerializable(Constants.ARGUMENT_ATTACK);

        if (attack==null){
            Toast.makeText(mainActivity, "Attack not found!", Toast.LENGTH_LONG).show();
            dismiss();
            return;
        }


        sequenceRef = getArguments().getInt(Constants.ARGUMENT_SEQUENCE_REF);


        activeDetails = mainActivity.summoner.hashMapSummons.get(id);
        //activeSummon.hashMapAttackExtraInformation.get(attack).attackBonus=0;
        //activeSummon.hashMapAttackExtraInformation.get(attack).damageBonus=0;
        if (activeDetails==null || attack==null){
            dismiss();
            return;
        }

        if (!activeDetails.hashMapAttackExtraInformation.containsKey(attack)){
            activeDetails.hashMapAttackExtraInformation.put(attack, new AttackExtraInformation());
        }


        monster = mainActivity.dataControl.bestiary.hashMapMonsters.get(activeDetails.name);


        CheckBox checkboxIncreaseDice = rootView.findViewById(R.id.checkbox_increase_dice);

        checkboxIncreaseDice.setChecked(activeDetails.hashMapAttackExtraInformation.get(attack).increaseDice);
        checkboxIncreaseDice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                activeDetails.hashMapAttackExtraInformation.get(attack).increaseDice=isChecked;
                setTitle();
            }
        });

       setTitle();


        final WheelView wheelViewAttackBonus = rootView.findViewById(R.id.wheelview_attack_bonus);
        List<String> items = new ArrayList<>();
        for (int i=-CENTREPOINT; i<CENTREPOINT; i++){
            items.add(Integer.toString(i));
        }
        wheelViewAttackBonus.setItems(items);
        // todo set the sequence
        int position = CENTREPOINT+activeDetails.hashMapAttackExtraInformation.get(attack).attackBonus;
        wheelViewAttackBonus.selectIndex(position);
        setWheelViewColor(wheelViewAttackBonus, position);




        wheelViewAttackBonus.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(WheelView wheelView, int position) {

                Log.w(Constants.TAG, "SELECTED: "+position);
                activeDetails.hashMapAttackExtraInformation.get(attack).attackBonus = position-CENTREPOINT;
                setTitle();
                //activeSummon.hashMapAttackExtraInformation.get(attack).attackBonus=-20+position;

            }

            @Override
            public void onWheelItemChanged(WheelView wheelView, int position) {

                setWheelViewColor(wheelView, position);

            }
        });


        final WheelView wheelViewDamageBonus = rootView.findViewById(R.id.wheelview_damage_bonus);
        wheelViewDamageBonus.setItems(items);
        position = CENTREPOINT+activeDetails.hashMapAttackExtraInformation.get(attack).damageBonus;
        wheelViewDamageBonus.selectIndex(position);
        setWheelViewColor(wheelViewDamageBonus, position);






        wheelViewDamageBonus.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(WheelView wheelView, int position) {

                Log.w(Constants.TAG, "SELECTED: "+position);
                activeDetails.hashMapAttackExtraInformation.get(attack).damageBonus= position-CENTREPOINT;
                setTitle();

            }

            @Override
            public void onWheelItemChanged(WheelView wheelView, int position) {

                Log.w(Constants.TAG, "ON CHANGE: "+position);
                setWheelViewColor(wheelView, position);

            }
        });

        ToggleButton buttonPowerAttack = rootView.findViewById(R.id.button_power_attack);
        if (monster.isPowerAttack() && activeDetails.getListMeleeAttacks().contains(attack)){
            buttonPowerAttack.setVisibility(View.VISIBLE);
            buttonPowerAttack.setChecked(activeDetails.hashMapAttackExtraInformation.get(attack).powerAttack);
            setButtonTextColor(buttonPowerAttack, activeDetails.hashMapAttackExtraInformation.get(attack).powerAttack);
            buttonPowerAttack.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    activeDetails.hashMapAttackExtraInformation.get(attack).powerAttack=isChecked;
                    setTitle();
                    setButtonTextColor(buttonView, isChecked);
                }
            });
        }

        ToggleButton buttonVitalStrike = rootView.findViewById(R.id.button_vital_strike);
        if (monster.isVitalStrike()){
            buttonVitalStrike.setVisibility(View.VISIBLE);
            buttonVitalStrike.setChecked(activeDetails.hashMapAttackExtraInformation.get(attack).vitalStrike);
            setButtonTextColor(buttonVitalStrike, activeDetails.hashMapAttackExtraInformation.get(attack).vitalStrike);
            buttonVitalStrike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    activeDetails.hashMapAttackExtraInformation.get(attack).vitalStrike=isChecked;
                    setTitle();
                    setButtonTextColor(buttonView, isChecked);
                }
            });
        }

        ToggleButton buttonSmite = rootView.findViewById(R.id.button_smite);
        if (isSmiter()){
            buttonSmite.setVisibility(View.VISIBLE);
            buttonSmite.setChecked(activeDetails.hashMapAttackExtraInformation.get(attack).smite);
            setButtonTextColor(buttonSmite, activeDetails.hashMapAttackExtraInformation.get(attack).smite);
            buttonSmite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    activeDetails.hashMapAttackExtraInformation.get(attack).smite=isChecked;
                    setTitle();
                    setButtonTextColor(buttonView, isChecked);
                }
            });
        }

        ToggleButton buttonPowerfulChanrge = rootView.findViewById(R.id.button_powerful_charge);
        if (isPowerfulCharger()){
            buttonPowerfulChanrge.setVisibility(View.VISIBLE);
            buttonPowerfulChanrge.setChecked(activeDetails.hashMapAttackExtraInformation.get(attack).powerfulCharge);
            setButtonTextColor(buttonPowerfulChanrge, activeDetails.hashMapAttackExtraInformation.get(attack).powerfulCharge);
            buttonPowerfulChanrge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    activeDetails.hashMapAttackExtraInformation.get(attack).powerfulCharge=isChecked;
                    setTitle();
                    setButtonTextColor(buttonView, isChecked);
                }
            });
        }

        Button buttonReset = rootView.findViewById(R.id.button_reset);
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wheelViewAttackBonus.smoothSelectIndex(CENTREPOINT);
                wheelViewDamageBonus.smoothSelectIndex(CENTREPOINT);
                activeDetails.hashMapAttackExtraInformation.get(attack).attackBonus = 0;
                activeDetails.hashMapAttackExtraInformation.get(attack).damageBonus = 0;
                setTitle();
            }
        });

        Button buttonRoll = rootView.findViewById(R.id.button_roll);
        buttonRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                mainActivity.diceRoller.rollAttackDice(activeDetails, attack, attack.attackSequence[sequenceRef], sequenceRef);
            }
        });

        Button buttonCancel = rootView.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });



    }

    private void setButtonTextColor(CompoundButton buttonView, boolean isChecked){
        if (isChecked){
            buttonView.setTextColor(getResources().getColor(R.color.green));
        } else {
            buttonView.setTextColor(getResources().getColor(R.color.white));
        }
    }


    private boolean isSmiter(){
        if (activeDetails.template.equals(Constants.TEMPLATE_CELESTIAL)
                || activeDetails.template.equals(Constants.TEMPLATE_FIENDISH)
                || activeDetails.template.equals(Constants.TEMPLATE_COUNTERPOISED)
                || activeDetails.template.equals(Constants.TEMPLATE_ENTROPIC)
                || activeDetails.template.equals(Constants.TEMPLATE_RESOLUTE)){
            return true;
        }
        return false;
    }

    private boolean isPowerfulCharger(){
        if (activeDetails.monster.powerfulCharge.length()>0){
            PowerfulCharge powerfulCharge = new PowerfulCharge(activeDetails.monster.powerfulCharge);
            if (attack.fullName.contains(powerfulCharge.reqAttack)){
                return true;
            }
        }
        return false;
    }


    private void setWheelViewColor(WheelView wheelView, int position){
        if (position<CENTREPOINT){
            wheelView.setHighlightColor(getResources().getColor(R.color.red));
        } else if (position>CENTREPOINT) {
            wheelView.setHighlightColor(getResources().getColor(R.color.green));
        } else {
            wheelView.setHighlightColor(getResources().getColor(R.color.grey_1));
        }
    }


    private void setTitle(){

        TextView textViewTitle = rootView.findViewById(R.id.textview_title);
        textViewTitle.setText(activeDetails.getAttackFullNameAdjustedDice(attack, sequenceRef));
    }




}
