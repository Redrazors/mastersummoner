package com.redrazors.mastersummoner.views.dialogs;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.views.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 21/06/2016.
 */
public class DialogBeginRoll extends DialogTemplate {


    private static final int CENTREPOINT=50;


    private String label;
    private int bonus;
    private int extraBonus;


    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_begin_roll, container, false);
    }

    @Override
    public void onCreate() {


        label = getArguments().getString(Constants.ARGUMENT_LABEL);
        bonus = getArguments().getInt(Constants.ARGUMENT_BONUS);





       setTitle();


        WheelView wheelView = rootView.findViewById(R.id.wheelview_attack_bonus);
        List<String> items = new ArrayList<>();
        for (int i=-CENTREPOINT; i<CENTREPOINT; i++){
            items.add(Integer.toString(i));
        }
        wheelView.setItems(items);

        wheelView.selectIndex(CENTREPOINT);
        setWheelViewColor(wheelView, CENTREPOINT);




        wheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(WheelView wheelView, int position) {

                extraBonus = position-CENTREPOINT;
                Log.w(Constants.TAG, "SELECTED: "+position);
                //activeSummon.hashMapAttackExtraInformation.get(attack).attackBonus = position-CENTREPOINT;
                setTitle();
                //activeSummon.hashMapAttackExtraInformation.get(attack).attackBonus=-20+position;

            }

            @Override
            public void onWheelItemChanged(WheelView wheelView, int position) {

                setWheelViewColor(wheelView, position);

            }
        });





        Button buttonRoll = rootView.findViewById(R.id.button_roll);
        buttonRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                mainActivity.diceRoller.rollD20(getFullLabel(), bonus+extraBonus);
            }
        });

        Button buttonCancel = rootView.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });



    }




    private void setWheelViewColor(WheelView wheelView, int position){
        if (position<CENTREPOINT){
            wheelView.setHighlightColor(getResources().getColor(R.color.red));
        } else if (position>CENTREPOINT) {
            wheelView.setHighlightColor(getResources().getColor(R.color.green));
        } else {
            wheelView.setHighlightColor(getResources().getColor(R.color.grey_1));
        }
    }


    private void setTitle(){

        TextView textViewTitle = rootView.findViewById(R.id.textview_title);
        textViewTitle.setText(getFullLabel());
    }


    private String getFullLabel(){
        return label+ " "+ Utils.getSymbol(bonus+extraBonus)+Integer.toString(bonus+extraBonus);
    }


}
