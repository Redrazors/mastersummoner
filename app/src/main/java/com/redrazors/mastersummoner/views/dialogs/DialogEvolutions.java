package com.redrazors.mastersummoner.views.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.data.Evolution;
import com.redrazors.mastersummoner.data.Monster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by david on 19/09/2018.
 */

public class DialogEvolutions extends DialogTemplate {

    private TextView textViewUsed;
    private ActiveDetails activeDetails;

    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_evolutions, container, false);
    }

    @Override
    public void onCreate() {

        final String id = getArguments().getString(Constants.ARGUMENT_SUMMON_ID);
        LinearLayout layoutParent = rootView.findViewById(R.id.layout_parent);
        textViewUsed = rootView.findViewById(R.id.textview_used);

        activeDetails = mainActivity.summoner.hashMapSummons.get(id);
        if (activeDetails==null){
            dismiss();
            return;
        }
        final Monster monster = dataControl.bestiary.hashMapMonsters.get(activeDetails.name);

        List<String> listEvolutions = new ArrayList<>(dataControl.hashMapEvolutions.keySet());



        if (activeDetails.getSize()<2){
            String[] illegalForSmall = new String[]{
                    "Bite (Ex)", "Bleed (Ex)", "Claws (Ex)", "Hooves (Ex)", "Magic Attacks (Su)", "Pincers (Ex)", "Pull (Ex)",
                    "Push (Ex)", "Slam (Ex)", "Sting (Ex)", "Tail (Ex)", "Tail Slap (Ex)", "Tentacle (Ex)", "Wing Buffet (Ex)"
            };
            for (String illegal: illegalForSmall){
                listEvolutions.remove(illegal);
            }
        }

        Collections.sort(listEvolutions);
        for (final String name: listEvolutions){
            final Evolution evolution = dataControl.hashMapEvolutions.get(name);

            LinearLayout layoutEvolution = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.layout_evolution, null);

            CheckBox checkBoxName = layoutEvolution.findViewById(R.id.checkbox_evolution_name);
            TextView textViewDescription = layoutEvolution.findViewById(R.id.textview_description);
            final Spinner spinnerAmount = layoutEvolution.findViewById(R.id.spinner_amount);

            List<String> listAmount = new ArrayList<>();
            for (int i=1; i<=5; i++){
                listAmount.add("x"+Integer.toString(i));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(mainActivity, R.layout.spinner_item_small_white, listAmount);
            spinnerAmount.setAdapter(adapter);
            if (activeDetails.hashMapEvolutionTimesTaken.containsKey(name)){
                spinnerAmount.setSelection(activeDetails.hashMapEvolutionTimesTaken.get(name)-1);
            }
            spinnerAmount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Evolution evolution = dataControl.hashMapEvolutions.get(name);
                    evolution.evolutionMethod.activateEvolution(false, monster, activeDetails);

                    activeDetails.hashMapEvolutionTimesTaken.put(name, position+1);

                    evolution.evolutionMethod.activateEvolution(true, monster, activeDetails);

                    setTextViewUsed();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            checkBoxName.setText(name);
            textViewDescription.setText(evolution.description);

            if (activeDetails.listActiveEvolutions.contains(name)){
                checkBoxName.setChecked(true);
                if (evolution.multi){
                    spinnerAmount.setVisibility(View.VISIBLE);
                }
            }
            checkBoxName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try{
                        if (isChecked){
                            activeDetails.listActiveEvolutions.add(name);
                            activeDetails.hashMapEvolutionTimesTaken.put(name, 1);
                            if (evolution.multi){
                                spinnerAmount.setVisibility(View.VISIBLE);
                            }
                            evolution.evolutionMethod.activateEvolution(true, monster, activeDetails);
                        } else {
                            evolution.evolutionMethod.activateEvolution(false, monster, activeDetails);
                            activeDetails.listActiveEvolutions.remove(name);
                            spinnerAmount.setVisibility(View.GONE);
                            spinnerAmount.setSelection(0);
                            activeDetails.hashMapEvolutionTimesTaken.remove(name);
                        }

                        setTextViewUsed();
                    } catch (NullPointerException e){
                        e.printStackTrace();
                    }

                }
            });

            layoutParent.addView(layoutEvolution);
        }

        Button buttonFinished = rootView.findViewById(R.id.button_finished);
        buttonFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.tabControl.getFragmentActive().updateSummons();
                dismiss();
            }
        });

        setTextViewUsed();

    }

    private void setTextViewUsed(){
        int used = 0;
        for (int taken: activeDetails.hashMapEvolutionTimesTaken.values()){
            used+=taken;
        }
        textViewUsed.setText("Evolution Points Used: "+Integer.toString(used));
    }
}
