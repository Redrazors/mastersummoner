package com.redrazors.mastersummoner.views.dialogs;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.views.fragments.FragmentActive;
import com.redrazors.mastersummoner.views.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 21/06/2016.
 */
public class DialogHP extends DialogTemplate {


    private EditText editTextNewHP;
    private int adjustHP;

    private boolean ignoreMax =false;
    private Switch switchIgnoreMax;
    private boolean negative;

    private ActiveDetails activeDetails;
    private Monster monster;
    private String id;

    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_hp, container, false);
    }

    @Override
    public void onCreate() {


        id = getArguments().getString(Constants.ARGUMENT_SUMMON_ID);

        activeDetails = mainActivity.summoner.hashMapSummons.get(id);
        if (activeDetails==null){
            dismiss();
            return;
        }

        monster = mainActivity.dataControl.bestiary.hashMapMonsters.get(activeDetails.name);


        editTextNewHP = rootView.findViewById(R.id.edit_text_new_hp);


        WheelView wheelView = rootView.findViewById(R.id.wheelview);
        List<String> items = new ArrayList<>();
        for (int i=-150; i<150; i++){
            items.add(Integer.toString(i));
        }
        wheelView.setItems(items);
        wheelView.setMaxSelectableIndex(250);
        wheelView.setMinSelectableIndex(50);
        wheelView.selectIndex(150);




        wheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectedListener() {
            @Override
            public void onWheelItemSelected(WheelView wheelView, int position) {

                Log.w(Constants.TAG, "SELECTED: "+position);

                // pos 123, hurt 27
                // pos 160 heal 10

                adjustHP = position-150;
                setTextViews();


            }

            @Override
            public void onWheelItemChanged(WheelView wheelView, int position) {

                if (position<150 && !negative){
                    negative=true;
                    wheelView.setHighlightColor(getResources().getColor(R.color.red));
                } else if (position>150&&negative) {
                    negative=false;
                    wheelView.setHighlightColor(getResources().getColor(R.color.green));
                } else if (position==150){
                    wheelView.setHighlightColor(getResources().getColor(R.color.grey_1));
                }

            }
        });


        TextView textViewMax = rootView.findViewById(R.id.textview_max_hp);
        textViewMax.setText(Integer.toString(activeDetails.getActiveMaxHP()));

        TextView textViewCurrent = rootView.findViewById(R.id.textview_current_hp);
        textViewCurrent.setText(Integer.toString(activeDetails.currentHP));




        switchIgnoreMax = rootView.findViewById(R.id.switch_ignore_max);
        switchIgnoreMax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ignoreMax =b;
            }
        });

        Button buttonAccept = rootView.findViewById(R.id.button_accept);
        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptChange();
            }
        });

        Button buttonCancel = rootView.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        setTextViews();
    }




    private void setTextViews(){

        if (activeDetails.currentHP>activeDetails.getActiveMaxHP()){
            ignoreMax=true;
            switchIgnoreMax.setChecked(true);
        }



        int newCurrent = activeDetails.currentHP+adjustHP;

        if (!ignoreMax){
            if (newCurrent>activeDetails.getActiveMaxHP()){
                newCurrent=activeDetails.getActiveMaxHP();
            }
        }
        editTextNewHP.setText(Integer.toString(newCurrent));


    }



    private void acceptChange(){



        try{
            String editTextHP = editTextNewHP.getText().toString();
            activeDetails.currentHP = Integer.parseInt(editTextHP);


            if (activeDetails.currentHP<=0){

                confirmRemoveSummon();

            } else {
                FragmentActive fragmentActive = mainActivity.tabControl.getFragmentActive();
                if (fragmentActive!=null){
                    fragmentActive.updateHP(id);
                }
            }

            dismiss();

        } catch (NumberFormatException e){
            Toast.makeText(getContext(), "Error parsing number", Toast.LENGTH_LONG).show();
        }


    }


    private void confirmRemoveSummon(){
        new AlertDialog.Builder(mainActivity)
                .setTitle("Remove Summon")
                .setMessage("This summon is dead. Do you want to remove it from your active summons?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        mainActivity.summoner.hashMapSummons.remove(id);

                        FragmentActive fragmentActive = mainActivity.tabControl.getFragmentActive();
                        if (fragmentActive!=null){
                            fragmentActive.updateSummons();
                        }
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FragmentActive fragmentActive = mainActivity.tabControl.getFragmentActive();
                        if (fragmentActive!=null){
                            fragmentActive.updateHP(id);
                        }
                    }
                }).show();
    }
}
