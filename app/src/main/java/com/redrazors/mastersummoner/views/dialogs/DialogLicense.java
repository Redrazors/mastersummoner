package com.redrazors.mastersummoner.views.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;


/**
 * Created by david on 19/09/2018.
 */

public class DialogLicense extends DialogTemplate {

    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_liences, container, false);
    }

    @Override
    public void onCreate() {

        TextView textViewCopyright = rootView.findViewById(R.id.textview_copyrights);
        String copyrights="";
        for (String copyright: Constants.HASHMAP_SOURCES.values()){
            copyrights+=copyright+"\n";
        }
        textViewCopyright.setText(copyrights);

        Button buttonFinished = rootView.findViewById(R.id.button_finished);
        buttonFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }
}
