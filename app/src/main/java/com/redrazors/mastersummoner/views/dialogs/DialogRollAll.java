package com.redrazors.mastersummoner.views.dialogs;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.active.AttackExtraInformation;
import com.redrazors.mastersummoner.active.PowerfulCharge;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.ElementalDice;
import com.redrazors.mastersummoner.dice.AttackResults;
import com.redrazors.mastersummoner.dice.Roll;
import com.redrazors.mastersummoner.staticmethods.AttackDamageStatics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


/**
 * Created by david on 19/09/2018.
 */

public class DialogRollAll extends DialogTemplate {

    private LinearLayout layoutParent;
    private AttackResults attackResults;

    private HashMap<ActiveDetails, Integer> hashMapSummonTotalDamage = new HashMap<>();
    private HashMap<ActiveDetails, TemporaryDataHolder> hashMapActiveSummonDetails = new HashMap<>();


    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_roll_all, container, false);
    }

    @Override
    public void onCreate() {

        layoutParent = rootView.findViewById(R.id.layout_parent);

        List<ActiveDetails> listSummons = new ArrayList<>(mainActivity.summoner.hashMapSummons.values());
        Collections.sort(listSummons, new Comparator<ActiveDetails>() {
            @Override
            public int compare(ActiveDetails a1, ActiveDetails a2) {
                return a1.getDisplayName().compareToIgnoreCase(a2.getDisplayName());
            }
        });

        for (final ActiveDetails activeDetails : listSummons) {
            final LinearLayout layoutActiveSummon = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.layout_all_summon, null);

            TemporaryDataHolder temporaryDataHolder = new TemporaryDataHolder();
            hashMapActiveSummonDetails.put(activeDetails, temporaryDataHolder);
            TextView textView = layoutActiveSummon.findViewById(R.id.textview_summon_name);
            textView.setText(activeDetails.getDisplayName());

            LinearLayout buttonShowHide = layoutActiveSummon.findViewById(R.id.button_show_attacks);
            final LinearLayout layoutAttacks = layoutActiveSummon.findViewById(R.id.layout_attacks);
            buttonShowHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (layoutAttacks.getVisibility()==View.VISIBLE){
                        layoutAttacks.setVisibility(View.GONE);
                    } else {
                        layoutAttacks.setVisibility(View.VISIBLE);
                    }
                }
            });

            hashMapActiveSummonDetails.get(activeDetails).textViewTotal = layoutActiveSummon.findViewById(R.id.textview_total_damage);

            hashMapSummonTotalDamage.put(activeDetails, 0);

            LinearLayout layoutParentAttack = layoutActiveSummon.findViewById(R.id.layout_parent_attacks);
            layoutParent.addView(layoutActiveSummon);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, Utils.getPxFromDP(mainActivity, 10), 0, 0);
            layoutActiveSummon.setLayoutParams(params);

            try{

                for (Attack attack : activeDetails.getListMeleeAttacks()) {

                    temporaryDataHolder.hashMapAttackTotalDamage.put(attack, new Integer[attack.getAttackSequenceOrNaturalMultiples().length]);
                    temporaryDataHolder.hashMapAttackCritExtraDamage.put(attack, new Integer[attack.getAttackSequenceOrNaturalMultiples().length]);
                    temporaryDataHolder.hashMapAttackCritCheckboxes.put(attack, new CheckBox[attack.getAttackSequenceOrNaturalMultiples().length]);
                    temporaryDataHolder.hashMapAttackCheckboxes.put(attack, new CheckBox[attack.getAttackSequenceOrNaturalMultiples().length]);

                    for (int i = 0; i < attack.getAttackSequenceOrNaturalMultiples().length; i++) {
                        LinearLayout layoutAttack = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.layout_all_summon_attack, null);
                        layoutAttack.setLayoutParams(params);
                        layoutParentAttack.addView(layoutAttack);
                        resolveAttack(activeDetails, attack, i, layoutAttack);
                    }
                }

                for (Attack attack : activeDetails.getListRangedAttacks()) {
                    temporaryDataHolder.hashMapAttackTotalDamage.put(attack, new Integer[attack.getAttackSequenceOrNaturalMultiples().length]);
                    temporaryDataHolder.hashMapAttackCritExtraDamage.put(attack, new Integer[attack.getAttackSequenceOrNaturalMultiples().length]);
                    temporaryDataHolder.hashMapAttackCritCheckboxes.put(attack, new CheckBox[attack.getAttackSequenceOrNaturalMultiples().length]);
                    temporaryDataHolder.hashMapAttackCheckboxes.put(attack, new CheckBox[attack.getAttackSequenceOrNaturalMultiples().length]);

                    for (int i = 0; i < attack.getAttackSequenceOrNaturalMultiples().length; i++) {
                        LinearLayout layoutAttack = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.layout_all_summon_attack, null);
                        layoutAttack.setLayoutParams(params);
                        layoutParentAttack.addView(layoutAttack);
                        resolveAttack(activeDetails, attack, i, layoutAttack);
                    }
                }

            } catch (NullPointerException e){
                e.printStackTrace();
            }



            setActiveSummonDamageTotal(activeDetails);
        }

        Button buttonFinished = rootView.findViewById(R.id.button_finished);
        buttonFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    private void setActiveSummonDamageTotal(ActiveDetails activeDetails) {
        int damageTotal = hashMapSummonTotalDamage.get(activeDetails);

        TemporaryDataHolder temporaryDataHolder = hashMapActiveSummonDetails.get(activeDetails);

        String elemental="";
        HashMap<String, Integer> hashMapElementalDamage = temporaryDataHolder.getHashMapElementalTotals();
        int counter=0;
        for (String element: hashMapElementalDamage.keySet()){
            int damage = hashMapElementalDamage.get(element);

            elemental+=element+" "+Integer.toString(damage);
            counter++;
            if (counter<hashMapElementalDamage.size()){
                elemental+=", ";
            }
        }
        if (elemental.length()>0){
            elemental = " (includes: "+elemental+")";
        }

        String text = "Total Damage: <font color=\"#66CD00\"><b>" + Integer.toString(damageTotal)+"</b></font>" + elemental;

        temporaryDataHolder.textViewTotal.setText(Html.fromHtml(text));

        setGrandTotal();
    }

    private void setGrandTotal(){
        int grantTotal=0;
        for (int damage: hashMapSummonTotalDamage.values()){
            grantTotal+=damage;
        }

        String elemental="";
        HashMap<String, Integer> hashMapElementalDamage = getHashMapGrandTotalElementalDamage();
        int counter=0;
        for (String element: hashMapElementalDamage.keySet()){
            int damage = hashMapElementalDamage.get(element);

            elemental+=element+" "+Integer.toString(damage);
            counter++;
            if (counter<hashMapElementalDamage.size()){
                elemental+=", ";
            }
        }
        if (elemental.length()>0){
            elemental = " (includes: "+elemental+")";
        }

        TextView textView = rootView.findViewById(R.id.textview_grant_total);
        textView.setText("Grand Total: "+Integer.toString(grantTotal)+elemental);
    }

    private HashMap<String, Integer> getHashMapGrandTotalElementalDamage(){
        HashMap<String, Integer> hashMap = new HashMap<>();
        for (TemporaryDataHolder temporaryDataHolder : hashMapActiveSummonDetails.values()){
            HashMap<String, Integer> hashMapSummon = temporaryDataHolder.getHashMapElementalTotals();

            for (String element: hashMapSummon.keySet()){
                int damage = hashMapSummon.get(element);

                if (hashMap.containsKey(element)){
                    damage+=hashMap.get(element);
                }
                hashMap.put(element, damage);
            }
        }
        return hashMap;
    }

    private void resolveAttack(final ActiveDetails activeDetails, final Attack attack, final int seqienceRef, LinearLayout layoutAttack) {


        TextView textView = layoutAttack.findViewById(R.id.textview_attack_name);
        String fullName = AttackDamageStatics.getAttackFullName(activeDetails, attack, seqienceRef, true, false);
        textView.setText(Html.fromHtml(fullName+getAdjustmentFlags(activeDetails, attack)));

        LinearLayout buttonShowAttackDetail = layoutAttack.findViewById(R.id.button_show_attack_detail);
        final TextView textViewRollDetails = layoutAttack.findViewById(R.id.textview_roll_details);
        layoutAttack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewRollDetails.getVisibility()==View.VISIBLE){
                    textViewRollDetails.setVisibility(View.GONE);
                } else {
                    textViewRollDetails.setVisibility(View.VISIBLE);
                }
            }
        });

        final TemporaryDataHolder temporaryDataHolder = hashMapActiveSummonDetails.get(activeDetails);

        temporaryDataHolder.hashMapAttackCritExtraDamage.get(attack)[seqienceRef] = 0;

        CheckBox checkBoxHit = layoutAttack.findViewById(R.id.checkbox_hit);
        temporaryDataHolder.hashMapAttackCheckboxes.get(attack)[seqienceRef] = checkBoxHit;
        checkBoxHit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int damage = temporaryDataHolder.hashMapAttackTotalDamage.get(attack)[seqienceRef];
                int summonTotal = hashMapSummonTotalDamage.get(activeDetails);
                if (isChecked) {
                    summonTotal += damage;
                } else {
                    summonTotal -= damage;

                    if (temporaryDataHolder.hashMapAttackCritCheckboxes.get(attack)[seqienceRef].isChecked()) {
                        temporaryDataHolder.hashMapAttackCritCheckboxes.get(attack)[seqienceRef].setChecked(false);
                        summonTotal -= temporaryDataHolder.hashMapAttackCritExtraDamage.get(attack)[seqienceRef];
                    }

                }
                hashMapSummonTotalDamage.put(activeDetails, summonTotal);
                setActiveSummonDamageTotal(activeDetails);
            }
        });

        CheckBox checkBoxCrit = layoutAttack.findViewById(R.id.checkbox_crit);
        temporaryDataHolder.hashMapAttackCritCheckboxes.get(attack)[seqienceRef] = checkBoxCrit;
        checkBoxCrit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                int damage = temporaryDataHolder.hashMapAttackCritExtraDamage.get(attack)[seqienceRef];
                int summonTotal = hashMapSummonTotalDamage.get(activeDetails);
                if (isChecked) {
                    summonTotal += damage;
                    if (!temporaryDataHolder.hashMapAttackCheckboxes.get(attack)[seqienceRef].isChecked()) {
                        temporaryDataHolder.hashMapAttackCheckboxes.get(attack)[seqienceRef].setChecked(true);
                        summonTotal += temporaryDataHolder.hashMapAttackTotalDamage.get(attack)[seqienceRef];
                    }
                } else {
                    summonTotal -= damage;
                }


                hashMapSummonTotalDamage.put(activeDetails, summonTotal);
                setActiveSummonDamageTotal(activeDetails);
            }
        });




        int hitBonus = attack.getAttackSequenceOrNaturalMultiples()[seqienceRef];

        hitBonus = AttackDamageStatics.getAttackHitBonus(activeDetails, attack, hitBonus, true);

        attackResults = new AttackResults(attack, hitBonus);
        Random random = new Random(); /* <-- this is a constructor */
        attackResults.createAttackRoll(activeDetails);
        int[] allRolls = new int[attackResults.listDiceAndRolls.size()];
        for (int i = 0; i < attackResults.listDiceAndRolls.size(); i++) {
            Roll roll = attackResults.listDiceAndRolls.get(i);
            allRolls[i] = random.nextInt(roll.diceSize) + 1;
        }
        parseAttackResults(allRolls, activeDetails, attack, layoutAttack, seqienceRef);
    }


    private String getAdjustmentFlags(ActiveDetails activeDetails, Attack attack){
        AttackExtraInformation attackExtraInformation = activeDetails.hashMapAttackExtraInformation.get(attack);

        String adjustmentFlags=" ";

        if (attackExtraInformation.powerAttack){
            adjustmentFlags += "<font color=\"#FFFFFF\">[PA] </font>";
        }
        if (attackExtraInformation.vitalStrike){
            adjustmentFlags += "<font color=\"#FFFFFF\">[VS] </font>";
        }

        if (attackExtraInformation.smite){
            adjustmentFlags += "<font color=\"#FFFFFF\">[S] </font>";
        }
        if (attackExtraInformation.powerfulCharge){
            adjustmentFlags += "<font color=\"#FFFFFF\">[PC] </font>";
        }
        if (attackExtraInformation.damageBonus!=0 || attackExtraInformation.attackBonus!=0){
            adjustmentFlags += "<font color=\"#FFFFFF\">[ADJ]</font>";
        }

        return adjustmentFlags;
    }

    private void parseAttackResults(int[] allRolls, ActiveDetails activeDetails, Attack attack, LinearLayout layoutAttack, int sequencRef) {

        // allrolls will correspond with list in attackResults
        attackResults.setResults(allRolls);

        PowerfulCharge powerfulCharge = AttackDamageStatics.getPowerfulCharge(activeDetails, attack);

        TemporaryDataHolder temporaryDataHolder = hashMapActiveSummonDetails.get(activeDetails);

        int hitTotal = attackResults.attackRoll.rollResult + attackResults.hitBonus;
        String hitResult = "Attack Roll 1d20 + " + Integer.toString(attackResults.hitBonus) + " = ("
                + attackResults.attackRoll.rollResult + ") + " + attackResults.hitBonus;

        int baseDamage = AttackDamageStatics.getDamageBonus(activeDetails, attack, true, powerfulCharge);
        attackResults.damageTotal = baseDamage;
        String damageResult = "";


        if (attackResults.attack.noDamage) {
            attackResults.damageTotal = 0;
            damageResult = "No damage";
        } else if (attackResults.attack.singleDamage) {
            attackResults.damageTotal = 1;
            damageResult = "1";
        } else {
            // build basic dice roll and damage bonus eg 3d6+7

            int damageDiceNumber = AttackDamageStatics.getDamageDiceNumber(activeDetails, attack, false, powerfulCharge);
            int damageDiceSize = AttackDamageStatics.getDamageDiceSize(activeDetails, attack, powerfulCharge);

            damageResult = "Damage " + Integer.toString(damageDiceNumber) + "d" + Integer.toString(damageDiceSize);
            if (attackResults.damageTotal > 0) {
                damageResult += Utils.getSymbol(attackResults.damageTotal) + Integer.toString(attackResults.damageTotal);
            }
            if (attackResults.attack.elementalDice != null) {
                damageResult += " + " + Integer.toString(attack.elementalDice.diceNumber) + "d" + Integer.toString(attack.elementalDice.diceSize) + "[" +
                        attack.elementalDice.type + "]";
            }

            damageResult += " = ";

            int counter = 0;
            for (Roll roll : attackResults.listDamageDice) {
                if (roll.halfResult) {
                    int halfResult = (int) Math.ceil((double) roll.rollResult / 2);
                    attackResults.damageTotal += halfResult;
                    damageResult += bracketNum(halfResult);
                } else {
                    attackResults.damageTotal += roll.rollResult;
                    damageResult += bracketNum(roll.rollResult);
                }

                counter++;
                if (counter < attackResults.listDamageDice.size()) {
                    damageResult += " + ";
                }
            }

            if (baseDamage != 0) {
                damageResult += " " + Utils.getSymbol(baseDamage) + " " + Integer.toString(baseDamage);
            }
            // reset counter and do elemental dice
            counter = 0;
            for (Roll roll : attackResults.listElementalDice) {
                attackResults.damageTotal += roll.rollResult;
                //Log.w(Constants.TAG, "ELEMENTAL ROLL: "+roll.)
                damageResult += " + (" + Integer.toString(roll.rollResult) + " " + roll.element + ")";

                temporaryDataHolder.adjustElementalDamage(attack, roll.element, roll.rollResult, sequencRef);

                counter++;
//                if (counter<attackResults.listElementalDice.size()){
//                    damageResult+=" + ";
//                }
            }
            for (ElementalDice elementalDice : attackResults.listSingleDamageElementalDice) {
                attackResults.damageTotal++;
                damageResult += " + (1 " + elementalDice.type + ")";

                temporaryDataHolder.adjustElementalDamage(attack, elementalDice.type, 1, sequencRef);
            }
        }


        //damageResult = "Damage "+attackResults.damageTotal +" ("+damageResult+")";

        int totalDam = attackResults.damageTotal;
        if (totalDam < 1 && !attack.noDamage) {
            totalDam = 1;
            damageResult = "NONLETHAL 1 (" + attackResults.damageTotal + " (" + damageResult + "))";
        }

        temporaryDataHolder.hashMapAttackTotalDamage.get(attack)[sequencRef] = totalDam;

        adjustTotal(activeDetails, totalDam);

        updateHitAndDamage(layoutAttack, hitTotal, hitResult, totalDam, damageResult, temporaryDataHolder.getElementalDisplay(attack, sequencRef));
        // assign values
        // check for crits
        checkForCrit(activeDetails, attack, layoutAttack, sequencRef);
        //update ui

    }

    private void checkForCrit(final ActiveDetails activeDetails, final Attack attack, LinearLayout layoutAttack, final int seqienceRef) {
        //if (attackResults.attackRoll.rollResult>=0){

        if (attackResults.attackRoll.rollResult >= attackResults.attack.critRange && !attackResults.attack.noDamage) {

            Random random = new Random(); /* <-- this is a constructor */
            attackResults.createCritRoll(activeDetails);

            int[] allRolls = new int[attackResults.listDiceAndRolls.size()];
            for (int i = 0; i < attackResults.listDiceAndRolls.size(); i++) {
                Roll roll = attackResults.listDiceAndRolls.get(i);
                if (roll.rollResult == -1) {
                    allRolls[i] = random.nextInt(roll.diceSize) + 1;
                }
            }
            parseCritResults(allRolls, activeDetails, attack, layoutAttack, seqienceRef);

        }
    }

    private void parseCritResults(int[] allRolls, ActiveDetails activeDetails, Attack attack, LinearLayout layoutAttack, int sequenceRef) {

        attackResults.setResults(allRolls);

        PowerfulCharge powerfulCharge = AttackDamageStatics.getPowerfulCharge(activeDetails, attack);

        int critConfirm = attackResults.critRoll.rollResult + attackResults.hitBonus;
        String critConfirmResult = "Crit Confirm Roll 1d20 + " + attackResults.hitBonus + " = "
                + "(" + attackResults.critRoll.rollResult + ") + " + attackResults.hitBonus;

        // add previis damage
        int critTotal = attackResults.damageTotal;
        // add basic damage mod multipled by crit multiplier -1
        int critDamageBonus = AttackDamageStatics.getDamageBonus(activeDetails, attack, true, powerfulCharge) * (attackResults.attack.critMultiplier - 1);
        int critBaseDamage = critDamageBonus;
        critTotal += critDamageBonus;

        String critTotalResult = "Critital Total ";

        if (attackResults.attack.singleDamage) {
            critTotal += 1;
            critTotalResult += "1";
        } else {
            int damageDiceNumber = AttackDamageStatics.getDamageDiceNumber(activeDetails, attack, true, powerfulCharge);

            int numCritDice = damageDiceNumber * (attackResults.attack.critMultiplier - 1);
            int critDiceSize = AttackDamageStatics.getDamageDiceSize(activeDetails, attack, powerfulCharge);
            critTotalResult += Integer.toString(numCritDice) + "d" + Integer.toString(critDiceSize);
            if (critBaseDamage > 0) {
                critTotalResult += " + " + Integer.toString(critBaseDamage);
            }
            critTotalResult += " + " + Integer.toString(attackResults.damageTotal) + " = ";
            int counter = 0;
            for (Roll roll : attackResults.listCritDice) {
                if (roll.halfResult) {
                    int halfResult = (int) Math.ceil((double) roll.rollResult / 2);
                    critTotal += halfResult;
                    critTotalResult += bracketNum(halfResult);
                } else {
                    critTotal += roll.rollResult;
                    critTotalResult += bracketNum(roll.rollResult);
                }

                counter++;
                if (counter < attackResults.listCritDice.size()) {
                    critTotalResult += " + ";
                }
            }

            if (critBaseDamage > 0) {
                critTotalResult += " + " + Integer.toString(critBaseDamage);
            }

            critTotalResult += " + " + Integer.toString(attackResults.damageTotal);
        }

        if (critTotal < 1) {
            critTotal = 1;
            critTotalResult = "NONLETHAL CRIT " + critTotal + " (" + critTotalResult + ")";
        } else {
            //critTotalResult = "Crit Total "+critTotal +" ("+critTotalResult+")";
        }


        TemporaryDataHolder temporaryDataHolder = hashMapActiveSummonDetails.get(activeDetails);
        int critAmount = critTotal - attackResults.damageTotal;
        temporaryDataHolder.hashMapAttackCritExtraDamage.get(attack)[sequenceRef] = critAmount;
        adjustTotal(activeDetails, critAmount);

        updateCritAndCritTotal(layoutAttack, critConfirm, critConfirmResult, critTotal, critTotalResult);

    }


    private void updateHitAndDamage(LinearLayout layoutAttack, int hitTotal, String hitResult, int totalDam, String damageResult, String elementalDetail) {

        TextView textView = layoutAttack.findViewById(R.id.textview_hit);
        String text = "Hit: <b>" + Integer.toString(hitTotal)+"</b>";
        textView.setText(Html.fromHtml(text));

        text = "Dam: <b>" + Integer.toString(totalDam)+"</b>"+elementalDetail;
        textView = layoutAttack.findViewById(R.id.textview_damage);
        textView.setText(Html.fromHtml(text));

        textView = layoutAttack.findViewById(R.id.textview_roll_details);
        textView.setText(hitResult + "\n" + damageResult);


    }

    private void updateCritAndCritTotal(LinearLayout layoutAttack, int critConfirm, String critConfirmResult, int critTotal, String critTotalResult) {
        TableRow tableRow = layoutAttack.findViewById(R.id.tablerow_crit);
        tableRow.setVisibility(View.VISIBLE);

        TextView textView = layoutAttack.findViewById(R.id.textview_crit_confirm);
        String text = "Crit Con.: <b>" + Integer.toString(critConfirm)+"</b>";
        textView.setText(Html.fromHtml(text));

        textView = layoutAttack.findViewById(R.id.textview_crit_total);
        text = "Tot. Dam.: <b>" + Integer.toString(critTotal)+"</b>";
        textView.setText(Html.fromHtml(text));

        textView = layoutAttack.findViewById(R.id.textview_roll_details);
        String existing = textView.getText().toString();
        textView.setText(existing + "\n" + critConfirmResult + "\n" + critTotalResult);


    }

    private String bracketNum(int num) {
        return "(" + Integer.toString(num) + ")";
    }


    private void adjustTotal(ActiveDetails activeDetails, int amount){
        int summonTotal = hashMapSummonTotalDamage.get(activeDetails);
        summonTotal+=amount;
        hashMapSummonTotalDamage.put(activeDetails, summonTotal);
    }


    private class TemporaryDataHolder {

        TextView textViewTotal;
        HashMap<Attack, Integer[]> hashMapAttackTotalDamage = new HashMap<>();
        HashMap<Attack, Integer[]> hashMapAttackCritExtraDamage = new HashMap<>();
        HashMap<Attack, CheckBox[]> hashMapAttackCritCheckboxes = new HashMap<>();
        HashMap<Attack, CheckBox[]> hashMapAttackCheckboxes = new HashMap<>();

        private HashMap<Attack, HashMap<String, Integer>[]> hashMapElementalTotals = new HashMap<>();

        void adjustElementalDamage(Attack attack, String element, int damage, int sequenceRef){
            if (!hashMapElementalTotals.containsKey(attack)){ ;
                HashMap<String, Integer>[] hashMap = new HashMap[attack.getAttackSequenceOrNaturalMultiples().length];
                for (int i=0; i<attack.getAttackSequenceOrNaturalMultiples().length; i++){
                    hashMap[i]=new HashMap<>();
                }
                hashMapElementalTotals.put(attack, hashMap);

            }
            if (hashMapElementalTotals.get(attack)[sequenceRef].containsKey(element)){
                damage +=hashMapElementalTotals.get(attack)[sequenceRef].get(element);
            }
            if (damage>0){
                hashMapElementalTotals.get(attack)[sequenceRef].put(element, damage);
            } else {
                hashMapElementalTotals.get(attack)[sequenceRef].remove(element);
            }
        }

        HashMap<String, Integer> getHashMapElementalTotals(){

            HashMap<String, Integer> hashMapTotal = new HashMap<>();
            for (Attack attack: hashMapAttackCheckboxes.keySet()){
                if (hashMapElementalTotals.containsKey(attack)){

                    for (int sequenceRef=0; sequenceRef<attack.getAttackSequenceOrNaturalMultiples().length; sequenceRef++){
                        if (hashMapAttackCheckboxes.get(attack)[sequenceRef].isChecked()){

                            for (String element: hashMapElementalTotals.get(attack)[sequenceRef].keySet()){

                                int amount = hashMapElementalTotals.get(attack)[sequenceRef].get(element);
                                if (hashMapTotal.containsKey(element)){
                                    amount+=hashMapTotal.get(element);
                                }
                                hashMapTotal.put(element, amount);

                            }


                        }
                    }
                }
            }

            return hashMapTotal;
        }

        String getElementalDisplay(Attack attack, int sequenceRef){
            String elementalDamage="";

            if (hashMapElementalTotals.containsKey(attack)){
                if (hashMapElementalTotals.get(attack)[sequenceRef]!=null){
                    HashMap<String, Integer> hashMap = hashMapElementalTotals.get(attack)[sequenceRef];
                    int counter=0;
                    for (String element: hashMap.keySet()){
                        int amount = hashMap.get(element);
                        elementalDamage+=element+" "+Integer.toString(amount);
                        counter++;
                        if (counter<hashMap.size()){
                            elementalDamage+=", ";
                        }
                    }
                }
            }

            if (elementalDamage.length()>0){
                elementalDamage = " (inc. "+elementalDamage+")";
            }

            return  elementalDamage;
        }

    }


}
