package com.redrazors.mastersummoner.views.dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.data.SummonListItem;
import com.redrazors.mastersummoner.staticmethods.ShadowCaller;
import com.redrazors.mastersummoner.views.fragments.FragmentSummon;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by david on 19/09/2018.
 */

public class DialogSelectExpanded extends DialogTemplate {

    private SharedPreferences preferences;

    private HashMap<String, CheckBox> hashMapCheckBoxes = new HashMap<>();
    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_select_expanded, container, false);
    }

    @Override
    public void onCreate() {

        preferences = getActivity().getSharedPreferences(Constants.PREFERENCES_EXPANDED_SUMMONS, Context.MODE_PRIVATE);

        LinearLayout layoutParent = rootView.findViewById(R.id.layout_parent);
        for (int i=1; i<=9; i++){
            List<String> listItems = new ArrayList<>();

            Log.e(Constants.TAG, "ADDING ITEMS LEVEL "+i);
            for (SummonListItem summonListItem: dataControl.hashMapSummonListItems.get(Constants.FEAT_EXPANDED).get(i)){

                Log.e(Constants.TAG, "ADDING "+summonListItem.name);
                listItems.add(summonListItem.name);

            }

            Collections.sort(listItems);

            for (final String name: listItems){
                LinearLayout layoutExpanded = (LinearLayout) mainActivity.getLayoutInflater().inflate(R.layout.layout_expanded_summon, null);
                TextView textView = layoutExpanded.findViewById(R.id.textview_level);
                textView.setText(Integer.toString(i));
                CheckBox checkBox = layoutExpanded.findViewById(R.id.checkbox_expanded);
                checkBox.setText(name);

                boolean checked = preferences.getBoolean(name, true);
                checkBox.setChecked(checked);

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        saveMonsterSetting(name, isChecked);
                    }
                });

                hashMapCheckBoxes.put(name, checkBox);


                layoutParent.addView(layoutExpanded);
            }

        }



        Button buttonSelectAll = rootView.findViewById(R.id.button_select_all);
        buttonSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (String name: hashMapCheckBoxes.keySet()){
                    try{
                        CheckBox checkBox = hashMapCheckBoxes.get(name);
                        checkBox.setChecked(true);
                    } catch (Exception e){
                        e.printStackTrace();
                    }

                }
                SharedPreferences.Editor  editor = preferences.edit();
                editor.clear();
                editor.apply();
            }
        });

        Button buttonDeselectAll = rootView.findViewById(R.id.button_deselect_all);
        buttonDeselectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor  editor = preferences.edit();
                for (String name: hashMapCheckBoxes.keySet()){
                    try{
                        CheckBox checkBox = hashMapCheckBoxes.get(name);
                        checkBox.setChecked(false);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    editor.putBoolean(name, false);

                }
                editor.apply();
            }
        });



        Button buttonFinished = rootView.findViewById(R.id.button_finished);
        buttonFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentSummon fragmentSummon = mainActivity.tabControl.getFragmentSummon();
                if (fragmentSummon!=null){
                    fragmentSummon.setListView();
                }
                dismiss();
            }
        });

    }

    private void saveMonsterSetting(String name, boolean checked){

        SharedPreferences.Editor  editor = preferences.edit();
        if (!checked){
            editor.putBoolean(name, false);
        } else {
            editor.remove(name);
        }

        editor.apply();
    }
}
