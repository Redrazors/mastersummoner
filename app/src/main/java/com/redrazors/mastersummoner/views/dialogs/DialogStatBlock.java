package com.redrazors.mastersummoner.views.dialogs;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.StatBlockParser.StatBlockParser;
import com.redrazors.mastersummoner.active.ActiveDetails;


/**
 * Created by david on 19/09/2018.
 */

public class DialogStatBlock extends DialogTemplate {



    private WebView webView;

    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_statblock, container, false);
    }

    @Override
    public void onCreate() {


        final String summonID = getArguments().getString(Constants.ARGUMENT_SUMMON_ID);



        webView = rootView.findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }
        });
        //webView.getSettings().setJavaScriptEnabled(true);

        ActiveDetails activeDetails = mainActivity.summoner.hashMapSummons.get(summonID);
        if (activeDetails==null){
            dismiss();
            return;
        }

        String statBlock = StatBlockParser.getHTMLStatBlock(activeDetails);
        //statBlock = OldStatBlockParser.getParsedStatBlock(statBlock);

        webView.loadDataWithBaseURL("file:///android_asset/", statBlock, "text/html", "utf-8", null);


        Button buttonFinished = rootView.findViewById(R.id.button_finished);
        buttonFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });

    }



}
