package com.redrazors.mastersummoner.views.dialogs;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.StatBlockParser.StatBlockParser;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.data.Monster;

import com.redrazors.mastersummoner.data.TemplateInfo;
import com.redrazors.mastersummoner.templates.TemplateControl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by david on 19/09/2018.
 */

public class DialogStatBlockSummon extends DialogTemplate {



    private WebView webView;
    private Spinner spinnerAmount, spinnerTemplate;
    private Monster monster;
    private ToggleButton buttonRodGiant, buttonAugment;


    @Override
    public View getRootView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.dialog_fragment_summon, container, false);
    }

    @Override
    public void onCreate() {


        final String monsterName = getArguments().getString(Constants.ARGUMENT_SUMMON_NAME);
        final boolean hasTemplate = getArguments().getBoolean(Constants.ARGUMENT_SUMMON_HAS_TEMPLATE);



        monster = dataControl.bestiary.hashMapMonsters.get(monsterName);
        if (monster==null){
            Toast.makeText(mainActivity, "Unable to find "+monsterName, Toast.LENGTH_LONG).show();
            dismiss();
            return;
        }

        buttonRodGiant = rootView.findViewById(R.id.button_rod_giant);
        buttonAugment = rootView.findViewById(R.id.button_augment);

        webView = rootView.findViewById(R.id.webview);
        //webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null) {
                    view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
                    return false;
                }
            }
        });



        spinnerAmount = rootView.findViewById(R.id.spinner_amount);
        spinnerTemplate = rootView.findViewById(R.id.spinner_template);



        List<String> listAmount = new ArrayList<>();

        if (dataControl.featFlags.superior){
            listAmount.add("1x");
            listAmount.add("2x");
            listAmount.add("3x");
            listAmount.add("4x");
            listAmount.add("5x");
            listAmount.add("6x");

            listAmount.add("1d3+1");
            listAmount.add("1d4+2");

        } else {
            listAmount.add("1x");
            listAmount.add("2x");
            listAmount.add("3x");
            listAmount.add("4x");
            listAmount.add("5x");

            listAmount.add("1d3");
            listAmount.add("1d4+1");
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<>(mainActivity, R.layout.spinner_item_small_white, listAmount);
        spinnerAmount.setAdapter(adapter);

        LinearLayout layoutSpinnerTemplate = rootView.findViewById(R.id.layout_spinner_template);

        layoutSpinnerTemplate.setVisibility(View.VISIBLE);
        spinnerTemplate.setVisibility(View.VISIBLE);

        List<String> listTemplates = TemplateControl.getListEligibleTemplates(dataControl.featFlags, monster, hasTemplate);

        adapter = new ArrayAdapter<>(mainActivity, R.layout.spinner_item_small_white, listTemplates);
        spinnerTemplate.setAdapter(adapter);
        spinnerTemplate.setSelection(getSelctedItemPosition(TemplateControl.getDefaultTemplate(dataControl.featFlags), listTemplates));
        spinnerTemplate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setWebView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (dataControl.featFlags.giantSummoner && monster.size<6){
            buttonRodGiant.setVisibility(View.VISIBLE);
            buttonRodGiant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        buttonView.setTextColor(getResources().getColor(R.color.green));
                    } else {
                        buttonView.setTextColor(getResources().getColor(R.color.white));
                    }
                    setWebView();
                }
            });
        }

        if (dataControl.featFlags.augmented){
            buttonAugment.setVisibility(View.VISIBLE);
            buttonAugment.setChecked(true);
            buttonAugment.setTextColor(getResources().getColor(R.color.green));
            buttonAugment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        buttonView.setTextColor(getResources().getColor(R.color.green));
                    } else {
                        buttonView.setTextColor(getResources().getColor(R.color.white));
                    }
                    setWebView();
                }
            });
        }

        Button buttonCancel = rootView.findViewById(R.id.button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button buttonSummon = rootView.findViewById(R.id.button_summon);
        buttonSummon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int amount=1;

                if (dataControl.featFlags.superior){
                    amount = spinnerAmount.getSelectedItemPosition()+1;
                    if (amount==7){
                        Random random = new Random(); /* <-- this is a constructor */
                        amount = random.nextInt(3) + 1;

                        Toast.makeText(mainActivity, "Rolled 1d3+1: "+"("+Integer.toString(amount)+")+1", Toast.LENGTH_LONG).show();
                        amount+=1;
                    } else if (amount==8){
                        Random random = new Random(); /* <-- this is a constructor */
                        amount = random.nextInt(4) + 1;

                        Toast.makeText(mainActivity, "Rolled 1d4+2: "+"("+Integer.toString(amount)+")+2", Toast.LENGTH_LONG).show();
                        amount+=2;
                    }
                } else {
                    amount = spinnerAmount.getSelectedItemPosition()+1;
                    if (amount==6){
                        Random random = new Random(); /* <-- this is a constructor */
                        amount = random.nextInt(3) + 1;

                        Toast.makeText(mainActivity, "Rolled 1d3: "+"("+Integer.toString(amount)+")", Toast.LENGTH_LONG).show();
                    } else if (amount==7){
                        Random random = new Random(); /* <-- this is a constructor */
                        amount = random.nextInt(4) + 1;

                        Toast.makeText(mainActivity, "Rolled 1d4+1: "+"("+Integer.toString(amount)+")+1", Toast.LENGTH_LONG).show();
                        amount+=1;
                    }
                }


                String template = spinnerTemplate.getSelectedItem().toString();

                mainActivity.summoner.summonMonster(monsterName, amount, template, buttonRodGiant.isChecked(), buttonAugment.isChecked());
                dismiss();

            }
        });

        setWebView();


    }

    private void setWebView(){
        String template = spinnerTemplate.getSelectedItem().toString();
        TemplateInfo templateInfo = dataControl.hashMapTemplates.get(template);

        ActiveDetails activeDetails = new ActiveDetails(null, monster.name, monster, template,
                0, buttonRodGiant.isChecked(), false, buttonAugment.isChecked(), templateInfo);
        String statBlock = StatBlockParser.getHTMLStatBlock(activeDetails);

        webView.loadDataWithBaseURL("file:///android_asset/", statBlock, "text/html", "utf-8", null);
    }

    private int getSelctedItemPosition(String selection, List<String> listItems){
        if (listItems.contains(selection)){
            return listItems.indexOf(selection);
        }
        return 0;
    }

}
