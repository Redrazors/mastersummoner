package com.redrazors.mastersummoner.views.dialogs;


import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.data.DataControl;


/**
 * Created by david on 12/08/2018.
 */

public abstract class DialogTemplate extends DialogFragment {

    public MainActivity mainActivity;
    public View rootView;
    public DataControl dataControl;




    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        mainActivity = (MainActivity) getActivity();
        dataControl = mainActivity.dataControl;



        rootView = getRootView(inflater, container);

        //rootView.setBackgroundColor(getResources().getColor(R.color.page_background_build));

        onCreate();
        return rootView;
                //inflater.inflate(R.layout.dialog_fragment_feats_expandable, container, false);
    }

    public abstract View getRootView(LayoutInflater inflater, ViewGroup container);
    public abstract void onCreate();


}
