package com.redrazors.mastersummoner.views.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.Utils;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.data.Attack;
import com.redrazors.mastersummoner.data.DataControl;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.staticmethods.AttackDamageStatics;
import com.redrazors.mastersummoner.views.animations.ProgressBarAnimation;
import com.redrazors.mastersummoner.views.dialogs.DialogBeginAttack;
import com.redrazors.mastersummoner.views.dialogs.DialogBeginRoll;
import com.redrazors.mastersummoner.views.dialogs.DialogEvolutions;
import com.redrazors.mastersummoner.views.dialogs.DialogHP;
import com.redrazors.mastersummoner.views.dialogs.DialogRollAll;
import com.redrazors.mastersummoner.views.dialogs.DialogStatBlock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class FragmentActive extends Fragment {



    private View rootView;
    private MainActivity mainActivity;
    private DataControl dataControl;

    private HashMap<String, LinearLayout> hashMapActiveSummons = new HashMap<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_active, container, false);


        mainActivity = (MainActivity) getActivity();
        dataControl = mainActivity.dataControl;

        Button buttonRemoveAllSummons = rootView.findViewById(R.id.button_remove_all);
        buttonRemoveAllSummons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmRemoveAllSummons();
            }
        });

        Button buttonRollAll = rootView.findViewById(R.id.button_roll_all);
        buttonRollAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainActivity.summoner.hashMapSummons.size()>0){
                    DialogRollAll dialogRollAll = new DialogRollAll();
                    mainActivity.openDialogFragment(dialogRollAll, "dialogRollAll");
                }
            }
        });

        updateSummons();




        return rootView;
    }


    public void updateSummons(){

        LinearLayout layoutParent = rootView.findViewById(R.id.layout_parent);
        layoutParent.removeAllViews();

        List<ActiveDetails> listSummons = new ArrayList<>(mainActivity.summoner.hashMapSummons.values());
        Collections.sort(listSummons, new Comparator<ActiveDetails>() {
            @Override
            public int compare(ActiveDetails a1, ActiveDetails a2) {
                return a1.getDisplayName().compareToIgnoreCase(a2.getDisplayName());
            }
        });

        for (final ActiveDetails activeDetails: listSummons){
            final Monster monster = mainActivity.dataControl.bestiary.hashMapMonsters.get(activeDetails.name);

            LinearLayout layoutSummon = (LinearLayout)mainActivity.getLayoutInflater().inflate(R.layout.layout_active_summon, null);
            hashMapActiveSummons.put(activeDetails.id, layoutSummon);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, 0, Utils.getPxFromDP(mainActivity, 10));
//            layoutSummon.setLayoutParams(params);


            Button buttonStatBlock = layoutSummon.findViewById(R.id.button_statblock);
            buttonStatBlock.setText(activeDetails.getDisplayName());

            TextView textViewAC = layoutSummon.findViewById(R.id.textview_ac);
            textViewAC.setText(activeDetails.getACString());

            TextView textViewSpeed = layoutSummon.findViewById(R.id.textview_speed);
            textViewSpeed.setText("Speed: "+activeDetails.getSpeedText());

            RelativeLayout relativeLayoutHP = layoutSummon.findViewById(R.id.button_hp);
            relativeLayoutHP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogHP dialogHP = new DialogHP();
                    Bundle bundle  = new Bundle();
                    bundle.putString(Constants.ARGUMENT_SUMMON_ID, activeDetails.id);
                    dialogHP.setArguments(bundle);
                    mainActivity.openDialogFragment(dialogHP, "dialogHP");
                }
            });

            if (!activeDetails.activated){

                int duration = (int)(Math.ceil((double) activeDetails.getActiveMaxHP()/10)*200);
                if (duration>1000){
                    duration=1000;
                }

                ProgressBar progress = layoutSummon.findViewById(R.id.progress_bar_current_hp);
                ProgressBarAnimation anim = new ProgressBarAnimation(progress, 0, activeDetails.currentHP);
                anim.setDuration(duration);
                progress.startAnimation(anim);
                activeDetails.activated=true;
            }


            updateHP(activeDetails.id);

            LinearLayout layoutParentAttacks = layoutSummon.findViewById(R.id.layout_parent_attacks);
            params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, 0, Utils.getPxFromDP(mainActivity, 5));

            for (final Attack attack: activeDetails.getListMeleeAttacks()){
                layoutParentAttacks.addView(getAttackLayout(activeDetails, attack, params));
            }

            for (final Attack attack: activeDetails.getListRangedAttacks()){
                layoutParentAttacks.addView(getAttackLayout(activeDetails, attack, params));
            }




//            Button button = layoutSummon.findViewById(R.id.button_init);
//            button.setText("Init ("+Integer.toString(monsterInfo.init)+")");
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //mainActivity.diceRoller.rollD20(monsterInfo.init);
//                }
//            });
//            button = layoutSummon.findViewById(R.id.button_perception);
//            button.setText("Perception ("+Integer.toString(monsterInfo.perception)+")");
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //mainActivity.diceRoller.rollD20(monsterInfo.perception);
//                }
//            });
            Button buttonCMB = layoutSummon.findViewById(R.id.button_cmb);
            int cmb = activeDetails.getCMB();
            buttonCMB.setText("C.M.B. "+Utils.getSymbolAndAmount(cmb));
            buttonCMB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startd20Roll("C.M.B.", activeDetails.getCMB());
                }
            });

            Button buttonSave = layoutSummon.findViewById(R.id.button_fort);
            buttonSave.setText("Fort "+Utils.getSymbolAndAmount(activeDetails.getFort()));
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startd20Roll("Fortitude", activeDetails.getFort());

                }
            });
            buttonSave = layoutSummon.findViewById(R.id.button_reflex);
            buttonSave.setText("Reflex "+Utils.getSymbolAndAmount(activeDetails.getReflex()));
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startd20Roll("Reflex", activeDetails.getReflex());

                }
            });
            buttonSave = layoutSummon.findViewById(R.id.button_will);
            final int will = activeDetails.getWill();
            buttonSave.setText("Will "+Utils.getSymbolAndAmount(will));
            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startd20Roll("Will", activeDetails.getWill());
                }
            });


            buttonStatBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogStatBlock dialogStatBlock = new DialogStatBlock();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.ARGUMENT_SUMMON_ID, activeDetails.id);
                    dialogStatBlock.setArguments(bundle);
                    mainActivity.openDialogFragment(dialogStatBlock, "dialogStatBlock");
                }
            });

            Button buttonRemove = layoutSummon.findViewById(R.id.button_remove);
            buttonRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmRemoveSummon(activeDetails.id, monster.name);
                }
            });

            Button buttonEvolution = layoutSummon.findViewById(R.id.button_evolutions);
            if (dataControl.featFlags.evolved){
                buttonEvolution.setVisibility(View.VISIBLE);
                int used = 0;
                for (int taken: activeDetails.hashMapEvolutionTimesTaken.values()){
                    used+=taken;
                }
                buttonEvolution.setText("Evolutions: "+Integer.toString(used));
                buttonEvolution.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogEvolutions dialogEvolutions = new DialogEvolutions();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.ARGUMENT_SUMMON_ID, activeDetails.id);
                        dialogEvolutions.setArguments(bundle);
                        mainActivity.openDialogFragment(dialogEvolutions, "dialogEvolutions");
                    }
                });
            } else {
                buttonEvolution.setVisibility(View.GONE);
            }


            LinearLayout layoutBreaker = (LinearLayout) mainActivity.getLayoutInflater().inflate(R.layout.breaker, null);
            layoutParent.addView(layoutBreaker);
            layoutParent.addView(layoutSummon);

        }
    }

    private void startd20Roll(String label, int bonus){
        DialogBeginRoll dialogBeginRoll = new DialogBeginRoll();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ARGUMENT_LABEL, label);
        bundle.putInt(Constants.ARGUMENT_BONUS, bonus);
        dialogBeginRoll.setArguments(bundle);
        mainActivity.openDialogFragment(dialogBeginRoll, "dialogBeginRoll");
    }

    private LinearLayout getAttackLayout(final ActiveDetails activeDetails, final Attack attack, LinearLayout.LayoutParams params){


        LinearLayout layoutAttackButtons = (LinearLayout) mainActivity.getLayoutInflater().inflate(R.layout.layout_attack_buttons, null);
        //layoutAttackButtons.setLayoutParams(params);

        Button[] buttons = new Button[5];
        buttons[0]=layoutAttackButtons.findViewById(R.id.button_attack_0);
        buttons[1]=layoutAttackButtons.findViewById(R.id.button_attack_1);
        buttons[2]=layoutAttackButtons.findViewById(R.id.button_attack_2);
        buttons[3]=layoutAttackButtons.findViewById(R.id.button_attack_3);
        buttons[4]=layoutAttackButtons.findViewById(R.id.button_attack_4);

        String attackName = AttackDamageStatics.getAttackFullName(activeDetails, attack, 0, false, true);
        buttons[0].setText(attackName);
        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAttackDialog(attack, activeDetails.id, 0);
            }
        });


        int[] attackSequence = activeDetails.getAttackSequence(attack);
        for (int i=1; i<attackSequence.length; i++){

            try{
                final int sequenceRef=i;
                buttons[i].setVisibility(View.VISIBLE);
                buttons[i].setText("+"+Integer.toString(attackSequence[i]));
                buttons[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        openAttackDialog(attack, activeDetails.id, sequenceRef);
                    }
                }) ;
            } catch (Exception e){
                e.printStackTrace();
            }

        }

        return layoutAttackButtons;
    }

    private void openAttackDialog(Attack attack, String id, int sequenceRef){
        DialogBeginAttack dialogBeginAttack = new DialogBeginAttack();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ARGUMENT_ATTACK, attack);
        bundle.putString(Constants.ARGUMENT_SUMMON_ID, id);
        bundle.putInt(Constants.ARGUMENT_SEQUENCE_REF, sequenceRef);
        dialogBeginAttack.setArguments(bundle);
        mainActivity.openDialogFragment(dialogBeginAttack, "dialogBeginAttack");
    }

    public void updateHP(String id){

        try{
            ActiveDetails activeDetails = mainActivity.summoner.hashMapSummons.get(id);
            Monster monster = mainActivity.dataControl.bestiary.hashMapMonsters.get(activeDetails.name);

            int maxpHP = activeDetails.getActiveMaxHP();

            LinearLayout layoutSummon = hashMapActiveSummons.get(id);

            ProgressBar progressBarHP = layoutSummon.findViewById(R.id.progress_bar_current_hp);
            progressBarHP.setMax(maxpHP);
            progressBarHP.setProgress(activeDetails.currentHP);

            TextView textViewHP = layoutSummon.findViewById(R.id.textview_hp);
            textViewHP.setText("HP "+activeDetails.currentHP+"/"+maxpHP);

        } catch (NullPointerException e){
            e.printStackTrace();
            Toast.makeText(mainActivity, "Summon was not found!", Toast.LENGTH_LONG).show();
        }


    }



    private void confirmRemoveSummon(final String id, final String name){
        new AlertDialog.Builder(mainActivity)
                .setTitle("Remove Summon")
                .setMessage("Remove "+name+" from your active summons?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        mainActivity.summoner.hashMapSummons.remove(id);

                        updateSummons();

                    }})
                .setNegativeButton(android.R.string.no,null).show();


    }

    private void confirmRemoveAllSummons(){
        if (!mainActivity.summoner.hashMapSummons.isEmpty()){
            new AlertDialog.Builder(mainActivity)
                    .setTitle("Remove All Summons")
                    .setMessage("Remove all from your active summons?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            mainActivity.summoner.hashMapSummons.clear();

                            updateSummons();

                        }})
                    .setNegativeButton(android.R.string.no,null).show();

        }


    }
}
