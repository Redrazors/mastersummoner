package com.redrazors.mastersummoner.views.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.active.ActiveDetails;
import com.redrazors.mastersummoner.data.DataControl;
import com.redrazors.mastersummoner.data.Evolution;
import com.redrazors.mastersummoner.data.Monster;
import com.redrazors.mastersummoner.views.dialogs.DialogLicense;
import com.redrazors.mastersummoner.views.dialogs.DialogSelectExpanded;

import java.util.HashMap;


public class FragmentOptions extends Fragment {



    private View rootView;
    private MainActivity mainActivity;
    private DataControl dataControl;


    private HashMap<String, CheckBox> hashMapCheckBoxes = new HashMap<>();


    private CheckBox checkBoxAugment, checkBoxSuperior, checkBoxVersatile, checkBoxSound, checkBoxDice, checkBoxRodGiant, checkBoxEvolved;
    private CheckBox checkBoxRingAeon, checkBoxRingAgathion,  checkBoxRingAngel,checkBoxRingArchon,checkBoxRingAsura,checkBoxRingDaemon,checkBoxRingDiv,
            checkBoxRingInevitable,checkBoxRingKyton,checkBoxRingProtean,checkBoxRingPsychopomp,checkBoxRingQlippoth;
    private Button buttonSelectExpanded;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_options, container, false);


        mainActivity = (MainActivity) getActivity();
        dataControl = mainActivity.dataControl;


        hashMapCheckBoxes.put(Constants.FEAT_ALTERNATIVE, (CheckBox)rootView.findViewById(R.id.checkbox_alternative));
        hashMapCheckBoxes.put(Constants.FEAT_EXPANDED, (CheckBox)rootView.findViewById(R.id.checkbox_expanded));
        hashMapCheckBoxes.put(Constants.FEAT_GOOD, (CheckBox)rootView.findViewById(R.id.checkbox_good));
        hashMapCheckBoxes.put(Constants.FEAT_NEUTRAL, (CheckBox)rootView.findViewById(R.id.checkbox_neutral));
        hashMapCheckBoxes.put(Constants.FEAT_EVIL, (CheckBox)rootView.findViewById(R.id.checkbox_evil));
        hashMapCheckBoxes.put(Constants.FEAT_SPIDER, (CheckBox)rootView.findViewById(R.id.checkbox_spiders));
        hashMapCheckBoxes.put(Constants.FEAT_SKELETON, (CheckBox)rootView.findViewById(R.id.checkbox_skeleton));
        hashMapCheckBoxes.put(Constants.FEAT_PLANT, (CheckBox)rootView.findViewById(R.id.checkbox_plant));
        hashMapCheckBoxes.put(Constants.ARCHETYPE_SHADOW_CALLER, (CheckBox)rootView.findViewById(R.id.checkbox_shadow_caller));
        hashMapCheckBoxes.put(Constants.ARMY_OF_DARkNESS, (CheckBox)rootView.findViewById(R.id.checkbox_army_darkness));


        hashMapCheckBoxes.put(Constants.RING_AEON, (CheckBox)rootView.findViewById(R.id.checkbox_ring_aeon));
        hashMapCheckBoxes.put(Constants.RING_AGATHION, (CheckBox)rootView.findViewById(R.id.checkbox_ring_agathion));
        hashMapCheckBoxes.put(Constants.RING_ANGEL, (CheckBox)rootView.findViewById(R.id.checkbox_ring_angel));
        hashMapCheckBoxes.put(Constants.RING_ARCHON, (CheckBox)rootView.findViewById(R.id.checkbox_ring_archon));
        hashMapCheckBoxes.put(Constants.RING_ASURA, (CheckBox)rootView.findViewById(R.id.checkbox_ring_asura));
        hashMapCheckBoxes.put(Constants.RING_DAEMON, (CheckBox)rootView.findViewById(R.id.checkbox_ring_daemon));
        hashMapCheckBoxes.put(Constants.RING_DIV, (CheckBox)rootView.findViewById(R.id.checkbox_ring_div));
        hashMapCheckBoxes.put(Constants.RING_INEVITABLE, (CheckBox)rootView.findViewById(R.id.checkbox_ring_inevitable));
        hashMapCheckBoxes.put(Constants.RING_KYTON, (CheckBox)rootView.findViewById(R.id.checkbox_ring_kyton));
        hashMapCheckBoxes.put(Constants.RING_PROTEAN, (CheckBox)rootView.findViewById(R.id.checkbox_ring_protean));
        hashMapCheckBoxes.put(Constants.RING_PSYCOPOMP, (CheckBox)rootView.findViewById(R.id.checkbox_ring_psychopomp));
        hashMapCheckBoxes.put(Constants.RING_QLIPPOTH, (CheckBox)rootView.findViewById(R.id.checkbox_ring_qlippoth));


        hashMapCheckBoxes.put(Constants.RING_DRAKE, (CheckBox)rootView.findViewById(R.id.checkbox_ring_drake));
        hashMapCheckBoxes.put(Constants.RING_KAMI, (CheckBox)rootView.findViewById(R.id.checkbox_ring_kami));
        hashMapCheckBoxes.put(Constants.RING_LESHY, (CheckBox)rootView.findViewById(R.id.checkbox_ring_leshy));




        buttonSelectExpanded = rootView.findViewById(R.id.button_select_expanded);


        checkBoxAugment = rootView.findViewById(R.id.checkbox_augment);
        checkBoxSuperior = rootView.findViewById(R.id.checkbox_superior);
        checkBoxVersatile = rootView.findViewById(R.id.checkbox_versatile);
        checkBoxSound = rootView.findViewById(R.id.checkbox_sound);
        checkBoxDice = rootView.findViewById(R.id.checkbox_dice);
        checkBoxRodGiant = rootView.findViewById(R.id.checkbox_rod_giant);
        checkBoxEvolved = rootView.findViewById(R.id.checkbox_evolved);

        for (final String flagName: hashMapCheckBoxes.keySet()){
            CheckBox checkBox = hashMapCheckBoxes.get(flagName);

            checkBox.setChecked(dataControl.featFlags.hashMapFlags.get(flagName));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    dataControl.featFlags.hashMapFlags.put(flagName, isChecked);
                    saveFlag(flagName, isChecked);
                    setListView();
                }
            });

        }

        hashMapCheckBoxes.get(Constants.FEAT_EXPANDED).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.hashMapFlags.put(Constants.FEAT_EXPANDED, isChecked);
                saveFlag(Constants.FEAT_EXPANDED, isChecked);
                if (isChecked){
                    buttonSelectExpanded.setVisibility(View.VISIBLE);
                } else {
                    buttonSelectExpanded.setVisibility(View.GONE);
                }
                setListView();

            }
        });

        hashMapCheckBoxes.get(Constants.FEAT_NEUTRAL).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.hashMapFlags.put(Constants.FEAT_NEUTRAL, isChecked);
                saveFlag(Constants.FEAT_NEUTRAL, isChecked);
                if (isChecked){
                    showAugmentText("Feat: Summon Neutral Monster", getResources().getString(R.string.neutral_warning));
                }
                setListView();

            }
        });

        checkBoxAugment.setChecked(dataControl.featFlags.augmented);
        checkBoxAugment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.augmented = isChecked;
                saveFlag(Constants.FEAT_AUGMENTED, isChecked);
                mainActivity.tabControl.getFragmentActive().updateSummons();
                if (isChecked){
                    showAugmentText("Feat: Augment Summons", getResources().getString(R.string.augment_warning));
                }
            }
        });

        checkBoxEvolved.setChecked(dataControl.featFlags.evolved);
        checkBoxEvolved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.evolved = isChecked;
                saveFlag(Constants.FEAT_EVOLVED, isChecked);
                for (ActiveDetails activeDetails: mainActivity.summoner.hashMapSummons.values()){
                    Monster monster = dataControl.bestiary.hashMapMonsters.get(activeDetails.name);
                    for (String name: activeDetails.listActiveEvolutions){
                        Evolution evolution = dataControl.hashMapEvolutions.get(name);
                        evolution.evolutionMethod.activateEvolution(false, monster, activeDetails);
                    }
                    activeDetails.hashMapEvolutionTimesTaken.clear();
                    activeDetails.listActiveEvolutions.clear();
                }

                mainActivity.tabControl.getFragmentActive().updateSummons();

                if (isChecked){
                    showAugmentText("Feat: Evolved Summon Monster", getResources().getString(R.string.evolved_warning));
                }
            }
        });

        checkBoxSuperior.setChecked(dataControl.featFlags.superior);
        checkBoxSuperior.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.superior = isChecked;
                saveFlag(Constants.FEAT_SUPERIOR, isChecked);
            }
        });

        checkBoxRodGiant.setChecked(dataControl.featFlags.giantSummoner);
        checkBoxRodGiant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.giantSummoner = isChecked;
                saveFlag(Constants.EQUIPMENT_ROD_GIANT, isChecked);

                if (isChecked){
                    showAugmentText("Rod of Giant Summoning", getResources().getString(R.string.giant_warning));
                }
            }
        });

        checkBoxVersatile.setChecked(dataControl.featFlags.versatile);
        checkBoxVersatile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.versatile = isChecked;
                saveFlag(Constants.FEAT_VERSATILE, isChecked);
            }
        });

        checkBoxSound.setChecked(dataControl.featFlags.noSound);
        checkBoxSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.noSound=isChecked;
                saveFlag(Constants.OPTION_SOUND, isChecked);
            }
        });

        checkBoxDice.setChecked(dataControl.featFlags.noDice);
        checkBoxDice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dataControl.featFlags.noDice = isChecked;
                saveFlag(Constants.OPTION_DICE, isChecked);

                mainActivity.diceRoller.showHideRollTray(!isChecked);
            }
        });

        final Button buttonSM = rootView.findViewById(R.id.button_sm);
        final Button buttonSNA = rootView.findViewById(R.id.button_sna);

        buttonSM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dataControl.featFlags.modeSummonMonster){
                    setModeSummonMonster();
                    saveFlag(Constants.MODE_SUMMON_MONSTER, true);
                    buttonSM.setBackground(getResources().getDrawable(R.drawable.toggle_on));
                    buttonSNA.setBackground(getResources().getDrawable(R.drawable.toggle_off));
                }

            }
        });
        buttonSNA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataControl.featFlags.modeSummonMonster){
                    setModeSummonNaturesAlly();
                    saveFlag(Constants.MODE_SUMMON_MONSTER, false);
                    buttonSM.setBackground(getResources().getDrawable(R.drawable.toggle_off));
                    buttonSNA.setBackground(getResources().getDrawable(R.drawable.toggle_on));
                }

            }
        });


        if (dataControl.featFlags.modeSummonMonster){
            setModeSummonMonster();
            buttonSM.setBackground(getResources().getDrawable(R.drawable.toggle_on));
            buttonSNA.setBackground(getResources().getDrawable(R.drawable.toggle_off));
        } else {
            setModeSummonNaturesAlly();
            buttonSM.setBackground(getResources().getDrawable(R.drawable.toggle_off));
            buttonSNA.setBackground(getResources().getDrawable(R.drawable.toggle_on));
        }


        buttonSelectExpanded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogSelectExpanded dialogSelectExpanded = new DialogSelectExpanded();
                mainActivity.openDialogFragment(dialogSelectExpanded, "dialogSelectExpanded");
            }
        });

        Button buttonLicense = rootView.findViewById(R.id.button_license);
        buttonLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogLicense dialogLicense = new DialogLicense();
                mainActivity.openDialogFragment(dialogLicense, "dialogLicense");
            }
        });
        return rootView;
    }

    private void setModeSummonMonster(){

        checkBoxVersatile.setText("Feat: Versatile Summon Monster");

        for (String option: Constants.OPTIONS_SUMMON_MONSTER){
            hashMapCheckBoxes.get(option).setVisibility(View.VISIBLE);
        }


        for (String option: Constants.OPTIONS_NATURES_ALLY){
            hashMapCheckBoxes.get(option).setVisibility(View.GONE);
            hashMapCheckBoxes.get(option).setChecked(false);
            dataControl.featFlags.hashMapFlags.put(option, false);
            saveFlag(option, false);

        }

        if (dataControl.featFlags.hashMapFlags.get(Constants.FEAT_EXPANDED)){
            buttonSelectExpanded.setVisibility(View.VISIBLE);
        }
        dataControl.featFlags.modeSummonMonster=true;

        setListView();

    }
    private void setModeSummonNaturesAlly(){

        checkBoxVersatile.setText("Feat: Versatile Summon Nature's Ally");

        for (String option: Constants.OPTIONS_NATURES_ALLY){
            hashMapCheckBoxes.get(option).setVisibility(View.VISIBLE);

        }

        for (String option: Constants.OPTIONS_SUMMON_MONSTER){
            hashMapCheckBoxes.get(option).setVisibility(View.GONE);
            hashMapCheckBoxes.get(option).setChecked(false);
            dataControl.featFlags.hashMapFlags.put(option, false);
            saveFlag(option, false);

        }

        buttonSelectExpanded.setVisibility(View.GONE);
        dataControl.featFlags.modeSummonMonster=false;

        setListView();

    }

    private void setListView(){
        FragmentSummon fragmentSummon = mainActivity.tabControl.getFragmentSummon();
        if (fragmentSummon!=null && fragmentSummon.isResumed()){
            fragmentSummon.setListView();
        }
    }

    private void saveFlag(String flagName, boolean active){
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(Constants.PREFERENCES_FEAT_FLATS, Context.MODE_PRIVATE).edit();
        editor.putBoolean(flagName, active);
        editor.apply();

    }

    private void showAugmentText(String title, String string){
        new AlertDialog.Builder(mainActivity)
                .setTitle(title)
                .setMessage(string)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }})
                .show();


    }
}
