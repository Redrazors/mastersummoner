package com.redrazors.mastersummoner.views.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.redrazors.mastersummoner.Constants;
import com.redrazors.mastersummoner.MainActivity;
import com.redrazors.mastersummoner.R;
import com.redrazors.mastersummoner.data.DataControl;
import com.redrazors.mastersummoner.data.SummonListItem;
import com.redrazors.mastersummoner.views.adapters.ListViewAdapterSummonList;
import com.redrazors.mastersummoner.views.dialogs.DialogStatBlockSummon;

import java.util.List;


public class FragmentSummon extends Fragment {



    //private static final String[] NUMERALS = new String[]{"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
    private View rootView;
    private MainActivity mainActivity;
    private DataControl dataControl;

    private int summonLevel=1;
    private Button[] buttonSummonLevels;


    private int activeButton=1;
    private TextView textViewSummonLevel;



    private static final String[] buttonNames = new String[]{"Minor", "I", "II" ,"III", "IV", "V", "VI", "VII", "VIII", "IX"};

    private ListView listView;
    private ListViewAdapterSummonList listViewAdapterSummonList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_summon, container, false);


        mainActivity = (MainActivity) getActivity();
        dataControl = mainActivity.dataControl;


        textViewSummonLevel = rootView.findViewById(R.id.textview_summon_level);


        buttonSummonLevels = new Button[10];

        for (int i=0; i<10; i++){

            int id = mainActivity.getResources().getIdentifier("button_"+Integer.toString(i), "id", mainActivity.getApplicationContext().getPackageName());

            buttonSummonLevels[i] = rootView.findViewById(id);

            buttonSummonLevels[i].setText(buttonNames[i]);
            final int ref=i;
            buttonSummonLevels[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (ref!=activeButton){

                        buttonSummonLevels[activeButton].setBackground(getResources().getDrawable(R.drawable.toggle_off));

                        activeButton=ref;

                        buttonSummonLevels[ref].setBackground(getResources().getDrawable(R.drawable.toggle_on));
                        summonLevel = ref;

                        textViewSummonLevel.setText(getSummonModeText()+buttonNames[ref]);
                        setListView();
                    }

                }
            });



        }

        buttonSummonLevels[1].setBackground(getResources().getDrawable(R.drawable.toggle_on));





        listView = rootView.findViewById(R.id.listview);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listViewAdapterSummonList.onItemSelected(view, position);
                if (listViewAdapterSummonList.currentSelection!=null){
                    DialogStatBlockSummon dialogStatBlockSummon = new DialogStatBlockSummon();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.ARGUMENT_SUMMON_NAME, listViewAdapterSummonList.currentSelection.name);
                    bundle.putBoolean(Constants.ARGUMENT_SUMMON_HAS_TEMPLATE, listViewAdapterSummonList.currentSelection.template);
                    dialogStatBlockSummon.setArguments(bundle);
                    mainActivity.openDialogFragment(dialogStatBlockSummon, "dialogStatBlockSummon");
                }

            }
        });

        setListView();


        return rootView;
    }

    private String getSummonModeText(){
//        Log.w(Constants.TAG, "mainac "+mainActivity);
//        Log.w(Constants.TAG, "DATACONTROL "+dataControl);
//        Log.w(Constants.TAG, "FF "+dataControl.featFlags);



        if (dataControl.featFlags.modeSummonMonster){
            if (summonLevel==0){
                return "Summon Minor Monster";
            }
            return "Summon Monster "+buttonNames[summonLevel];
        }
        if (summonLevel==0){
            return "Summon Minor Ally";
        }
        return "Summon Nature's Ally "+buttonNames[summonLevel];
    }

    public void setListView(){


        textViewSummonLevel.setText(getSummonModeText());

        List<SummonListItem> listItems = dataControl.getSummonList(summonLevel);

        listViewAdapterSummonList = new ListViewAdapterSummonList(mainActivity, listItems, summonLevel);
        listView.setAdapter(listViewAdapterSummonList);

    }


}
